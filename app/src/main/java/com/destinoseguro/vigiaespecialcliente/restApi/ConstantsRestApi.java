package com.destinoseguro.vigiaespecialcliente.restApi;


/**
 * Created by root on 24/11/17.
 */

public class ConstantsRestApi {

    public final static String CONTROLER_CUSTOMER = "cliente/";
    public final static String MODULE = "app/";
    public final static String ROOT_URL = "http://appvigia.destinoseguro.co/";
    public final static String ACCESS_TOKEN = "alP+dduKROHGsXC1XZCluCF8NyOJeUGexWXGixLyQ7C9WYF9r4Rg0lD0wUAxhsL8LmUnWhZxM3upMqLVPLKsrzDa1L5Rng+p\\/DRJSmR6lAnlZS0xdo+XQ8L5Lfoymw8H9VcM+ZlVdQMsV4z2O1eH40DdOtaJ4L\\/v01NoXJGEY0yHNIdrEws3KIo1QjQp7QG5InC87iMQpEecFJBeie4uq5UrumRTsU\\/lhOGyqkjcCCo=";
//        val ROOT_URL = " http://androidtutorials.herokuapp.com/"

//    http://androidtutorials.herokuapp.com/usersFake
public final static String ROOT_URL2 = "http://androidtutorials.herokuapp.com/";
public final static String END = "usersFake";


    // registro de ususarios
    public final static String POINT_VALIDATE_IMEI = "getValidateImei";
    public final static String POINT_ADD_USER = "setRegister";
    //    http://192.168.100.56/appvigia/app/cliente/getValidateImei
    public final static String SET_REGISTER =ROOT_URL + MODULE + CONTROLER_CUSTOMER + POINT_ADD_USER;
    public final static String VALIDATE_iMEI = MODULE + CONTROLER_CUSTOMER + POINT_VALIDATE_IMEI;



    // solicitudes
    public final static String POINT_GET_SERVICES = "getTipoServicio";
    public final static String POINT_GET_VEHICLES = "getTipoVehiculo";
    public final static String POINT_GET_OPERATIONS = "getTipoOperacion";
    // http://192.168.100.56/appvigia/app/cliente/getTipoServicio
    public final static String GET_TYPE_SERVICES = MODULE + CONTROLER_CUSTOMER + POINT_GET_SERVICES;
    public final static String GET_TYPE_VEHICLES = MODULE + CONTROLER_CUSTOMER + POINT_GET_VEHICLES;
    public final static String GET_TYPE_OPERATIONS = MODULE + CONTROLER_CUSTOMER + POINT_GET_OPERATIONS;


    // viajes
    public final static String POINT_GET_TRAVELS = "getViajes";
    public final static String POINT_GET_LIST_ASSIGNED = "getAsignado";
    public final static String POINT_GET_LIST_PASSENGERS = "getPasajero";
    public final static String POINT_GET_LIST_POINS_MAP = "getPoints";
    // http://190.145.70.82/appvigia/app/cliente/getViajes
    public final static String GET_TRAVELS = MODULE + CONTROLER_CUSTOMER + POINT_GET_TRAVELS;
    public final static String GET_ASSIGNMENT = MODULE + CONTROLER_CUSTOMER + POINT_GET_LIST_ASSIGNED;
    public final static String GET_PASSENGERS = MODULE + CONTROLER_CUSTOMER + POINT_GET_LIST_PASSENGERS;
    public final static String GET_RUTE = MODULE + CONTROLER_CUSTOMER + POINT_GET_LIST_POINS_MAP;



    // historicos
    // http://190.145.70.82/appvigia/app/cliente/getViajes
//    http://190.145.70.82/appvigia/app/cliente/setCalificacion
    public final static String POINT_GET_HISTORICALS = "getFinalizado";
    public final static String POINT_SET_CALIFICATION_SERVICE = "setCalificacion";
    public final static String GET_HISTORICALS = MODULE + CONTROLER_CUSTOMER + POINT_GET_HISTORICALS;
    public final static String SET_CALIFICATION_SERVICE = MODULE + CONTROLER_CUSTOMER + POINT_SET_CALIFICATION_SERVICE;


//    http://190.145.70.82/appvigia/app/cliente/setSolicitud
      public final static String POINT_SET_REQUEST = "setSolicitud";
    public final static String SET_REQUEST = MODULE + CONTROLER_CUSTOMER + POINT_SET_REQUEST;


}
