package com.destinoseguro.vigiaespecialcliente.restApi

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import com.google.gson.GsonBuilder
import com.google.gson.Gson



/**
 * Created by root on 24/11/17.
 */
class RestApiAdapter {


    fun conectionApiRest(): Retrofit {

        val gson = GsonBuilder()
                .setLenient()
                .create()


        val retrofit:Retrofit = Retrofit.Builder()
               .baseUrl(ConstantsRestApi.ROOT_URL)
               .addConverterFactory(GsonConverterFactory.create(gson))
               .build()

        return retrofit

    }

}