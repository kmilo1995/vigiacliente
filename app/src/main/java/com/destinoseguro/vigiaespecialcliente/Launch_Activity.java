package com.destinoseguro.vigiaespecialcliente;

import android.*;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v13.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.destinoseguro.vigiaespecialcliente.RoomAppDatabase.AppDatabase;
import com.destinoseguro.vigiaespecialcliente.dashboard.activitys.dashboard;
import com.destinoseguro.vigiaespecialcliente.firebase.Firebase;
import com.destinoseguro.vigiaespecialcliente.presentation.PrefManager;
import com.destinoseguro.vigiaespecialcliente.register.activitys.Preactivation;
import com.destinoseguro.vigiaespecialcliente.register.activitys.register;
import com.destinoseguro.vigiaespecialcliente.register.model.EndPointsRegister;
import com.destinoseguro.vigiaespecialcliente.register.model.RequestFirebase;
import com.destinoseguro.vigiaespecialcliente.register.model.ResponseRegister;
import com.destinoseguro.vigiaespecialcliente.register.model.Role;
import com.destinoseguro.vigiaespecialcliente.restApi.RestApiAdapter;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class Launch_Activity extends AppCompatActivity {

    private VideoView mVideoView;
    private ViewPager viewPager;
    private MyViewPagerAdapter myViewPagerAdapter;
    private LinearLayout dotsLayout;
    private TextView[] dots;
    private int[] layouts;
    private Button btnSkip, btnNext,login;
    private PrefManager prefManager;
    private Firebase firebase = Firebase.getInstance();


    private final RestApiAdapter restApiAdapter = new RestApiAdapter();

    private final Retrofit retrofit = restApiAdapter.conectionApiRest();

    private final int REQUEST_READ_PHONE_STATE = 6;
    private final String PHOTO = "photo";
    private final String NAME = "name";
    private final String EMAIL = "email";
    private final String LASTNAME = "LASTNAME";

    private String IMEI = "";
    private SharedPreferences sharedPreferences = null;

    RequestFirebase requestFirebase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
       Boolean session = sharedPreferences.getBoolean("SESSION_USER", false);
       IMEI = sharedPreferences.getString("IMEI","");
       if (session){
           validateActivationAccount(IMEI);
           //startDashBoard(sharedPreferences);

       }else {





           requestFirebase = new RequestFirebase(this);
           requestWindowFeature(Window.FEATURE_NO_TITLE);
           getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

           prefManager = new PrefManager(this);
           if (!prefManager.isFirstTimeLaunch()) {
               launchHomeScreen();
               finish();
           }
           requestWindowFeature(Window.FEATURE_NO_TITLE);
           getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
           setContentView(R.layout.activity_launch);
           mVideoView =  findViewById(R.id.mVideoView);
           Uri uri = Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.video_vigia);

        mVideoView.setVideoURI(uri);
        mVideoView.start();
        mVideoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mediaPlayer) {
                mediaPlayer.setLooping(true);
                mediaPlayer.setVolume(0,0);
            }
        });



           viewPager = findViewById(R.id.view_pager);
           dotsLayout = findViewById(R.id.layoutDots);
           btnSkip = findViewById(R.id.btn_skip);
           btnNext = findViewById(R.id.btn_next);
           login = findViewById(R.id.launchBtnlogin);
           // layouts of all welcome sliders
           // add few more layouts if you want
           layouts = new int[]{
                   R.layout.slider1,
                   R.layout.slider2,
                   R.layout.slider3,
                   R.layout.slider4,
           };
           // adding bottom dots
           addBottomDots(0);
           // making notification bar transparent
           changeStatusBarColor();
           myViewPagerAdapter = new MyViewPagerAdapter();
           viewPager.setAdapter(myViewPagerAdapter);
           viewPager.addOnPageChangeListener(viewPagerPageChangeListener);
           btnSkip.setOnClickListener(new View.OnClickListener() {
               @Override
               public void onClick(View v) {
                   launchHomeScreen();
               }
           });
           btnNext.setOnClickListener(new View.OnClickListener() {
               @Override
               public void onClick(View v) {
                   // checking for last page
                   // if last page home screen will be launched
                   int current = getItem(+1);
                   if (current < layouts.length) {
                       // move to next screen
                       viewPager.setCurrentItem(current);
                   } else {
                       launchHomeScreen();
                   }
               }
           });



           login.setOnClickListener(new View.OnClickListener() {
               @Override
               public void onClick(View v) {

                   permissionReadPhoneState();


               }
           });
       }



    }

    private void startDashBoard(SharedPreferences sharedPreferences) {

        Intent intent = new Intent(getApplicationContext(),dashboard.class);

//        String imgLogoProfile = dataSnapshotUser.child("photo").getValue().toString();
//        String nameProfile = dataSnapshotUser.child("name").getValue().toString();
//        String emailProfile = dataSnapshotUser.child("email").getValue().toString();

        String imgLogoProfile = sharedPreferences.getString(PHOTO, null);
        String nameProfile = sharedPreferences.getString(NAME, null);
        String emailProfile = sharedPreferences.getString(EMAIL, null);
        String lastname = sharedPreferences.getString(LASTNAME, null);
        nameProfile = Character.toUpperCase(nameProfile.charAt(0)) + nameProfile.substring(1,nameProfile.length());
        lastname = Character.toUpperCase(lastname.charAt(0)) + lastname.substring(1,lastname.length());

        intent.putExtra("IMG_LOGO", imgLogoProfile);
        intent.putExtra("NAME_PROFILE", nameProfile+' ' + lastname);
        intent.putExtra("EMAIL_PROFILE", emailProfile);
        intent.putExtra("IMEI",IMEI);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();

    }


    @Override
    protected void onPostResume() {
        super.onPostResume();

        if (mVideoView != null){

          mVideoView.start();
        }

    }

    private void addBottomDots(int currentPage) {
        dots = new TextView[layouts.length];
        int[] colorsActive = getResources().getIntArray(R.array.array_dot_active);
        int[] colorsInactive = getResources().getIntArray(R.array.array_dot_inactive);
        dotsLayout.removeAllViews();
        for (int i = 0; i < dots.length; i++) {
            dots[i] = new TextView(this);
            dots[i].setText(Html.fromHtml("&#8226;"));
            dots[i].setTextSize(35);
            dots[i].setTextColor(colorsInactive[currentPage]);
            dotsLayout.addView(dots[i]);
        }
        if (dots.length > 0)
            dots[currentPage].setTextColor(colorsActive[currentPage]);
    }
    private int getItem(int i) {
        return viewPager.getCurrentItem() + i;
    }
    private void launchHomeScreen() {
        prefManager.setFirstTimeLaunch(false);
//        startActivity(new Intent(Launch_Activity.this, login.class));
        finish();
    }
    //  viewpager change listener
    ViewPager.OnPageChangeListener viewPagerPageChangeListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageSelected(int position) {
            addBottomDots(position);
            // changing the next button text 'NEXT' / 'GOT IT'
            if (position == layouts.length - 1) {
                // last page. make button text to GOT IT
                btnNext.setText("GOT IT");
                btnSkip.setVisibility(View.GONE);
            } else {
                // still pages are left
                btnNext.setText("NEXT");
                btnSkip.setVisibility(View.VISIBLE);
            }
        }
        @Override
        public void onPageScrolled(int arg0, float arg1, int arg2) {
        }
        @Override
        public void onPageScrollStateChanged(int arg0) {
        }
    };
    /**
     * Making notification bar transparent
     */
    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.TRANSPARENT);
        }
    }
    /**
     * View pager adapter
     */
    public class MyViewPagerAdapter extends PagerAdapter {
        private LayoutInflater layoutInflater;
        public MyViewPagerAdapter() {
        }
        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = layoutInflater.inflate(layouts[position], container, false);
            container.addView(view);
            return view;
        }
        @Override
        public int getCount() {
            return layouts.length;
        }
        @Override
        public boolean isViewFromObject(View view, Object obj) {
            return view == obj;
        }
        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            View view = (View) object;
            container.removeView(view);
        }
    }





    public  void showActivityValidateAcount(DataSnapshot dataSnapshot,Context context){

       boolean validation = Boolean.parseBoolean(dataSnapshot.child("activation").getValue().toString());

        if (validation) {
            Log.e("launch if roles ", "showActivityValidateAcount ");
            getRolesFirebase(dataSnapshot);


        }else{

            Log.e("launch else ", "showActivityValidateAcount ");
            Intent intent = new Intent(context,Preactivation.class);
            startActivity(intent);



        }
    }

    private void getRolesFirebase(final DataSnapshot dataSnapshotUser) {

      String  roleName =  dataSnapshotUser.child("roleName").getValue().toString();
        if (roleName != null) {
            Log.e("RoleName",  roleName);

            firebase.getSelectAlterna("Aplicaciones/Customer/Roles/" + roleName).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {


                    Log.e("launch onDataChange","getRolesFirebase "+ dataSnapshot.toString());

                    AppDatabase db = AppDatabase.getAppDatabase(getApplicationContext());
                    db.RoleDao().nukaTable();

                    for (DataSnapshot role : dataSnapshot.getChildren()) {

//                        roles.add(role.getValue().toString());
//
                        Role rol = new Role(Integer.parseInt(role.getKey().toString()),role.getValue().toString());

                        int response = db.RoleDao().update(rol);
                        if (response == 0){

                            db.RoleDao().insert(rol);
                        }
                    }

                    String imgLogoProfile = dataSnapshotUser.child("photo").getValue().toString();
                    String nameProfile = dataSnapshotUser.child("name").getValue().toString();
                    String emailProfile = dataSnapshotUser.child("email").getValue().toString();
                    String lastName = dataSnapshotUser.child("lastName").getValue().toString();

                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putString(PHOTO, imgLogoProfile );
                    editor.putString(NAME, nameProfile );
                    editor.putString(EMAIL, emailProfile );
                    editor.putString(LASTNAME, lastName );
                    editor.putString("IMEI", IMEI );

                    editor.commit();

                    startDashBoard(sharedPreferences);

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    Log.e("launch onCancelled ", "getRolesFirebase " + databaseError.getMessage());
                }
            });

        }

    }



    private void validateIsRegister(final String IMEI) {


//        consulta el servidor para validar que el imei del equipo se encuentra registrado
        Call<ResponseRegister> response = retrofit.create(EndPointsRegister.class).getValidateImei( IMEI );

        response.enqueue(new Callback<ResponseRegister>() {
            @Override
            public void onResponse(Call<ResponseRegister> call, Response<ResponseRegister> response) {

                try{

                    Log.e("launch onResponse ","getValidateImei "+response.body().getData()+" "+response.body().getSuccess());

//                        valida el campo activation del usuario en firebase

//                        validateActivationAccount(response.body().getSuccess());
                    if( response.body().getSuccess() ){
                       validateActivationAccount(IMEI);

                    }else{
                        Intent intent = new Intent(getApplicationContext(),register.class);

                        intent.putExtra("imei",IMEI);

                        startActivity(intent);
                        finish();
                    }



                }catch (Exception e){
                    Log.e("error getValidateImei ", e.getMessage());
                    Toast.makeText(getApplicationContext(),"Error de servidor", Toast.LENGTH_LONG).show();
                    return;

                }



            }


            @Override
            public void onFailure(Call<ResponseRegister> call, Throwable t) {
                Log.e("onFailure ", "getValidateImei "+t.getMessage()+" "+ String.valueOf(t.getCause()));

                Toast.makeText(getApplicationContext(),"No hay conexión Comprueba tus ajustes",Toast.LENGTH_LONG).show();
            }
        });
    }


    private void validateActivationAccount(String IMEI) {

        IMEI="b1c3d2bbad9c176b";
        firebase.getSelectAlterna("Usuarios/Customers/" + IMEI).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                try {

                    Log.e("launch onDataChange ", "validateActivationAccount " + dataSnapshot.toString());

                    /*String base64Img = dataSnapshot.child("photo").getValue().toString();
                    Bitmap bitmapImage =  convertStringToBitmap(base64Img);

                    dashboard.logoProfile.setImageBitmap(bitmapImage);*/
                    showActivityValidateAcount(dataSnapshot,getApplicationContext());

                } catch (Exception e) {
//
                    Log.e("launch exception ", "validateActivationAccount " + e.getMessage());
                    Toast.makeText(getApplicationContext(), "Error de cuenta con firebase", Toast.LENGTH_LONG).show();

                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e("launch onCancelled ", " validateActivationAccount " + databaseError.getMessage());
            }
        });


    }




    private void permissionReadPhoneState() {


        // Here, thisActivity is the current activity
        if (ContextCompat.checkSelfPermission(this,
                android.Manifest.permission.READ_PHONE_STATE)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    android.Manifest.permission.READ_PHONE_STATE)) {

                // Show an expanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
                //
                ActivityCompat.requestPermissions(this,
                        new String[]{android.Manifest.permission.READ_PHONE_STATE},
                        REQUEST_READ_PHONE_STATE);


            } else {

                // No explanation needed, we can request the permission.

                ActivityCompat.requestPermissions(this,
                        new String[]{android.Manifest.permission.READ_PHONE_STATE},
                        REQUEST_READ_PHONE_STATE);

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        }else{

            ActivityCompat.requestPermissions(this,
                    new String[]{android.Manifest.permission.READ_PHONE_STATE},
                    REQUEST_READ_PHONE_STATE);
        }

    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {






        switch (requestCode) {
            case REQUEST_READ_PHONE_STATE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    IMEI = Settings.Secure.getString(getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID);

                    validateIsRegister(IMEI);
                    Log.e("rest","success "+ IMEI);
                } else {

//                    solicitarPermisosManual();
                    Log.e("rest","permission denied");
                    Toast.makeText(this,"Para poder validar el usuario es necesario activar los permisos del telefono "+requestCode,Toast.LENGTH_LONG).show();
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                break;
            }
//
        }
    }

}
