package com.destinoseguro.vigiaespecialcliente.dashboard.activitys;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.support.annotation.RequiresApi;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.destinoseguro.vigiaespecialcliente.R;
import com.destinoseguro.vigiaespecialcliente.RoomAppDatabase.AppDatabase;
import com.destinoseguro.vigiaespecialcliente.firebase.Firebase;
import com.destinoseguro.vigiaespecialcliente.historical.fragments.fragment_historical;
import com.destinoseguro.vigiaespecialcliente.messages.fragments.fragment_messages;
import com.destinoseguro.vigiaespecialcliente.payments.fragments.fragment_payments;
import com.destinoseguro.vigiaespecialcliente.profile.activitys.Setting;
import com.destinoseguro.vigiaespecialcliente.profile.fragments.fragment_profile;
import com.destinoseguro.vigiaespecialcliente.register.activitys.register;
import com.destinoseguro.vigiaespecialcliente.register.model.Role;
import com.destinoseguro.vigiaespecialcliente.register.model.RequestFirebase;
import com.destinoseguro.vigiaespecialcliente.requests.fragments.fragment_request;
import com.destinoseguro.vigiaespecialcliente.travels.fragments.fragment_list_travels;
import com.github.clans.fab.FloatingActionMenu;
import com.github.clans.fab.FloatingActionButton;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;


public class dashboard extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {


    private static int MENU_ITEM_SELECTED = 0;



    Fragment fragmentProfile = new fragment_profile();
    fragment_list_travels fragmentTravels =new fragment_list_travels();
    fragment_messages fragmentMessages =new fragment_messages();
    fragment_historical fragmentHistorical =new fragment_historical();
    fragment_payments fragmentPayments =new fragment_payments();
    fragment_request fragmentRequest =new fragment_request();
    Fragment fragment = null;

    FloatingActionButton[] listButton = {};


//    FloatingActionButton fab;

//    floating




    private FloatingActionMenu menuFab;

    private FloatingActionButton fab_edit;
    private FloatingActionButton fab_remove;
    private FloatingActionButton fab_add;
    private FloatingActionButton fab_list;
    private FloatingActionButton fab_save;


    private List<FloatingActionMenu> menus = new ArrayList<>();
    private Handler mUiHandler = new Handler();

    private static HashMap<String,Role>listRoles;

    private Firebase firebase = Firebase.getInstance();
    private Bundle arg = new Bundle();

    private String IMEI = null;
    private NavigationView navigationView;
    private RequestFirebase requestFirebase;
    public static CircleImageView logoProfile;
    public static TextView nav_header_textView_userName;
    public static TextView nav_header_textView_conveyorName;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        setTitle("Viajes");
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Intent getDataProfile = getIntent();


        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);

        String imgLogoProfileString = getDataProfile.getStringExtra("IMG_LOGO");
        String nameProfile = getDataProfile.getStringExtra("NAME_PROFILE");
        String emailProfile = getDataProfile.getStringExtra("EMAIL_PROFILE");

        IMEI = getDataProfile.getStringExtra("IMEI");

        sharedPreferences.edit().putBoolean("SESSION_USER",true).commit();



        Bitmap imgLogoProfileBitmap = convertStringToBitmap(imgLogoProfileString);


        listRoles = new HashMap<>();

        listRoles.put("profile",new Role(1,"profile",false,R.id.nav_profile));
        listRoles.put("historicals",new Role(2,"historicals",false,R.id.nav_historical));
        listRoles.put("messages",new Role(3,"messages",false,R.id.nav_mensajes));
        listRoles.put("travels",new Role(4,"travels",false,R.id.nav_travels));
        listRoles.put("requests",new Role(5,"requests",false,R.id.nav_requests));
        listRoles.put("payments",new Role(6,"payments",false,R.id.nav_payments));
        navigationView = findViewById(R.id.nav_view);

        View headerView =  navigationView.getHeaderView(0);
        logoProfile = headerView.findViewById(R.id.nav_header_image);
        nav_header_textView_userName = headerView.findViewById(R.id.nav_header_textView_userName);
        nav_header_textView_conveyorName = headerView.findViewById(R.id.nav_header_textView_conveyorName);

        if(imgLogoProfileBitmap != null){
            logoProfile.setImageBitmap(imgLogoProfileBitmap);
        }

        if(nameProfile != null ){
            nav_header_textView_userName.setText(nameProfile);
        }
        if(emailProfile != null ){
            nav_header_textView_conveyorName.setText(emailProfile);
        }




        requestFirebase = new RequestFirebase(this);
        AppDatabase db = AppDatabase.getAppDatabase(getApplicationContext());
//        Log.e("RoleName1",""+db.UserDao().user().toString());

        hideItems( navigationView,  db,  this);
//        getRolesFirebase(navigationView);

//        floating
        menuFab = findViewById(R.id.menuFab);

        fab_edit = findViewById(R.id.fab_edit);
        fab_remove =findViewById(R.id.fab_remove);
        fab_add = findViewById(R.id.fab_add);
        fab_list = findViewById(R.id.fab_list);
        fab_save = findViewById(R.id.fab_save);



        menuFab.hideMenuButton(false);

        onActivityCreated();


        selectedItem( R.id.nav_travels );

        DrawerLayout drawer =  findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();


        navigationView.setNavigationItemSelectedListener(this);

    }




    public  void hideItems(NavigationView navigationView, AppDatabase db, Context context)
    {


        Menu nav_Menu = navigationView.getMenu();

        for ( Role role : db.RoleDao().listPermissions() ) {
            String nameItem = role.getName();
            try {



                role = listRoles.get(role.getName());
                role.setAccess(true);
                listRoles.put(role.getName(), role);

                nav_Menu.findItem(role.getIdResourse()).setVisible(true);

            }catch (Exception e){
                Toast.makeText(context,"error de roles con el item "+nameItem,Toast.LENGTH_LONG).show();
                Log.e("error de roles", e.getMessage());
            }

        }







    }




    private void selectActionButtonFloating(int menuItemSelected) {


        switch (menuItemSelected){

            case R.id.nav_profile:

                break;
            case R.id.nav_travels:

//                Toast.makeText(this,"viajes",Toast.LENGTH_LONG).show();
                break;
            case R.id.nav_mensajes:

//                Toast.makeText(this,"mensajes",Toast.LENGTH_LONG).show();
                break;
            case R.id.nav_historical:

                Toast.makeText(this,"historicos",Toast.LENGTH_LONG).show();
                break;
            case R.id.nav_payments:

//                Toast.makeText(this,"pagos",Toast.LENGTH_LONG).show();
                break;
            case R.id.nav_requests:



//                Toast.makeText(this,"solicitudes",Toast.LENGTH_LONG).show();
                break;
        }

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_travel, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        selectedItem( id );

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

//        displayView(item.getItemId());

        String title = !item.getTitle().toString().equals("") ?item.getTitle().toString():getString(R.string.app_name);
        MENU_ITEM_SELECTED = id;


        selectedItem(id);

        // set the toolbar title
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(title);
        }


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);

        return true;
    }

    private void selectedItem(int id) {

        if (id == R.id.nav_profile) {

            fragment = fragmentProfile;

            eventsProfile();

        } else if (id == R.id.nav_travels || id == R.id.action_travel) {



               arg.putString("IMEI",IMEI);
               fragment = fragmentTravels;
               if(!fragment.isAdded()){

                   fragment.setArguments(arg);
               }

            menuFab.setVisibility(View.GONE);


        } else if (id == R.id.nav_mensajes || id == R.id.action_message) {

            fragment = fragmentMessages;


        } else if (id == R.id.nav_historical) {

            arg.putString("IMEI",IMEI);

            fragment = fragmentHistorical;
            if(!fragment.isAdded()){

                fragment.setArguments(arg);
            }





        } else if (id == R.id.nav_payments) {

            fragment = fragmentPayments;


        } else if (id == R.id.nav_requests || id == R.id.action_new_request) {


            fragment = fragmentRequest;

            eventsRequest();

        }

        if (fragment != null) {
            FragmentTransaction ft = getFragmentManager().beginTransaction();
//            ft.remove(fragment);

            ft.replace(R.id.content_frame, fragment);
            ft.disallowAddToBackStack();
            ft.commit();
        }
    }

    private void eventsRequest() {
        menuFab.setVisibility(View.VISIBLE);
        menuFab.getMenuIconView().setImageDrawable(getResources().getDrawable( R.drawable.fab_add) );


        // array que contiene los fabButtons que tienen que verse en el menufloating
        listButton = new FloatingActionButton[]{fab_save,fab_add};

        // oculta todos los fabButton y muestra solo los fabButon que se le especifique al array
        showFloatingActionButtons(listButton);
        fab_add.setLabelText("Crear pasajeros");
        fab_add.setImageDrawable(getResources().getDrawable(R.drawable.ic_person_pin));

        fab_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                menuFab.close(true);

                fragmentRequest.createPassengers();
            }
        });
//

        fab_save.setLabelText("Enviar Solicitud");
        fab_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                menuFab.close(true);

                fragmentRequest.saveRequest();
            }
        });




    }

    private void eventsProfile() {

        menuFab.setVisibility(View.GONE);
        /*menuFab.getMenuIconView().setImageDrawable(getResources().getDrawable( R.drawable.fab_add) );

        // array que contiene los fabButtons que tienen que verse en el menufloating
        listButton = new FloatingActionButton[]{};

        // oculta todos los fabButton y muestra solo los fabButon que se le especifique al array
        showFloatingActionButtons(listButton);

        fab_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                menuFab.close(true);
                fragment_profile.Companion.enableInput(fragment_profile.Companion.getViewFragment());
            }
        });

        fab_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                menuFab.close(true);
                fragment_profile.Companion.disableInput(fragment_profile.Companion.getViewFragment());
            }
        });*/
    }

    private void showFloatingActionButtons(com.github.clans.fab.FloatingActionButton[] listButton) {
        ArrayList<FloatingActionButton> buttonsArrayList = new ArrayList<FloatingActionButton>();

        buttonsArrayList.add(this.fab_add);
        buttonsArrayList.add(this.fab_edit);
        buttonsArrayList.add(this.fab_remove);
        buttonsArrayList.add(this.fab_list);
        buttonsArrayList.add(this.fab_save);

        this.fab_edit.setVisibility(View.GONE);
        this.fab_save.setVisibility(View.GONE);
        this.fab_add.setVisibility(View.GONE);
        this.fab_remove.setVisibility(View.GONE);
        this.fab_list.setVisibility(View.GONE);

        for ( FloatingActionButton fab : listButton  ) {

            int index = buttonsArrayList.indexOf(fab);

            buttonsArrayList.get(index).setVisibility(View.GONE);
        }


    }


    public void onActivityCreated() {


        menuFab.setVisibility(View.GONE);
        menus.add(menuFab);

//

        fab_edit.setOnClickListener(clickListener);
        fab_remove.setOnClickListener(clickListener);
        fab_list.setOnClickListener(clickListener);
        fab_add.setOnClickListener(clickListener);
        fab_save.setOnClickListener(clickListener);


        int delay = 400;
        for (final FloatingActionMenu menu : menus) {
            mUiHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    menu.showMenuButton(false);
                }
            }, delay);
            delay += 150;
        }

        menuFab.setOnMenuButtonClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (menuFab.isOpened()) {

                    selectActionButtonFloating(MENU_ITEM_SELECTED);
//                    Toast.makeText(getApplicationContext(), menuRed.getMenuButtonLabelText(), Toast.LENGTH_SHORT).show();
                }

                menuFab.toggle(true);
            }
        });


    }



    public static boolean isConection(Context context)
    {
        boolean connected = true;
        ConnectivityManager connec = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        // Recupera todas las redes (tanto móviles como wifi)
        NetworkInfo[] redes = connec.getAllNetworkInfo();

        for (int i = 0; i < redes.length; i++) {
            // Si alguna red tiene conexión, se devuelve true
            if (redes[i].getState() == NetworkInfo.State.CONNECTED) {
                connected = false;
            }
        }
        return connected;
    }





    private View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.fab_add:
                    break;
                case R.id.fab_edit:

                    break;
                case R.id.fab_remove:

                    break;

                case R.id.fab_save:

                    break;
                case R.id.fab_list:

                    break;
            }
        }
    };

    private Bitmap convertStringToBitmap(String imgStringBase64  ) {
        Bitmap decodedImage = BitmapFactory.decodeResource(getResources(), R.drawable.perfil);
        try {
            if (!imgStringBase64.isEmpty()) {

                byte[] imageBytes = Base64.decode(imgStringBase64, Base64.DEFAULT);
                decodedImage = BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.length);
            }
        }
        catch(Exception error){
            Log.e("Error: ",error.toString());
        }


        return decodedImage;
    }




}