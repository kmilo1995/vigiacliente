package com.destinoseguro.vigiaespecialcliente.messages.fragments


import android.os.Bundle
import android.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.destinoseguro.vigiaespecialcliente.R
import com.destinoseguro.vigiaespecialcliente.messages.objects.listMessagesAdapter
import kotlinx.android.synthetic.main.fragment_messages.view.*

/**
 * Created by root on 16/11/17.
 */
class fragment_messages: Fragment() {

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View {

        val view:View = inflater!!.inflate(R.layout.fragment_messages,container,false)
        activity.title = "Mensajes"

        var listMessagesArrayList = ArrayList<String>()
        listMessagesArrayList.add("mensaje 1")
        listMessagesArrayList.add("mensaje 2")
        listMessagesArrayList.add("mensaje 3")

        var listMessagesAdapter = listMessagesAdapter(activity,listMessagesArrayList)

        var linearLayoutManager:RecyclerView.LayoutManager = LinearLayoutManager(activity)

        view.messagessRecycleView.layoutManager = linearLayoutManager


        view.messagessRecycleView.adapter = listMessagesAdapter


        return view

    }
}