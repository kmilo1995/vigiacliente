package com.destinoseguro.vigiaespecialcliente.messages.objects

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.destinoseguro.vigiaespecialcliente.R
import kotlinx.android.synthetic.main.card_view_messages_row.view.*
import java.util.zip.Inflater

/**
 * Created by root on 9/12/17.
 */
class listMessagesAdapter(private var context: Context,val listMessagesArrayList: ArrayList<String>):RecyclerView.Adapter<listMessagesAdapter.CustomerViewHolder>() {



    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int):CustomerViewHolder{

        var view = LayoutInflater.from(parent!!.context).inflate(R.layout.card_view_messages_row ,null)

        return CustomerViewHolder(view)
    }

    override fun onBindViewHolder(holder:CustomerViewHolder, position: Int) {

        var itemMenssage = listMessagesArrayList.get(position)

        holder.menssage.text = itemMenssage


    }

    override fun getItemCount(): Int {
        return listMessagesArrayList.size
    }


  class CustomerViewHolder(view:View):RecyclerView.ViewHolder(view){


      var menssage = view.menssage

  }

}