package com.destinoseguro.vigiaespecialcliente.historical.model

/**
 * Created by root on 21/12/17.
 */


class ResponseHistorical {

    var success: String? = null
    var data: List<HistoricalServer>? = null

    /**
     * No args constructor for use in serialization
     *
     */
    constructor() {}

    /**
     *
     * @param data
     * @param success
     */
    constructor(success: String, data: List<HistoricalServer>) : super() {
        this.success = success
        this.data = data
    }

}


class HistoricalServer {

    var id: String? = null
    var vestado: String? = null
    var direccionorigen: String? = null
    var direcciondestino: String? = null
    var fechainicio: String? = null
    var fechafinal: Any? = null
    var servicio: String? = null
    var vehiculo: String? = null
    var empresa: String? = null
    var pasajeros: String? = null
    var vciclico: String? = null
    var observacion: String? = null
    var distancia: String? = null

    /**
     * No args constructor for use in serialization
     *
     */
    constructor() {}

    /**
     *
     * @param id
     * @param vciclico
     * @param pasajeros
     * @param observacion
     * @param servicio
     * @param direcciondestino
     * @param fechafinal
     * @param vestado
     * @param empresa
     * @param direccionorigen
     * @param vehiculo
     * @param fechainicio
     */
    constructor(id: String, vestado: String, direccionorigen: String, direcciondestino: String, fechainicio: String, fechafinal: Any, servicio: String, vehiculo: String, empresa: String, pasajeros: String, vciclico: String, observacion: String, distancia: String) : super() {
        this.id = id
        this.vestado = vestado
        this.direccionorigen = direccionorigen
        this.direcciondestino = direcciondestino
        this.fechainicio = fechainicio
        this.fechafinal = fechafinal
        this.servicio = servicio
        this.vehiculo = vehiculo
        this.empresa = empresa
        this.pasajeros = pasajeros
        this.vciclico = vciclico
        this.observacion = observacion
        this.distancia = distancia
    }

    override fun toString(): String {
        return "HistoricalServer(id=$id, vestado=$vestado, direccionorigen=$direccionorigen, direcciondestino=$direcciondestino, fechainicio=$fechainicio, fechafinal=$fechafinal, servicio=$servicio, vehiculo=$vehiculo, empresa=$empresa, pasajeros=$pasajeros, vciclico=$vciclico, observacion=$observacion, distancia=$distancia)"
    }


}

