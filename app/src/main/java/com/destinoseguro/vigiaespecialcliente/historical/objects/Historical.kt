package com.destinoseguro.vigiaespecialcliente.historical.objects

/**
 * Created by root on 29/11/17.
 */
class Historical {

    var code: String? = null
    var origin: String? = null
    var destination: String? = null
    var applicantName: String? = null
    var compani: String? = null
    var state: String? = null

    constructor(code: String?, origin: String?, destination: String?, applicantName: String?, compani: String?, state: String?) {
        this.code = code
        this.origin = origin
        this.destination = destination
        this.applicantName = applicantName
        this.compani = compani
        this.state = state
    }

    constructor(code: String?, state: String?) {
        this.code = code
        this.state = state
    }


}