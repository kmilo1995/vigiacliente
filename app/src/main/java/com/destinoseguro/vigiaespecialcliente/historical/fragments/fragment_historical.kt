package com.destinoseguro.vigiaespecialcliente.historical.fragments

import android.os.Bundle
import android.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.destinoseguro.vigiaespecialcliente.R
import com.destinoseguro.vigiaespecialcliente.historical.model.EndPointHistoricals
import com.destinoseguro.vigiaespecialcliente.historical.model.HistoricalServer
import com.destinoseguro.vigiaespecialcliente.historical.model.ResponseHistorical
import com.destinoseguro.vigiaespecialcliente.historical.objects.listHistoricalAdapter
import com.destinoseguro.vigiaespecialcliente.restApi.RestApiAdapter
import com.destinoseguro.vigiaespecialcliente.travels.model.Travel
import kotlinx.android.synthetic.main.fragment_historical.view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


/**
 * Created by root on 16/11/17.
 */
class fragment_historical: Fragment() {

    private val restApiAdapter = RestApiAdapter()
    private val retrofit = restApiAdapter.conectionApiRest()
    private var IMEI:String? = ""

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {



            IMEI = arguments.getString("IMEI")

            val view:View = inflater!!.inflate(R.layout.fragment_historical,container,false)

            loadListHistoricalServer()

        return view
    }




    fun loadListHistoricalServer(){


        var response: Call<ResponseHistorical> = retrofit.create(EndPointHistoricals::class.java).historicals


        response!!.enqueue(object : Callback<ResponseHistorical> {

            override fun onResponse(call: Call<ResponseHistorical>?, response: Response<ResponseHistorical>?) {

                if (response != null) {

                    var listTravels = ArrayList<Travel>()


                    for (travel: HistoricalServer in response.body()!!.data!!){

                        var newTravel = Travel(
                                travel.empresa,
                                travel.id,
                                travel.vestado,
                                travel.direccionorigen,
                                travel.direcciondestino,
                                "solicitante", // falta definir en planeacion
                                travel.fechainicio,
                                travel.fechafinal.toString(),
                                null,
                                travel.distancia,
                                travel.servicio,
                                travel.vehiculo,
                                "tipo de operacion", // falta definir en planeacion
                                travel.observacion,
                                travel.pasajeros.toString(),
                                travel.vciclico!!.toBoolean(),
                                true
                        )

                        listTravels.add(newTravel)



                    }

                    var adapterHistorical = listHistoricalAdapter(activity,listTravels,IMEI!!)

                    val layoutManager:RecyclerView.LayoutManager = LinearLayoutManager(activity)

                    view.historical_recyclerView.layoutManager = layoutManager
                    view.historical_recyclerView.setHasFixedSize(false)

                    view.historical_recyclerView.adapter = adapterHistorical

                }

            }

            override fun onFailure(call: Call<ResponseHistorical>?, t: Throwable?) {
                if (t != null) {
                    Log.e("fragment_historical","loadListHistoricalServer onFailure "+ t.message)
                }
            }

        })

    }
}