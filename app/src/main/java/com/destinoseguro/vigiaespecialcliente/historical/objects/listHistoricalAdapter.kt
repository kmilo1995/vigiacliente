package com.destinoseguro.vigiaespecialcliente.historical.objects

import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.support.annotation.RequiresApi
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.RotateAnimation
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import com.destinoseguro.vigiaespecialcliente.R
import com.destinoseguro.vigiaespecialcliente.R.id.imageViewExpand
import com.destinoseguro.vigiaespecialcliente.travels.activitys.calificationServices
import com.destinoseguro.vigiaespecialcliente.travels.activitys.travel
import com.destinoseguro.vigiaespecialcliente.travels.model.Travel
import com.destinoseguro.vigiaespecialcliente.travels.objects.ExpandAndCollapseViewUtil
import com.github.clans.fab.FloatingActionButton
import kotlinx.android.synthetic.main.card_view_historical_row.view.*
import javax.xml.datatype.DatatypeConstants.DURATION

/**
 * Created by root on 29/11/17.
 */
class listHistoricalAdapter(private val context: Context, private val listHistoricalArrayList: List<Travel>,IMEI:String): RecyclerView.Adapter<listHistoricalAdapter.CustomerViewHolder>() {

    private val DURATION = 250
    private val IMEI = IMEI

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): CustomerViewHolder {

       var view: View = LayoutInflater.from(parent!!.context).inflate(R.layout.card_view_historical_row,null)

        return CustomerViewHolder(view)
    }


    @RequiresApi(Build.VERSION_CODES.M)
    override fun onBindViewHolder(holder: CustomerViewHolder?, position: Int) {

        val itemCard = listHistoricalArrayList!![position]

        if (holder != null) {

            holder.textCode!!.text = "VSE-"+itemCard.code
            holder.historicalCardViewTextOrigen!!.text = itemCard.origin
            holder.historicalCardViewTextDestination!!.text = itemCard.destination
            holder.textApplicant!!.text = itemCard.applicantName
            holder.textCompany!!.text = itemCard.companyName
            holder.travelLabelState!!.text = itemCard.state

            if(Build.VERSION.SDK_INT == Build.VERSION_CODES.LOLLIPOP_MR1 || Build.VERSION.SDK_INT == Build.VERSION_CODES.LOLLIPOP){
                holder.travelLabelState!!.setBackgroundColor(ContextCompat.getColor(context,R.color.color_state_travel_finalized))

            }
            else{
                holder.travelLabelState!!.setBackgroundColor(context.getColor(R.color.color_state_travel_finalized))
            }


            holder.showDetail!!.setOnClickListener(View.OnClickListener {

                toggleDetails(holder)

            })

            holder.showDetailFabButton!!.setOnClickListener(View.OnClickListener {


                var intent = Intent(context, travel::class.java)

                val bundle = Bundle()

                Log.e("item card pasajeros",""+itemCard.toString())

                bundle.putParcelable("travel",itemCard)
                bundle.putString("idTravel",itemCard.code)
                bundle.putString("IMEI", IMEI)
                intent.putExtras(bundle)

                context.startActivity(intent)


//

            })

            holder.historicalCalification!!.setOnClickListener(View.OnClickListener {


                var intent = Intent(context, calificationServices::class.java)

                val bundle = Bundle()

                Log.e("item card pasajeros",""+itemCard.toString())

                bundle.putParcelable("travel",itemCard)
                bundle.putString("idTravel",itemCard.code)
                bundle.putString("IMEI",IMEI)
                intent.putExtras(bundle)

                context.startActivity(intent)


//

            })

        }

    }

    override fun getItemCount(): Int {
        return listHistoricalArrayList.size
    }




    inner class CustomerViewHolder(view:View):RecyclerView.ViewHolder(view) {



        var showDetail:LinearLayout? = view.showDetail
        var linearLayoutDetails:LinearLayout? = view.linearLayoutDetails
        var imageViewExpand:ImageView? = view.imageViewExpand
        var textCode:TextView? = view.historicalCardViewTextCode
        var textApplicant:TextView? = view.historicalCardViewTextApplicant
        var historicalCardViewTextOrigen:TextView? = view.historicalCardViewTextOrigen
        var historicalCardViewTextDestination:TextView? = view.historicalCardViewTextDestination
        var textCompany:TextView? = view.historicalCardViewTextCompany
        var travelLabelState: TextView? = view.historicalLabelState
        var showDetailFabButton: FloatingActionButton? = view.historicalShowDetailFabButton
        var historicalCalification: FloatingActionButton? = view.historicalCalification

    }


    fun toggleDetails(holder: CustomerViewHolder?) {
        if (holder != null) {
            if (holder.linearLayoutDetails!!.getVisibility() == View.GONE) {
                ExpandAndCollapseViewUtil.expand(holder.linearLayoutDetails, DURATION)
                holder.imageViewExpand!!.setImageResource(R.drawable.ic_arrow_up)
                rotate(-180.0f,holder)

                var margin = 44
                val lp = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)
                lp.setMargins(margin, margin, margin, 0)
                holder.showDetail!!.layoutParams = lp
            } else {
                ExpandAndCollapseViewUtil.collapse(holder.linearLayoutDetails, DURATION)
                holder.imageViewExpand!!.setImageResource(R.drawable.ic_arrow_down)
                rotate(180.0f,holder)
                var margin = 44
                val lp = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)
                lp.setMargins(margin, margin, margin, margin)
                holder.showDetail!!.layoutParams = lp
//                holder.linearLayoutDetails!!.visibility = View.GONE
            }
        }
    }

    private fun rotate(angle: Float,holder: CustomerViewHolder?) {
        val animation = RotateAnimation(0.0f, angle, Animation.RELATIVE_TO_SELF, 0.5f,
                Animation.RELATIVE_TO_SELF, 0.5f)
        animation.fillAfter = true
        animation.duration = DURATION.toLong()
        if (holder != null) {
            holder.imageViewExpand!!.startAnimation(animation)
        }
    }


}