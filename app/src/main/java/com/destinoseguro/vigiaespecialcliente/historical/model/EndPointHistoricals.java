package com.destinoseguro.vigiaespecialcliente.historical.model;


import com.destinoseguro.vigiaespecialcliente.register.model.ResponseRegister;
import com.destinoseguro.vigiaespecialcliente.restApi.ConstantsRestApi;
import com.destinoseguro.vigiaespecialcliente.travels.model.ResponseAssignment;
import com.destinoseguro.vigiaespecialcliente.travels.model.ResponsePassengers;
import com.destinoseguro.vigiaespecialcliente.travels.model.ResponseTravel;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by root on 21/12/17.
 */

public interface EndPointHistoricals {


    @POST(ConstantsRestApi.GET_HISTORICALS)
    Call<ResponseHistorical>getHistoricals();


    @FormUrlEncoded
    @POST(ConstantsRestApi.GET_ASSIGNMENT)
    Call<ResponseAssignment> getAsignment(@Field("idservicio") String idservicio);

    @FormUrlEncoded
    @POST(ConstantsRestApi.GET_PASSENGERS)
    Call<ResponsePassengers> getPassengers(@Field("idservicio") String idservicio);

    @FormUrlEncoded
    @POST(ConstantsRestApi.SET_CALIFICATION_SERVICE)
    Call<ResponseRegister> setCalificationService(
            @Field("imei") String imei,
            @Field("calificacion")String calification,
            @Field("observacion")String observation,
            @Field("idservicio")String id_service
            );

}
