package com.destinoseguro.vigiaespecialcliente.travels.model

/**
 * Created by root on 20/12/17.
 */
class ResponsePassengers {

    var isSuccess: Boolean = false
    var data: List<PassengersServer>? = null

    /**
     * No args constructor for use in serialization
     *
     */
    constructor() {}

    /**
     *
     * @param data
     * @param success
     */
    constructor(success: Boolean, data: List<PassengersServer>) : super() {
        this.isSuccess = success
        this.data = data
    }

}


class PassengersServer{


      var id: String? = null
      var pasajero: String? = null
      var direccionorigen: String?= null
      var direcciondestino: String?= null
      var fechainicio: String?= null
      var observacion: String?= null
      var calificacion: String? = null
      var celular: String? = null
      var documento: String? = null

    /**
     * No args constructor for use in serialization
     *
     */
     constructor() {}

    /**
     *
     * @param id
     * @param observacion
     * @param calificacion
     * @param direcciondestino
     * @param direccionorigen
     * @param fechainicio
     * @param pasajero
     */
    constructor(id: String, pasajero: String, direccionorigen: String, direcciondestino: String, fechainicio: String, observacion: String, calificacion: String,documento:String,celular:String): super(){

        this.id = id
        this.pasajero = pasajero
        this.direccionorigen = direccionorigen
        this.direcciondestino = direcciondestino
        this.fechainicio = fechainicio
        this.observacion = observacion
        this.calificacion = calificacion
        this.documento = documento
        this.celular = celular
    }

    override fun toString(): String {
        return "PassengersServer(id=$id, pasajero=$pasajero, direccionorigen=$direccionorigen, direcciondestino=$direcciondestino, fechainicio=$fechainicio, observacion=$observacion, calificacion=$calificacion, celular=$celular, documento=$documento)"
    }


}