package com.destinoseguro.vigiaespecialcliente.travels.fragments

import android.os.Bundle
import android.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.destinoseguro.vigiaespecialcliente.R
import com.destinoseguro.vigiaespecialcliente.RoomAppDatabase.AppDatabase
import com.destinoseguro.vigiaespecialcliente.dashboard.activitys.dashboard
import com.destinoseguro.vigiaespecialcliente.restApi.RestApiAdapter
import com.destinoseguro.vigiaespecialcliente.travels.model.EndPointsTravel
import com.destinoseguro.vigiaespecialcliente.travels.model.ResponseTravel
import com.destinoseguro.vigiaespecialcliente.travels.model.Travel
import com.destinoseguro.vigiaespecialcliente.travels.model.TravelServer
import com.destinoseguro.vigiaespecialcliente.travels.objects.listTravelsAdapter
import kotlinx.android.synthetic.main.fragment_travels_list.*
import kotlinx.android.synthetic.main.fragment_travels_list.view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


/**
 * Created by root on 16/11/17.
 */
class fragment_list_travels : Fragment() {

    private val restApiAdapter = RestApiAdapter()
    private val retrofit = restApiAdapter.conectionApiRest()

    private var IMEI:String? = ""

    private var vista:View? = null



    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        IMEI = arguments.getString("IMEI")

        val view:View = inflater!!.inflate(R.layout.fragment_travels_list, container, false)

        vista = view

        activity.title = "Viajes"
        val db:AppDatabase = AppDatabase.getAppDatabase(activity)
//        db.TravelDao().nukeTable()
        var  listTravels = db.TravelDao().getAll()


        validateInternet()

        loadListTravel(listTravels,db)
//        Toast.makeText(activity,listTravels.size.toString(),Toast.LENGTH_SHORT).show()
//



        return vista
    }

    private fun validateInternet() {

        if (dashboard.isConection(activity)){
           // vista!!.linearLayoutMessageNotConection.visibility =View.VISIBLE
        }else{
           // vista!!.linearLayoutMessageNotConection.visibility =View.GONE
        }

    }

    private fun loadListTravel(listTravels: List<Travel>, db: AppDatabase) {

        if (listTravels.isNotEmpty()){

            loadListTravelInternalDataBase(listTravels)
        }else{

            loadListTravelServer(db)
        }
    }




    private fun loadListTravelInternalDataBase(listTravels: List<Travel>) {

        printDataInRecycle(listTravels)

    }




    fun loadListTravelServer(db: AppDatabase) {


        var response: Call<ResponseTravel> = retrofit.create(EndPointsTravel::class.java).travel


         response!!.enqueue(object :Callback<ResponseTravel>{

             override fun onResponse(call: Call<ResponseTravel>?, response: Response<ResponseTravel>?) {

                 if (response != null) {

                   var listTravels = ArrayList<Travel>()


                         for (travel: TravelServer in response.body()!!.data!!){

                             val newTravel = Travel(
                                     travel.empresa,
                                     travel.id,
                                     travel.vestado,
                                     travel.direccionorigen,
                                     travel.direcciondestino,
                                     "solicitante",
                                     travel.fechainicio,
                                     travel.fechafinal.toString(),
                                     travel.ruta as String?,
                                     travel.distancia,
                                     travel.servicio,
                                     travel.vehiculo,
                                     "tipo operacion",
                                     travel.observacion,
                                     travel.pasajeros,
                                     travel.vciclico?.toBoolean(),
                                     true
                             )

                             listTravels.add(newTravel)


                         }

                     if(activity != null){

                         loadInternalDataBaseWithDataServer(listTravels)

                         printDataInRecycle(listTravels)
                     }


                 }

             }

             private fun loadInternalDataBaseWithDataServer(listTravels: ArrayList<Travel>) {

                 for (tavel:Travel in listTravels){

                     db.TravelDao().insert(tavel)
                 }

             }

             override fun onFailure(call: Call<ResponseTravel>?, t: Throwable?) {
                 if (t != null) {
                    // linearLayoutMessageNotConection.visibility = View.VISIBLE
                     Log.e("fragment_list_travels","loadListTravelServer onFailure "+ t.message)
                 }
             }

         })

     }

    private fun printDataInRecycle(listTravels: List<Travel>) {

        val adapterTravel = listTravelsAdapter(activity,listTravels,IMEI!!)

        val layoutManager:RecyclerView.LayoutManager = LinearLayoutManager(activity) as RecyclerView.LayoutManager

        vista!!.travels_recyclerView.layoutManager = layoutManager
        //vista!!.travels_recyclerView.setHasFixedSize(true)
        vista!!.travels_recyclerView.adapter = adapterTravel

    }
}