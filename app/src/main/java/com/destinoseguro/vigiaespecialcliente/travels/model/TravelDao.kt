package com.destinoseguro.vigiaespecialcliente.travels.model

import android.arch.persistence.room.*
import com.destinoseguro.vigiaespecialcliente.travels.activitys.travel

/**
 * Created by root on 4/12/17.
 */
@Dao
interface TravelDao {

   @Query("SELECT * FROM table_travel WHERE state <> 'Finalizado' ")
    fun getAll(): List<Travel>

    @Query("SELECT * FROM table_travel WHERE state  LIKE 'Finalizado'")
    fun getAllHistorical(): List<Travel>


    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(vararg travel: Travel)


    @Delete
    fun delete(travel: Travel)


   @Query("SELECT * FROM table_travel WHERE id = (SELECT MAX(ID) FROM table_travel); ")
   fun getLastId():Int


    @Query("DELETE FROM table_travel")
    fun nukeTable()

}