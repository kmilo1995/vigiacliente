package com.destinoseguro.vigiaespecialcliente.travels.fragments



import android.Manifest
import android.animation.ValueAnimator
import android.annotation.SuppressLint
import android.app.ProgressDialog
import android.content.Context
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.*
import android.support.v4.app.Fragment
import android.preference.PreferenceManager
import android.support.annotation.RequiresApi
import android.support.v4.app.ActivityCompat
import android.support.v4.app.FragmentActivity
import android.support.v4.content.ContextCompat
import android.support.v7.widget.CardView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.LinearInterpolator
import android.widget.*
import com.destinoseguro.vigiaespecialcliente.R
import com.destinoseguro.vigiaespecialcliente.firebase.Firebase
import com.destinoseguro.vigiaespecialcliente.google_maps_api.GeoApiContextInstance
import com.destinoseguro.vigiaespecialcliente.modulos_pruebas.MapsActivity
import com.destinoseguro.vigiaespecialcliente.restApi.RestApiAdapter
import com.destinoseguro.vigiaespecialcliente.travels.model.Destino
import com.destinoseguro.vigiaespecialcliente.travels.model.EndPointsTravel
import com.destinoseguro.vigiaespecialcliente.travels.model.Origen
import com.destinoseguro.vigiaespecialcliente.travels.model.ResponseRute
import com.destinoseguro.vigiaespecialcliente.travels.objects.*
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import com.google.android.gms.maps.model.JointType.ROUND
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import com.google.maps.*
import com.google.maps.model.DirectionsResult
import com.google.maps.model.DistanceMatrix
import com.google.maps.model.TravelMode
import kotlinx.android.synthetic.main.fragment_travel_map.view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.UnsupportedEncodingException
import java.util.ArrayList
import java.util.concurrent.CountDownLatch

/**
 * created by root on 30/11/17.
 */
class FragmentTravelMap : Fragment(),
        OnMapReadyCallback,
        DirectionFinderListener,
        GoogleMap.OnInfoWindowClickListener,
        GoogleMap.OnInfoWindowCloseListener {



    private var mMap: GoogleMap? = null
    private var markers: ArrayList<Marker> = ArrayList()
    private var polylinePaths:List<Polyline>? = ArrayList()
    private var progressDialog: ProgressDialog? = null
    private var routeCache:Route? = null
    private var listOrigins: List<Origen>? = null
    private var listDestinations: List<Destino>? = null
    private var stateService:String = ""
    private var IMEI:String = ""
    private var idService:String? = ""
    private var loadMap:Boolean = true
    private var cardViewDetailInfoPassengerRoute: CardView? =null
    private var cardPassengerNameRoute: TextView? =null
    private var estimatedTimeBetweenDriverAndPassenger: TextView? =null
    private var estimatedDistanceBetweenDriverAndPassenger: TextView? =null
    private var vehiclePosition: List<String>? =null
    private var btnTraffic: ImageButton? =null
    private var locationManager: LocationManager? =null
    private var ZoomMyUbication: Boolean? =false
    private var activateInRuteMode: Boolean? =false

    @SuppressLint("MissingPermission")
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {


        // cargar la vista dentro del fragment
        val view:View?   = inflater.inflate(R.layout.fragment_travel_map,container,false)

        // manejador de posiciones
        locationManager = activity!!.getSystemService(Context.LOCATION_SERVICE) as LocationManager

        // iniciar los views
        cardViewDetailInfoPassengerRoute = view!!.findViewById(R.id.cardViewDetailInfoPassengerRute)
        cardPassengerNameRoute = view.findViewById(R.id.cardPassengerNameRute)
        estimatedTimeBetweenDriverAndPassenger = view.findViewById(R.id.cardPassenger_estimatedTime)
        estimatedDistanceBetweenDriverAndPassenger = view.findViewById(R.id.cardPassenger_estimatedDistance)
        btnTraffic = view.findViewById(R.id.btnTraffic)
        view.indicatedTraffic.visibility = View.GONE
        view.indicatedModeInRute.visibility = View.GONE


        // boton que permite dibujar en el mapa el punto donde se localiza el usuario
        view.fab_my_location.setOnClickListener( {

            gpsUpdate()
            ZoomMyUbication = true
        })


        gpsUpdate()

        // obtiene los argumentos de la actividad travel
        val bundle = arguments
        // obtine el estado del servicio
        stateService = bundle!!.getString("STATE_SERVICE")
        // obtiene el imei del dispositivo
        IMEI = bundle.getString("IMEI")
        // obtiene el id del servicio
        idService = bundle.getString("ID_SERVICE")

        // Devuelve un FragmentManager privado para colocar y administrar Fragmentos dentro de este Fragmento. obtiene el fragmento del mapa
        val mapFragment = childFragmentManager.findFragmentById((R.id.map)) as SupportMapFragment
        // configurar la devolución de llamada en el fragmento
        mapFragment.getMapAsync(this)

        // valida si la ruta se guardo almenos una vez  de lo contrario la consulta al servidor
        if (routeCache == null){
        // consulta al servidor una ruta de viaje
            loadRouteInMap()
            loadMap = false
        }


        // onclick del boton que activa el trafico en el mapa
        view.btnTraffic?.setOnClickListener( {
            //habilita el trafico
            when(mMap!!.isTrafficEnabled){
                true-> {

                    mMap!!.isTrafficEnabled = false
                    view.indicatedTraffic.visibility = View.GONE
                    // muestra un mensaje informando al ususario sobre la desactivacion del trafico
                    Toast.makeText(activity,R.string.textDisabledTrafic,Toast.LENGTH_SHORT).show()

                }
                false-> {

                    mMap!!.isTrafficEnabled = true
                    view.indicatedTraffic.visibility = View.VISIBLE

                    // muestra un mensaje informando al ususario sobre la activacion del trafico
                    Toast.makeText(activity,R.string.textActivateTrafic,Toast.LENGTH_SHORT).show()
                }
            }

        })

        view.btnModeInRute?.setOnClickListener( {
            //habilita el trafico
            when(activateInRuteMode){
                true-> {

                    view.indicatedModeInRute.visibility = View.GONE
                    activateInRuteMode = false
                    // muestra un mensaje informando al ususario sobre la desactivacion del trafico
                    Toast.makeText(activity,R.string.textDisabledModeInRute,Toast.LENGTH_SHORT).show()

                }
                false-> {

                    view.indicatedModeInRute.visibility = View.VISIBLE
                    activateInRuteMode = true
                    // muestra un mensaje informando al ususario sobre la activacion del trafico
                    Toast.makeText(activity,R.string.textActivateModeInRute,Toast.LENGTH_SHORT).show()
                }
            }

        })

        return view
    }




     private val ACCESS_FINE_LOCATION:Int = 3
    private fun gpsUpdate(){


//
        // Here, thisActivity is the current activity
        if (ContextCompat.checkSelfPermission(activity!!,
                        android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
//            Toast.makeText(activity,"checkSelfPermission",Toast.LENGTH_SHORT).show()
            // Should we show an explanation?
            if (shouldShowRequestPermissionRationale(
                            android.Manifest.permission.ACCESS_FINE_LOCATION)) {

//                Toast.makeText(activity,"shouldShowRequestPermissionRationale",Toast.LENGTH_SHORT).show()

                // Show an expanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
                //
                requestPermissions(
                        arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION),
                        ACCESS_FINE_LOCATION)


            } else {

                Toast.makeText(activity,"Es necesario que active los permisos de ubucacion",Toast.LENGTH_SHORT).show()
                // No explanation needed, we can request the permission.
                requestPermissions(
                        arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION),
                        ACCESS_FINE_LOCATION)


                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        } else {
//            Toast.makeText(activity,"checkSelfPermission else",Toast.LENGTH_SHORT).show()
            requestPermissions(
                    arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION),
                    ACCESS_FINE_LOCATION)

        }


    }


    private var locationListenerGps = object: LocationListener {
        @SuppressLint("MissingPermission")
        override fun onLocationChanged(location: Location?) {
           Log.e("GPS",location.toString())
            if (location != null) {
                mMap!!.isMyLocationEnabled = true
                if (ZoomMyUbication!!){

                    mMap!!.moveCamera(CameraUpdateFactory.newCameraPosition(CameraPosition(LatLng(location.latitude,location.longitude),18f,18f,18f)))
                }
                locationManager!!.removeUpdates(this)

            }

        }

        override fun onStatusChanged(provider: String?, status: Int, extras: Bundle?) {}

        override fun onProviderEnabled(provider: String?) {}

        override fun onProviderDisabled(provider: String?) {}


    }



    @SuppressLint("MissingPermission")
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)


        Log.e("pemiso si","= "+requestCode.toString())



        when(requestCode){
            ACCESS_FINE_LOCATION->{

                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED && locationManager!!.isProviderEnabled(LocationManager.GPS_PROVIDER)) {

                    locationManager!!.requestLocationUpdates(
                            LocationManager.GPS_PROVIDER, 2000, 10f, locationListenerGps)
                } else {

                    //                    solicitarPermisosManual();
                    Log.e("rest", "permission denied")
                    Toast.makeText(activity,"Es necesario que active los permisos de ubucacion o Active el Gps",Toast.LENGTH_SHORT).show()                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }


            }


        }
    }


    /**
     * La tarea del metodo sendRequest es hacer ua url mediante la lista de origenes y la lista de destinos
     * para realizar una solicitud a google y obtener
     * la ruta con cada una de sus marcas
     *
     * @param origin contiene la lista de puntos que corresponden al origen
     * @param destination contiene la lista de puntos que corresponden a losdestinos
     */
    private fun sendRequest(origin: List<Origen>?, destination: List<Destino>?) {

        // obtiene la ultima marca de la lista de destinos
        val latestDestination = destination!!.size - 1
        // guarda el indice de la primera posicion de los origenes
        val firstOrigin = 0
        // se obtiene un string que hace parte de la url de la peticion y contiene cada marca en la ruta
        val wayPoints = getWayPoints(origin,destination)

             Log.e("destination size", destination.size.toString())

        try {
            if (destination != null) {
                DirectionFinder(
                        this,
                        origin!!.get(firstOrigin).location,
//                        origin!!.get(origin.size-1).location,
                        destination.get(latestDestination).location,
                        wayPoints,
                        2
                ).execute()
            }
        } catch (e: UnsupportedEncodingException) {
            e.printStackTrace()
        }

    }



    /**
     * La tarea del metodo getWayPoints es tranformar dos listas de puntos o marcas
     * en un string que seara parte de una url
     *
     * @param origin lista de origenes
     * @param destination lista de destinos
     * @return String contiene los waypoint de la ruta
     */
    private fun getWayPoints(origin: List<Origen>?, destination: List<Destino>?): String? {


        var wayPonts = ""
        // validacion de origenes
        if (origin != null) {
            // recorrido de cada origen
            for (pointsOrigin:Origen in origin.iterator()){
                // se descarta la primera posision solo se nececitan los puntos intermedios
                if (pointsOrigin.orden != 1 )
                    // se concatena la cordenada de cada punto y se separa por |
                    wayPonts += pointsOrigin.location+"|"

            }
        }

        if (destination != null) {
            for (pointsDestination:Destino in destination.iterator()){

                if ( pointsDestination.orden != destination.size){

                    if (pointsDestination.orden != destination.size-1){

                        wayPonts += pointsDestination.location+"|"
                    }else{
                        wayPonts += pointsDestination.location
                    }
                }



            }
        }

        return wayPonts
    }




    /**
     * La tarea del metodo loadRouteInMap es consumir el servicio getRoute
     * y cargar las listas de listOrigins y listDestinations se usa la libreria retrofit
     *
     */
    private fun loadRouteInMap() {

        // llama la instancia del adaptador
        val resApiAdapter = RestApiAdapter()
        // llama la conecxion
        val retrofit = resApiAdapter.conectionApiRest()
        // llama la url por medio del metodo getRoute que resive como parametro un imei
        val response: Call<ResponseRute> = retrofit.create(EndPointsTravel::class.java).getRoute( idService )
        // getiona la respuesta asincronicamente por medio de un callback
        response.enqueue(object : Callback<ResponseRute>{
            // retorna la respuesta
            override fun onResponse(call: Call<ResponseRute>?, response: Response<ResponseRute>?) {

                if (response != null) {

                    listOrigins = response.body()?.data?.origen
                    listDestinations = response.body()?.data?.destino
                    // pasa por parametros las listas con los puntos para la creacion de la url de googlemaps
                    sendRequest(listOrigins,listDestinations)

                    Log.e("response rute", response.body()?.data?.destino?.get(0)?.document.toString())
                }
            }
            // se ejecuta en caso de problemas con la peticion al servidor
            override fun onFailure(call: Call<ResponseRute>?, t: Throwable?) {

                if (t != null) {
                    Log.e("response rute",t.message.toString())
                }
            }
        })
    }





    override fun onDirectionFinderStart() {
        progressDialog = ProgressDialog.show(context, "Please wait.",
                "Finding direction..!", true)

        if (markers != null) {
            for (marker in markers) {
                marker.remove()
            }
        }


        if (polylinePaths != null) {
            for (polyline in polylinePaths!!) {
                polyline.remove()
            }
        }
    }



    override fun onDirectionFinderSuccess(routes: MutableList<Route>?) {

        progressDialog!!.dismiss()
        polylinePaths = ArrayList()
//        destinationMarkers = ArrayList<Marker>()

        if (routes != null) {
            Log.e("rutas", routes.size.toString())
        }

        printMarketsRoute()

        if (routes != null) {
            for (route in routes) {
                mMap!!.moveCamera(CameraUpdateFactory.newLatLngZoom(route.startLocation, 11.40f))
                routeCache = route

                val polylineOptions = PolylineOptions().geodesic(true).color(Color.BLUE).width(11f)

                for (i in 0 until route.points.size)
                    polylineOptions.add(route.points[i])

                (polylinePaths as ArrayList<Polyline>).add(mMap!!.addPolyline(polylineOptions))
            }
        }

    }




    private fun printMarketsRoute() {

        var icon = bitmapDescriptorFromVector(activity, R.drawable.ic_person_start)

        for (pointOrigin:Origen in listOrigins!!.iterator()){

            if (pointOrigin.orden > 1 ){

                icon = bitmapDescriptorFromVector(activity, R.drawable.ic_person_people_waiting)
            }

            val latLngString = pointOrigin.location!!.split(",")
            val latitude = latLngString[0].toDouble()
            val longitude = latLngString[1].toDouble()

            if(stateService.equals("En Transito")){

            }

            markers.add(
                    mMap!!.addMarker(
                            MarkerOptions()
                                    .icon(icon)
                                    .title(pointOrigin.passenger+" #"+pointOrigin.orden)
                                    .snippet(pointOrigin.geolocate)
                                    .position(LatLng(latitude, longitude))))

        }

        for (pointDestination:Destino in listDestinations!!.iterator()){

            val latLngString = pointDestination.location!!.split(",")
            val latitude = latLngString[0].toDouble()
            val longitude = latLngString[1].toDouble()

            markers.add(
                    mMap!!.addMarker(
                            MarkerOptions()
                                    .icon(bitmapDescriptorFromVector(activity, R.drawable.ic_person_end))
                                    .title(pointDestination.geolocate)
                                    .position(LatLng(latitude, longitude))))
        }
    }




    val geoApiContext  = GeoApiContextInstance().instance()!!

    override fun onInfoWindowClick(marker: Marker?) {

        var positionPassenger:Int? = null

        try {

             positionPassenger = (marker!!.id[1]-2).toString().toInt()

        }catch (e:NumberFormatException){
            Log.e("fragment map ",e.message)
        }

        if (positionPassenger != null){

            Log.e("puntero"," = "+positionPassenger)
            cardViewDetailInfoPassengerRoute!!.visibility = View.VISIBLE
            cardPassengerNameRoute?.text = listOrigins?.get(positionPassenger)?.passenger
        }


        if (marker != null) {
            marker.setInfoWindowAnchor(0.0f,20.0f)
        }



        try{

            val latLogVehicle = LatLng(vehiclePosition!![0].toDouble(),vehiclePosition!![1].toDouble())

            val origins = arrayOf("${latLogVehicle.latitude},${latLogVehicle.longitude}")
            val destinations = arrayOf("${marker!!.position.latitude},${marker.position.longitude}")



            val requestMatrixApi = DistanceMatrixApi.getDistanceMatrix(geoApiContext,origins,destinations)

            val latch =  CountDownLatch(1)
            requestMatrixApi.setCallback(object : PendingResult.Callback<DistanceMatrix> {

                override fun onResult(result: DistanceMatrix?) {


                    if (result != null) {

                        activity!!.runOnUiThread {
                            //Cambiar controles
                            try {

                                estimatedDistanceBetweenDriverAndPassenger?.text =  result.rows[0].elements[0].distance.humanReadable
                                estimatedTimeBetweenDriverAndPassenger?.text = result.rows[0].elements[0].duration.humanReadable
                            }catch (e:Exception){

                            }
                        }


                    }

                    latch.countDown()
                }

                override fun onFailure(e: Throwable?) {
                    if (e != null) {
                        Log.e("distancia error ",e.message)


                    }
                    latch.countDown()
                }

            })

        }catch (e:KotlinNullPointerException){
            Log.e("catch distance error ",e.message.toString())
            cardViewDetailInfoPassengerRoute!!.visibility = View.GONE
        }



    }



    /**
     * onInfoWindowClose es una funcion sobrescrita de la interfas GoogleMap.OnInfoWindowCloseListener
     * que se ejecuta en el momento que la InfoWindow del marker se cierra
     *
     * @param marker marca del punto
     */
    override fun onInfoWindowClose(marker: Marker?) {

        // desaparece la tarjeta que contiene la informacion de distancia y tiempo entre el conductor y el marker
        cardViewDetailInfoPassengerRoute!!.visibility = View.GONE
        // cambia de posision InfoWindow
        marker!!.setInfoWindowAnchor(0.50f,0.0f)

    }





    /**
     * La tarea del metodo bitmapDescriptorFromVector es tranformar una imagen vectorial en un
     * BitmapDescriptor
     *
     * @param context comienza resibiendo el contexto de la FragmentActivity
     * @param vectorResId identificado de la imagen vectorial
     * @return BitmapDescriptor
     */
    private fun  bitmapDescriptorFromVector(context: FragmentActivity?, vectorResId:Int):BitmapDescriptor{

        val vectorDrawable = ContextCompat.getDrawable(context!!.application.applicationContext, vectorResId)
        vectorDrawable!!.setBounds(0, 0, vectorDrawable.intrinsicWidth, vectorDrawable.intrinsicHeight)
        val bitmap = Bitmap.createBitmap(vectorDrawable.intrinsicWidth, vectorDrawable.intrinsicHeight, Bitmap.Config.ARGB_8888)
        val canvas = Canvas(bitmap)
        vectorDrawable.draw(canvas)
        return BitmapDescriptorFactory.fromBitmap(bitmap)

    }






    override fun onMapReady(googleMap: GoogleMap?) {
        mMap = googleMap
        mMap!!.setOnInfoWindowClickListener(this)
        mMap!!.setOnInfoWindowCloseListener(this)
        mMap!!.uiSettings.isMyLocationButtonEnabled = false



        var style =  styleMap()

        mMap!!.setMapStyle(style)
        if (stateService.equals(Constants.INTRANSIT)){


            if(routeCache == null){

                trackDownDriver()
            }

        }

        Log.e("mmap","----- "+mMap.toString())
        val hcmus = LatLng(10.762963, 106.682394)
//        mMap?.moveCamera(CameraUpdateFactory.newLatLngZoom(hcmus, 50f))
        if(routeCache != null){

            mMap!!.moveCamera(CameraUpdateFactory.newLatLngZoom(routeCache!!.startLocation, 11.40f))
        }
        markers.add(mMap!!.addMarker(MarkerOptions()
                .title("")
                .position(hcmus)))

        if (ActivityCompat.checkSelfPermission(context!!, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context!!, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return
        }
        mMap?.setMyLocationEnabled(true)

    }







    private fun trackDownDriver() {

        Toast.makeText(context,"validar posisicion del vehiculo ",Toast.LENGTH_LONG).show()
        val firebase = Firebase.getInstance()


        firebase.getSelectAlterna("Servicio/$idService/conductorxy").addValueEventListener(object :ValueEventListener{
            override fun onDataChange(dataSnapshop: DataSnapshot?) {

                Log.e("ubicacion vehiculo",dataSnapshop!!.value.toString())
                if(dataSnapshop.value != null){








                     vehiclePosition = dataSnapshop!!.value.toString().split(",")

                   var  positionsVehicle:ArrayList<String> = validatePositionVehicle(dataSnapshop!!.value.toString())

                    var latitudeVehicle = vehiclePosition!!.get(0).toDouble()
                    var longitudeVehicle = vehiclePosition!!.get(1).toDouble()






                    if (markers != null) {
                        for (marker in markers) {

                            Log.e("marcas"+marker.id,marker.toString())

                        }
                    }

                    Log.e("total ////////////////","/"+ markers.size)

                    if(markers.size-1 > 0 ){

                        val markerCar = markers.get(0)

                        markerCar.position = LatLng(latitudeVehicle, longitudeVehicle)



                        markers.set(0,markerCar)

                        if (positionsVehicle.size > 0){

                            animationAuto(positionsVehicle,latitudeVehicle,longitudeVehicle,markerCar)
                        }



                    }
                    else{

                        val icon = bitmapDescriptorFromVector(activity, R.drawable.ic_car_top)
                        markers.add(0,
                                mMap!!.addMarker(
                                        MarkerOptions()
                                                .icon(icon)
                                                .title(vehiclePosition.toString())
                                                .position(LatLng(latitudeVehicle, longitudeVehicle))))
                    }

                }else{

                    Toast.makeText(context,"servicio $idService no encontrado",Toast.LENGTH_LONG).show()

                }

            }

            override fun onCancelled(dataBaseError: DatabaseError?) {
                Log.e("ubicacion failed ",dataBaseError!!.message)

            }
        })
    }

    var pointOne:String = "4.675942,-74.056026"
    var pointTwo:String = "4.693267,-74.052552"

    private fun validatePositionVehicle(point: String): ArrayList<String> {


        var arrayPointsVehicle:ArrayList<String> = arrayListOf()

        if (pointOne.isNotEmpty()){

            if (pointTwo.isNotEmpty()){

                pointOne = pointTwo
                pointTwo = point

                arrayPointsVehicle!!.add(0,pointOne)
                arrayPointsVehicle!!.add(1,pointTwo)

            }else{
                pointTwo = point
                arrayPointsVehicle!!.add(0,pointOne)
                arrayPointsVehicle!!.add(1,pointTwo)
            }
        }else{
           pointOne = point
        }

        return arrayPointsVehicle

    }




    internal lateinit var valueAnimator: ValueAnimator
    private var speed = 10000

    private var polylineOptions: PolylineOptions? = null
    private var blackPolylineOptions:PolylineOptions? = null
    private var blackPolyline: Polyline? = null
    private var greyPolyLine:Polyline? = null
    private var counterPoliLine: Int = 0

    private fun animationAuto(positionsVehicle: ArrayList<String>, latitudeVehicle: Double, longitudeVehicle: Double, markerCar: Marker) {


         val TAG = MapsActivity::class.java.simpleName
         var polyLineList: ArrayList<LatLng>? = null
         var v: Float = 0.toFloat()
         var lat: Double = 0.toDouble()
         var lng:Double = 0.toDouble()
         var handler: Handler? = null
         var startPosition: LatLng? = null
         var endPosition:LatLng? = null
         var index: Int = 0
         var next:Int = 0
         var sydney: LatLng? = null


        counterPoliLine = 0
        mMap!!.setMapType(GoogleMap.MAP_TYPE_NORMAL)
        mMap!!.isTrafficEnabled = false
        mMap!!.setIndoorEnabled(false)
        mMap!!.setBuildingsEnabled(false)
        mMap!!.getUiSettings().isZoomControlsEnabled = true
        // Add a marker in Home and move the camera
        sydney = LatLng(latitudeVehicle,longitudeVehicle)

        var requestUrl: String? = null

        Log.e("puntos iniciales",positionsVehicle.toString())

        val latitudeOriginVeicle:Double = positionsVehicle[0].split(",")[0].toDouble()
        val longitudeOriginVeicle:Double = positionsVehicle[0].split(",")[1].toDouble()

        val latitudeDestinationVeicle:Double = positionsVehicle[1].split(",")[0].toDouble()
        val longitudeDestinationVeicle:Double = positionsVehicle[1].split(",")[1].toDouble()


        val apiRequest:DirectionsApiRequest = DirectionsApi.newRequest(geoApiContext)
        apiRequest.origin(com.google.maps.model.LatLng(latitudeOriginVeicle, longitudeOriginVeicle))
        apiRequest.destination(com.google.maps.model.LatLng(latitudeDestinationVeicle, longitudeDestinationVeicle))
        apiRequest.mode(TravelMode.DRIVING)
        handler = Handler()

        apiRequest.setCallback(object: PendingResult.Callback<DirectionsResult>{

            override fun onResult(result: DirectionsResult?) {

                Log.e(TAG, result!!.routes.toString() + "")
                try {
                    val jsonArray = result.routes
                    for (i in 0 until jsonArray.size) {
                        val route = jsonArray.get(i)
                        val poly = route.overviewPolyline
                        val polyline = poly.decodePath()

                        polyLineList = ArrayList()
                        for (latLng in polyline.toList()){
                           var p = LatLng(latLng.lat,latLng.lng)
                            polyLineList!!.add(p)

                        }
                        Log.e(TAG, polyLineList.toString())
                    }
                    //Adjusting bounds
                    val builder = LatLngBounds.Builder()
                    for (latLng:LatLng in polyLineList!!.toList()) {
                        builder.include(latLng)
                    }



//


                    handler!!.postDelayed(object : Runnable {
                                @RequiresApi(api = Build.VERSION_CODES.KITKAT)


                                override fun run() {

                                    polylineOptions = PolylineOptions()
                                    polylineOptions!!.color(Color.GRAY)
                                    polylineOptions!!.width(5f)
                                    polylineOptions!!.startCap(SquareCap())
                                    polylineOptions!!.endCap(SquareCap())
                                    polylineOptions!!.jointType(ROUND)
                                    polylineOptions!!.addAll(polyLineList)
                                    greyPolyLine = mMap!!.addPolyline(polylineOptions)

                                    blackPolylineOptions = PolylineOptions()
                                    blackPolylineOptions!!.width(5f)
                                    blackPolylineOptions!!.color(Color.BLACK)
                                    blackPolylineOptions!!.startCap(SquareCap())
                                    blackPolylineOptions!!.endCap(SquareCap())
                                    blackPolylineOptions!!.jointType(ROUND)
                                    blackPolyline = mMap!!.addPolyline(blackPolylineOptions)

                                    mMap!!.addMarker(MarkerOptions()
                                            .position(polyLineList!!.get(polyLineList!!.size - 1)))

                                    if (index < polyLineList!!.size - 1) {
                                        index++
                                        next = index + 1
                                    }
                                    if (index < polyLineList!!.size - 1) {
                                        startPosition = polyLineList!!.get(index)
                                        endPosition = polyLineList!!.get(next)
                                    }
                                    counterPoliLine++
                                    Log.e("largo",polyLineList!!.size.toString()+" counter "+counterPoliLine)
                                    speed = 50000 / polyLineList!!.size
                                    // segunda animacion
                                    valueAnimator = ValueAnimator.ofFloat(0f, 1f)
                                    valueAnimator.setDuration(speed.toLong())
                                    valueAnimator.setInterpolator(LinearInterpolator())
                                    valueAnimator.addUpdateListener(ValueAnimator.AnimatorUpdateListener { valueAnimator ->
                                        v = valueAnimator.animatedFraction
                                        lng = v * endPosition!!.longitude + (1 - v) * startPosition!!.longitude
                                        lat = v * endPosition!!.latitude + (1 - v) * startPosition!!.latitude
                                        var newPos = LatLng(lat, lng)
                                        markerCar.position = newPos
                                        markerCar.setAnchor(0.5f, 0.5f)
                                        markerCar.rotation = getBearing(startPosition!!, newPos!!)


                                        if (activateInRuteMode!!){
                                            mMap!!.moveCamera(CameraUpdateFactory
                                                    .newCameraPosition(CameraPosition.Builder()
                                                            .target(newPos)
                                                            .zoom(18.5f)
                                                            .build()))
                                        }


                                    })

                                    if (counterPoliLine < polyLineList!!.size){

                                        valueAnimator.start()
                                        handler!!.postDelayed(this, speed.toLong())

                                     }else{
                                        valueAnimator.cancel()
                                        counterPoliLine = 0


                                    }


                                }
                            }, 1)






                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onFailure(e: Throwable?) {
               Log.e("animationAuto failure",e!!.message)
            }

        })


    }



//    private class LongOperation(var polyLineList: ArrayList<LatLng>?, var index: Int, var next: Int,  var startPosition: LatLng?, var endPosition: LatLng?, var counterPoliLine: Int, var markerCar: Marker, var speed: Int, var lat: Double, var lng: Double, var v: Float,var activity: FragmentActivity?) : AsyncTask<String, Void, String>() {
//
//
//        override fun doInBackground(vararg params: String?): String {
//            if (index < polyLineList!!.size - 1) {
//                index++
//                next = index + 1
//            }
//            if (index < polyLineList!!.size - 1) {
//                startPosition = polyLineList!!.get(index)
//                endPosition = polyLineList!!.get(next)
//            }
//            counterPoliLine++
//            Log.e("largo",polyLineList!!.size.toString()+" counter "+counterPoliLine)
//            speed = 50000 / polyLineList!!.size
//            // segunda animacion
//            var valueAnimator = ValueAnimator.ofFloat(0f, 1f)
//            valueAnimator.setDuration(speed.toLong())
//            valueAnimator.setInterpolator(LinearInterpolator())
//            valueAnimator.addUpdateListener(ValueAnimator.AnimatorUpdateListener { valueAnimator ->
//                v = valueAnimator.animatedFraction
//                lng = v * endPosition!!.longitude + (1 - v) * startPosition!!.longitude
//                lat = v * endPosition!!.latitude + (1 - v) * startPosition!!.latitude
//                val newPos = LatLng(lat, lng)
//                markerCar.position = newPos
//                markerCar.setAnchor(0.5f, 0.5f)
//                markerCar.rotation = getBearing(startPosition!!, newPos)
////                                        mMap!!.moveCamera(CameraUpdateFactory
////                                                .newCameraPosition(CameraPosition.Builder()
////                                                        .target(newPos)
////                                                        .zoom(18.5f)
////                                                        .build()))
//            })
//
//            if (counterPoliLine < polyLineList!!.size){
//
//                valueAnimator.start()
////                handler!!.postDelayed(this, speed.toLong())
//
//            }else{
//                valueAnimator.cancel()
//                counterPoliLine = 0
//
//            }
//            return "Executed"
//        }
//
//        private fun getBearing(begin: LatLng, end: LatLng): Float {
//            val lat = Math.abs(begin.latitude - end.latitude)
//            val lng = Math.abs(begin.longitude - end.longitude)
//
//            if (begin.latitude < end.latitude && begin.longitude < end.longitude)
//                return Math.toDegrees(Math.atan(lng / lat)).toFloat()
//            else if (begin.latitude >= end.latitude && begin.longitude < end.longitude)
//                return (90 - Math.toDegrees(Math.atan(lng / lat)) + 90).toFloat()
//            else if (begin.latitude >= end.latitude && begin.longitude >= end.longitude)
//                return (Math.toDegrees(Math.atan(lng / lat)) + 180).toFloat()
//            else if (begin.latitude < end.latitude && begin.longitude >= end.longitude)
//                return (90 - Math.toDegrees(Math.atan(lng / lat)) + 270).toFloat()
//            return -1f
//        }
//
//
//
//    }


    private fun decodePoly(encoded: String): List<LatLng> {
        val poly = ArrayList<LatLng>()
        var index = 0
        val len = encoded.length
        var lat = 0
        var lng = 0

        while (index < len) {
            var b: Int
            var shift = 0
            var result = 0
            do {
                b = encoded[index++].toInt() - 63
                result = result or (b and 0x1f shl shift)
                shift += 5
            } while (b >= 0x20)
            val dlat = if (result and 1 != 0) (result shr 1).inv() else result shr 1
            lat += dlat

            shift = 0
            result = 0
            do {
                b = encoded[index++].toInt() - 63
                result = result or (b and 0x1f shl shift)
                shift += 5
            } while (b >= 0x20)
            val dlng = if (result and 1 != 0) (result shr 1).inv() else result shr 1
            lng += dlng

            val p = LatLng(lat.toDouble() / 1E5,
                    lng.toDouble() / 1E5)
            poly.add(p)
        }

        return poly
    }

    private fun getBearing(begin: LatLng, end: LatLng): Float {
        val lat = Math.abs(begin.latitude - end.latitude)
        val lng = Math.abs(begin.longitude - end.longitude)

        if (begin.latitude < end.latitude && begin.longitude < end.longitude)
            return Math.toDegrees(Math.atan(lng / lat)).toFloat()
        else if (begin.latitude >= end.latitude && begin.longitude < end.longitude)
            return (90 - Math.toDegrees(Math.atan(lng / lat)) + 90).toFloat()
        else if (begin.latitude >= end.latitude && begin.longitude >= end.longitude)
            return (Math.toDegrees(Math.atan(lng / lat)) + 180).toFloat()
        else if (begin.latitude < end.latitude && begin.longitude >= end.longitude)
            return (90 - Math.toDegrees(Math.atan(lng / lat)) + 270).toFloat()
        return -1f
    }


    /**
     * la tarea del metodo styleMap es consultar en las preferencias de la aplicacion el estilo visual del mapa
     * luego lo filtra en una lista de stilos precargados en el proyecto y retorna el stilo seleccionado
     *
     * @return MapStyleOptions
     */
    private fun styleMap(): MapStyleOptions {

        // obtiene las preferencias del sistema
        val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
        // obtiene el indice del stilo
        val styleMapPreference = sharedPreferences.getString("style_map_list", "0")
        // selecciona el estilo segun el indice y lo retorna
        when(styleMapPreference) {
            "0"-> return MapStyleOptions.loadRawResourceStyle(context, R.raw.mapstyle_grayscale)
            "1"-> return MapStyleOptions.loadRawResourceStyle(context, R.raw.mapstyle_night)
            "2"-> return MapStyleOptions.loadRawResourceStyle(context, R.raw.mapstyle_retro)
            else -> return MapStyleOptions.loadRawResourceStyle(context, R.raw.mapstyle_retro)
        }
    }






}
