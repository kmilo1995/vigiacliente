package com.destinoseguro.vigiaespecialcliente.travels.objects

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.TextView
import android.widget.Toast
import com.destinoseguro.vigiaespecialcliente.R
import com.destinoseguro.vigiaespecialcliente.R.id.cardPassengerName
import com.destinoseguro.vigiaespecialcliente.requests.model.Passenger
import com.destinoseguro.vigiaespecialcliente.requests.objects.passenger
import kotlinx.android.synthetic.main.card_view_passenger_row_travel.view.*

/**
 * Created by root on 1/12/17.
 */
class listPassengerAdapter(private val mContext: Context, private val passengerArrayList: List<Passenger>?) : RecyclerView.Adapter<listPassengerAdapter.CustomViewHolder>() {

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): CustomViewHolder {
        val view = LayoutInflater.from(viewGroup.context).inflate(R.layout.card_view_passenger_row_travel, null)
        return CustomViewHolder(view)
    }

    override fun onBindViewHolder(customViewHolder: CustomViewHolder, i: Int) {
        val passengerItem = passengerArrayList!![i]

        //Setting text view title
        customViewHolder.name!!.text = passengerItem.name+" "+passengerItem.lastName
        customViewHolder.document!!.text = passengerItem.document
        customViewHolder.phone!!.text = passengerItem.phoneNumber
        customViewHolder.origin!!.text = passengerItem.origin
        customViewHolder.destination!!.text = passengerItem.destination
//        customViewHolder.buttonDelete.setOnClickListener(View.OnClickListener {
//            Toast.makeText(mContext,""+passengerItem.name, Toast.LENGTH_LONG).show()
//
//            this.notifyItemRemoved(i)
//            this.passengerArrayList.remove(passengerItem)
//
//        })
    }

    override fun getItemCount(): Int {
        return passengerArrayList?.size ?: 0
    }

    inner class CustomViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        var name: TextView? = view.cardPassengerName
        var document: TextView? = view.person_document
        var phone: TextView? = view.person_phone
        var origin: TextView? = view.person_origin
        var destination: TextView? = view.person_destination
//        var buttonDelete: ImageButton



    }
}