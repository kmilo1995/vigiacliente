package com.destinoseguro.vigiaespecialcliente.travels.activitys

import android.support.design.widget.TabLayout
import android.support.v7.app.AppCompatActivity

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem

import com.destinoseguro.vigiaespecialcliente.R
import com.destinoseguro.vigiaespecialcliente.travels.model.Travel
import com.destinoseguro.vigiaespecialcliente.travels.fragments.fragment_travel_asignment
import com.destinoseguro.vigiaespecialcliente.travels.fragments.FragmentTravelMap
import com.destinoseguro.vigiaespecialcliente.travels.fragments.fragment_travel_passengers
import com.destinoseguro.vigiaespecialcliente.travels.fragments.fragment_travel_service
import com.destinoseguro.vigiaespecialcliente.travels.objects.Constants
import com.destinoseguro.vigiaespecialcliente.travels.objects.detailService
import kotlinx.android.synthetic.main.activity_travel.*

class travel : AppCompatActivity() {

    /**
     * The [android.support.v4.view.PagerAdapter] that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * [android.support.v4.app.FragmentStatePagerAdapter].
     */
    private var mSectionsPagerAdapter: SectionsPagerAdapter? = null

    private var state:String? = null

    private var totalViewPager:Int = 4

    var travel: Travel? = null
    var idTravel: String? = null
    var IMEI: String? = null



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)




//        se obtine el objeto travel
        val bundle = intent.extras

        travel = bundle!!.getParcelable<Travel>("travel")



        idTravel = bundle.getString("idTravel")
        IMEI = bundle.getString("IMEI")

        Log.e("travel antes",""+idTravel)


        setContentView(R.layout.activity_travel)
        toolbar.title = "VSE-"+travel!!.code
        setSupportActionBar(toolbar)


//

        state = travel!!.state


        if(state.equals(Constants!!.REQUESTED)){

            tabs.removeTabAt(1)
            tabs.removeTabAt(0)
            totalViewPager = 2
        }

        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = SectionsPagerAdapter(supportFragmentManager)

        // Set up the ViewPager with the sections adapter.
        travelViewPagerContainer.adapter = mSectionsPagerAdapter

        travelViewPagerContainer.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(tabs))


        tabs.addOnTabSelectedListener(TabLayout.ViewPagerOnTabSelectedListener(travelViewPagerContainer))

//        fab.setOnClickListener { view ->
//
//                Toast.makeText(this,"travel numero "+idTravel, Toast.LENGTH_LONG).show()
//
//        }

    }


    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        //menuInflater.inflate(R.menu.menu_travel, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        val id = item.itemId

        if (id == R.id.action_settings) {
            return true
        }

        return super.onOptionsItemSelected(item)
    }


    /**
     * A [FragmentPagerAdapter] that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    inner class SectionsPagerAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {

        override fun getItem(position: Int): Fragment? {


            if (state.equals(Constants.REQUESTED)){

                when(position){
                    0-> {

                        val args = Bundle()
                        var service = detailService(
                                travel!!.origin,
                                travel!!.destination,
                                travel!!.startTime,
                                travel!!.typeServices,
                                travel!!.typeVehicle,
                                travel!!.distance
                        )
                        args.putParcelable("service",service)
                        args.putString("img", travel!!.rute)
                        var fragment_services:fragment_travel_service? = fragment_travel_service()
                        fragment_services!!.arguments = args
                        return fragment_services
                    }
                    1-> {
                        val args = Bundle()
                        Log.e("lista antes",""+idTravel)


//                        args.putParcelableArrayList("passengers", arrayListPassenger)
                        args.putString("code_services", idTravel!!)
                        var fragment_passengers:fragment_travel_passengers? = fragment_travel_passengers()
                        fragment_passengers!!.arguments = args
                        return fragment_passengers
                    }
                    else-> return null

                }
            }else{

                when(position){
                    0-> {

                        val args = Bundle()
                        Log.e("lista antes",""+idTravel)


//                        args.putParcelableArrayList("passengers", arrayListPassenger)
                        args.putString("STATE_SERVICE", travel!!.state)
                        args.putString("IMEI", IMEI)
                        args.putString("ID_SERVICE", idTravel)
                        var fragment_travel_map: FragmentTravelMap? = FragmentTravelMap()
                        fragment_travel_map!!.arguments = args
                        return fragment_travel_map
//                        return FragmentTravelMap()
                    }
                    1-> {
                        val args = Bundle()
                        args.putString("code_services",idTravel)
                        var fragment_assignment:fragment_travel_asignment? = fragment_travel_asignment()
                        fragment_assignment!!.arguments = args
                        return fragment_assignment
                    }
                    2-> {
                        val args = Bundle()
                        var service = detailService(
                                travel!!.origin,
                                travel!!.destination,
                                travel!!.startTime,
                                travel!!.typeServices,
                                travel!!.typeVehicle,
                                travel!!.distance

                        )
                        args.putParcelable("service",service)
                        var fragment_services:fragment_travel_service? = fragment_travel_service()
                        fragment_services!!.arguments = args
                        return fragment_services
                    }
                    3-> {
                        val args = Bundle()
//                        Log.e("lista antes",""+travel!!.listPassengers!!.size)
//                        args.putParcelableArrayList("passengers", arrayListPassenger)
                        args.putString("code_services", idTravel)
                        var fragment_passengers:fragment_travel_passengers? = fragment_travel_passengers()
                        fragment_passengers!!.arguments = args
                        return fragment_passengers
                    }
                    else-> return null

                }

            }



        }

        override fun getCount(): Int {
            // Show 3 total pages.
            return totalViewPager
        }
    }


}
