package com.destinoseguro.vigiaespecialcliente.travels.objects

import android.annotation.SuppressLint
import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

/**
 * Created by root on 1/12/17.
 */
@SuppressLint("ParcelCreator")
@Parcelize
class detailService(
         var origin:String?,
         var destination:String?,
         var dataTime:String?,
         var typeServices:String?,
         var typeVehicle:String?,
         var distance:String?

        ):Parcelable {


}