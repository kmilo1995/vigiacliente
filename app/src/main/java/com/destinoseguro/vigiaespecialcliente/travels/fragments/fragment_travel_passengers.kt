package com.destinoseguro.vigiaespecialcliente.travels.fragments



import android.support.v4.app.Fragment
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.destinoseguro.vigiaespecialcliente.R
import com.destinoseguro.vigiaespecialcliente.RoomAppDatabase.AppDatabase
import com.destinoseguro.vigiaespecialcliente.requests.model.Passenger
import com.destinoseguro.vigiaespecialcliente.travels.objects.listPassengerAdapter
import com.destinoseguro.vigiaespecialcliente.requests.objects.passenger
import com.destinoseguro.vigiaespecialcliente.restApi.RestApiAdapter
import com.destinoseguro.vigiaespecialcliente.travels.model.EndPointsTravel
import com.destinoseguro.vigiaespecialcliente.travels.model.PassengersServer
import com.destinoseguro.vigiaespecialcliente.travels.model.ResponsePassengers
import kotlinx.android.synthetic.main.fragment_travel_passenger.*
import kotlinx.android.synthetic.main.fragment_travel_passenger.view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Created by root on 30/11/17.
 */
class fragment_travel_passengers : Fragment() {

//    llamado a la base de datos
   var  db:AppDatabase = AppDatabase.getAppDatabase(activity)

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val view:View = inflater.inflate(R.layout.fragment_travel_passenger,container,false)

        var bundle = arguments
//vse

        var idTravel = bundle!!.getString("code_services")

        loadAssignedPassengers(idTravel, view)

//        var passengerList = db.PassengerDao().loadAllByIds(idTravel)
//        val layoutManager:RecyclerView.LayoutManager? = LinearLayoutManager(activity) as RecyclerView.LayoutManager
//
//        view.travelShowListPassengerRecycleView.layoutManager = layoutManager
//        view.travelShowListPassengerRecycleView.setHasFixedSize(true)
//
//        val adapterPassenger = listPassengerAdapter(view.context,passengerList)
//
//        view.travelShowListPassengerRecycleView.adapter = adapterPassenger
//

        return view
    }


    fun loadAssignedPassengers(idTravel: String?, view: View){


        var resApiAdapter = RestApiAdapter()
        var retrofit = resApiAdapter.conectionApiRest()

        var response: Call<ResponsePassengers> = retrofit.create(EndPointsTravel::class.java).getPassengers( idTravel )

        response.enqueue(object : Callback<ResponsePassengers> {

            override fun onResponse(call: Call<ResponsePassengers>?, response: Response<ResponsePassengers>?) {


                 var listPassengers = ArrayList<Passenger>()

                Log.e("problema", "////"+response.toString() )

                if(response!!.code() == 500){
                    Toast.makeText(activity,response.toString(),Toast.LENGTH_LONG).show()
                    return
                }

                if (response != null) {

                    for (passenger: PassengersServer in response.body()!!.data!!){

                        var newPassenger = Passenger(
                                passenger.pasajero,
                                "",
                                passenger.documento,
                                passenger.celular,
                                passenger.direccionorigen,
                                passenger.direcciondestino,
                                "falta observacion",
                                "observacion",
                                idTravel!!.toInt()
                        )

                        listPassengers.add(newPassenger)

                    }


                    val layoutManager:RecyclerView.LayoutManager? = LinearLayoutManager(activity) as RecyclerView.LayoutManager
//
                    view.travelShowListPassengerRecycleView.layoutManager = layoutManager
                    view.travelShowListPassengerRecycleView.setHasFixedSize(true)

                    val adapterPassenger = listPassengerAdapter(view.context,listPassengers)

                    view.travelShowListPassengerRecycleView.adapter = adapterPassenger


                    Log.e("pasajeros" , response.body()!!.data!![0].toString())
                }
            }

            override fun onFailure(call: Call<ResponsePassengers>?, t: Throwable?) {
                Log.e("fragmenttravelpasseng","loadAssignedPassengers onFailure "+t!!.message)
            }

        })

    }
}