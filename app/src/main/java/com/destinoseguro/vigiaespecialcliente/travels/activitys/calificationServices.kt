package com.destinoseguro.vigiaespecialcliente.travels.activitys

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.RatingBar
import android.widget.Toast
import com.destinoseguro.vigiaespecialcliente.R
import com.destinoseguro.vigiaespecialcliente.historical.model.EndPointHistoricals
import com.destinoseguro.vigiaespecialcliente.register.model.ResponseRegister
import com.destinoseguro.vigiaespecialcliente.restApi.RestApiAdapter
import com.destinoseguro.vigiaespecialcliente.travels.model.Travel
import kotlinx.android.synthetic.main.activity_calification_services.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class calificationServices : AppCompatActivity() {


    var travel: Travel? = null
    var calification: String? = ""
    var observation: String? = ""

    private val restApiAdapter = RestApiAdapter()
    private val retrofit = restApiAdapter.conectionApiRest()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_calification_services)



        val bundle = intent.extras

        travel = bundle!!.getParcelable<Travel>("travel")
        var IMEI = bundle!!.getString("IMEI")

        var id_service = travel?.code.toString()





        calificationRatingBar.onRatingBarChangeListener = RatingBar.OnRatingBarChangeListener { ratingBar, rating, fromUser ->

            calification = rating.toString()
//            Toast.makeText(this,""+rating,Toast.LENGTH_LONG).show()
        }

        calificationBtn.setOnClickListener(View.OnClickListener {

            observation = calificationObservation.text.toString()

            if (calification.equals("") || calification == null){

                Toast.makeText(this,"Para enviar la solicitud deve calificar el servicio"+IMEI,Toast.LENGTH_LONG).show()
            }else{


                sendCalification(observation, calification!!,id_service,IMEI)

            }

        })
    }

    private fun sendCalification(observation: String?, calification: String,id_service:String,IMEI:String) {

        var response: Call<ResponseRegister> = retrofit.create(EndPointHistoricals::class.java).setCalificationService(
                IMEI,
                calification,
                observation,
                id_service
        )

        Log.e("data calification ", IMEI+" "+calification+" "+observation+" "+id_service)


        response.enqueue(object :Callback<ResponseRegister>{

            override fun onResponse(call: Call<ResponseRegister>?, response: Response<ResponseRegister>?) {

                if (response != null) {
                    Toast.makeText(this@calificationServices, response.body()!!.data.toString() , Toast.LENGTH_LONG).show()
                    Log.e("sendCalification ","onResponse "+response.toString())
                    finish()
                }
            }

            override fun onFailure(call: Call<ResponseRegister>?, t: Throwable?) {
                Log.e("sendCalification ","onFailure "+t!!.message.toString())
            }
        })
    }





}
