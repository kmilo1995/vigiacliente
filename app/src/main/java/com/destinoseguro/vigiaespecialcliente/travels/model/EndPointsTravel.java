package com.destinoseguro.vigiaespecialcliente.travels.model;

import com.destinoseguro.vigiaespecialcliente.restApi.ConstantsRestApi;
import com.destinoseguro.vigiaespecialcliente.travels.model.ResponseTravel;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by root on 18/12/17.
 */

public interface EndPointsTravel {


    @POST(ConstantsRestApi.GET_TRAVELS)
    Call<ResponseTravel>getTravel();


    @FormUrlEncoded
    @POST(ConstantsRestApi.GET_ASSIGNMENT)
    Call<ResponseAssignment> getAsignment(@Field("idservicio") String idservicio);

    @FormUrlEncoded
    @POST(ConstantsRestApi.GET_PASSENGERS)
    Call<ResponsePassengers> getPassengers(@Field("idservicio") String idservicio);


    @FormUrlEncoded
    @POST(ConstantsRestApi.GET_RUTE)
    Call<ResponseRute> getRoute(@Field("idservicio") String idservicio);



}
