package com.destinoseguro.vigiaespecialcliente.travels.objects

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.res.Resources
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Build
import android.os.Bundle
import android.support.annotation.RequiresApi
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.util.Base64
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.RotateAnimation
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.destinoseguro.vigiaespecialcliente.R
import com.destinoseguro.vigiaespecialcliente.RoomAppDatabase.AppDatabase
import com.destinoseguro.vigiaespecialcliente.firebase.Firebase
import com.destinoseguro.vigiaespecialcliente.travels.activitys.travel
import com.destinoseguro.vigiaespecialcliente.travels.model.Travel
import com.github.clans.fab.FloatingActionButton
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import kotlinx.android.synthetic.main.card_view_travel_row.view.*
import java.util.*
import kotlin.collections.ArrayList

/**
 * Created by root on 29/11/17.
 */
class listTravelsAdapter(private val context: Context, private val listTavelList: List<Travel>,IMEI:String):RecyclerView.Adapter<listTravelsAdapter.CustomerViewHolder>(){

    private val DURATION = 250
    var c:Context? = null
    var IMEi =IMEI
    private val firebase = Firebase.getInstance()
    private var listTavelArrayList = ArrayList<Travel>(listTavelList)

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): CustomerViewHolder {
        if (parent != null) {
            c = parent.context
        }
        val view:View = LayoutInflater.from(parent!!.context).inflate(R.layout.card_view_travel_row,null)
        return CustomerViewHolder(view)
    }


    @RequiresApi(Build.VERSION_CODES.M)
    override fun onBindViewHolder(holder: CustomerViewHolder?, position: Int) {

        val itemCard = listTavelArrayList!![position]

        if (holder != null ) {

            holder.textCode!!.text = "VSE-"+itemCard.code

            var ruteImageBitmap = convertStringToBitmap(itemCard.rute.toString())
            if (ruteImageBitmap != null){

                holder.imgRute!!.setImageBitmap(ruteImageBitmap)
            }else{

                holder.imgRute!!.setImageResource(R.drawable.map)
            }



            firebase.getSelectAlterna("Servicio/${itemCard.code}/estadoviaje").addValueEventListener(object:ValueEventListener{

                override fun onDataChange(dataSnapshotServices : DataSnapshot?) {

                    itemCard.state = nameStateTravel(dataSnapshotServices!!.value.toString())
                    if (dataSnapshotServices!!.value.toString().equals("null")){
                        itemCard.state = "Asignado"
                    }


                    if(itemCard.state.equals("Finalizado")){

                        listTavelArrayList.removeAt(position)
                        notifyItemRemoved(position);
                        notifyItemRangeChanged(position,listTavelArrayList.size)
                        var db: AppDatabase = AppDatabase.getAppDatabase(context)
                        db.TravelDao().delete(itemCard)

                    }

                    holder.travelLabelState!!.text = itemCard.state
                    if(Build.VERSION.SDK_INT == Build.VERSION_CODES.LOLLIPOP_MR1 || Build.VERSION.SDK_INT == Build.VERSION_CODES.LOLLIPOP){
                        holder.travelLabelState!!.setBackgroundColor(ContextCompat.getColor(context,validateState(itemCard.state)))
                    }
                    else{
                        holder.travelLabelState!!.setBackgroundColor(context.getColor(validateState(itemCard.state)))
                    }

                    holder.showDetailFabButton!!.setOnClickListener(View.OnClickListener {





                        var intent = Intent(context, travel::class.java)

                        val bundle = Bundle()

                        Log.e("item card pasajeros",""+itemCard.toString())

                        bundle.putParcelable("travel",itemCard)
                        bundle.putString("idTravel", itemCard.code!!)
                        bundle.putString("IMEI", IMEi)
                        intent.putExtras(bundle)

                        context.startActivity(intent)


//

                    })
                    Log.e("listTravelAdapter","services state onDataChange"+dataSnapshotServices.toString()+" "+itemCard.code)

                }

                private fun nameStateTravel(state: String): String? {
                    when (state){

                        "8"-> return "Solicitado"
                        "14"-> return "Asignado"
                        "15"-> return "Finalizado"
                        "16"-> return "En Transito"
                        else->return "Solicitado"

                    }

                }

                override fun onCancelled(e: DatabaseError?) {

                    Log.e("listTravelAdapter","services state onCancelled"+e!!.message)
                }
            })


//            holder.textOrigenDestination!!.text = itemCard.origin+"\n"+itemCard.destination
            holder.textOrigen!!.text = itemCard.origin
            holder.textDestination!!.text = itemCard.destination
            holder.textApplicant!!.text = itemCard.applicantName
            holder.textCompany!!.text = itemCard.companyName
            holder.travelLabelState!!.text = itemCard.state



            holder.showDetail!!.setOnClickListener(View.OnClickListener {

                toggleDetails(holder)

            })



        }

    }

    private fun validateState(state: String?):Int{

        Log.e("Estados",state)
        when (state){

            "Solicitado"-> return R.color.color_state_travel_requested
            "Asignado"-> return R.color.color_state_travel_assigned
            "En Transito"-> return R.color.color_state_travel_onroute
            "Finalizado"-> return R.color.color_state_travel_finalized
            else->return R.color.color_state_travel_requested

        }
    }

    override fun getItemCount(): Int {

      return listTavelArrayList?.size ?: 0

    }




   inner class CustomerViewHolder(view:View):RecyclerView.ViewHolder(view) {

       var showDetail:LinearLayout? = view.showDetail
       var linearLayoutDetails:LinearLayout? = view.linearLayoutDetails
       var imageViewExpand: ImageView? = view.imageViewExpand
       var imgRute: ImageView? = view.imgRute
       var textCode:TextView? = view.travelCardViewTextCode
       var textApplicant:TextView? = view.travelCardViewTextApplicant
       var textOrigen:TextView? = view.travelCardViewTextOrigen
       var textDestination:TextView? = view.travelCardViewTextDestination
//       var textOriginDestination:TextView? = view.travelCardViewTextOriginDestination
       var textCompany:TextView? = view.travelCardViewTextCompany
       var travelLabelState:TextView? = view.travelLabelState
       var showDetailFabButton:FloatingActionButton? = view.travelShowDetailFabButton



    }


    fun toggleDetails(holder: CustomerViewHolder?) {
        if (holder != null) {
            if (holder.linearLayoutDetails!!.getVisibility() == View.GONE) {
                ExpandAndCollapseViewUtil.expand(holder.linearLayoutDetails, DURATION)
                holder.imageViewExpand!!.setImageResource(R.drawable.ic_arrow_up)
                rotate(-180.0f,holder)

                var margin = 44
                val lp = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)
                lp.setMargins(margin, margin, margin, 0)
                holder.showDetail!!.layoutParams = lp
            } else {
                ExpandAndCollapseViewUtil.collapse(holder.linearLayoutDetails, DURATION)
                holder.imageViewExpand!!.setImageResource(R.drawable.ic_arrow_down)
                rotate(180.0f,holder)
                var margin = 44
                val lp = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)
                lp.setMargins(margin, margin, margin, margin)
                holder.showDetail!!.layoutParams = lp
//                holder.linearLayoutDetails!!.visibility = View.GONE
            }
        }
    }

    private fun rotate(angle: Float,holder: CustomerViewHolder?) {
        val animation = RotateAnimation(0.0f, angle, Animation.RELATIVE_TO_SELF, 0.5f,
                Animation.RELATIVE_TO_SELF, 0.5f)
        animation.fillAfter = true
        animation.duration = DURATION.toLong()
        if (holder != null) {
            holder.imageViewExpand!!.startAnimation(animation)
        }
    }

    fun convertStringToBitmap(imgStringBase64: String): Bitmap? {

        var decodedImage = BitmapFactory.decodeResource(Resources.getSystem(), R.drawable.perfil)

        try {

            if (!imgStringBase64.isEmpty()){

                val imageBytes = Base64.decode(imgStringBase64, Base64.DEFAULT)
                Log.e("imageBytes",imageBytes.toString())
                decodedImage = BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.size)
            }
        }catch (e:Exception){
          Log.e("convertStringToBitmap","catch"+e.message)
        }



        return decodedImage
    }

}