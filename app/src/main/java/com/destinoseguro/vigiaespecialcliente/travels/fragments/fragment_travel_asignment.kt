package com.destinoseguro.vigiaespecialcliente.travels.fragments



import android.support.v4.app.Fragment
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.destinoseguro.vigiaespecialcliente.R
import com.destinoseguro.vigiaespecialcliente.restApi.RestApiAdapter
import com.destinoseguro.vigiaespecialcliente.travels.model.AssignmentServer
import com.destinoseguro.vigiaespecialcliente.travels.model.EndPointsTravel
import com.destinoseguro.vigiaespecialcliente.travels.model.ResponseAssignment
import com.destinoseguro.vigiaespecialcliente.travels.objects.listAsignedAdapter
import kotlinx.android.synthetic.main.fragment_travel_asignment.view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Created by root on 30/11/17.
 */
class fragment_travel_asignment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {


        var bundle = arguments

        var idTravel = bundle!!.getString("code_services")

        val view:View = inflater.inflate(R.layout.fragment_travel_asignment,container,false)


        loadAssignedDrivers(idTravel,view)


        return view
    }

     fun loadAssignedDrivers(idTravel: String?, view: View){


         var resApiAdapter = RestApiAdapter()
         var retrofit = resApiAdapter.conectionApiRest()

         var response:Call<ResponseAssignment> = retrofit.create(EndPointsTravel::class.java).getAsignment( idTravel )

         Log.e("asignacion respuesta","= "+response.request())

         response.enqueue(object :Callback<ResponseAssignment>{

             override fun onResponse(call: Call<ResponseAssignment>?, response: Response<ResponseAssignment>?) {

                 if (response != null) {
                     Log.e("asignacionesssss" , response.body()!!.data!![0].toString())



                     var listAsignedDriver = response.body()!!.data
                     var listAsignedAdapter = listAsignedAdapter(listAsignedDriver)
                       view.horizontal_cycle.adapter = listAsignedAdapter






                 }
             }

             override fun onFailure(call: Call<ResponseAssignment>?, t: Throwable?) {
                 Log.e("fragmenttravelasignment","loadAssignedDrivers onFailure "+t!!.message)
             }

         })

     }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {


        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }
}