package com.destinoseguro.vigiaespecialcliente.travels.objects

import android.view.View
import com.destinoseguro.vigiaespecialcliente.R
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.Marker
import android.view.LayoutInflater
import com.destinoseguro.vigiaespecialcliente.travels.model.Destino
import com.destinoseguro.vigiaespecialcliente.travels.model.Origen
import kotlinx.android.synthetic.main.info_windows_marker_point.view.*


/**
 * Created by root on 22/01/18.
 */
class customInfoWindowsAdapter:GoogleMap.InfoWindowAdapter  {


    private val TAG = "CustomInfoWindowAdapter"
    private var inflater: LayoutInflater? = null
    private var pointsOrigin:List<Origen>? = null
    private var point:Origen? = null
    private var pointsDestination:List<Destino>? = null


    constructor(inflater: LayoutInflater,pointsOrigin: List<Origen>?){

        this.inflater = inflater
        this.pointsOrigin = pointsOrigin
    }


    constructor(inflater: LayoutInflater,point:Origen?){

        this.inflater = inflater
        this.point = point
    }

    constructor(inflater: LayoutInflater, pointsDestination: Iterator<Destino>){

        this.inflater = inflater
    }

    override fun getInfoContents(p0: Marker?): View {

        var view = inflater!!.inflate(R.layout.info_windows_marker_point, null)




                view.cardPassengerNameRute.text = point?.passenger

//             view.cardPassenger_documentRute.text = pointsOrigin!!.document.toString()
//             view.cardPassenger_phoneRute.text = pointsOrigin!!.cellphone.toString()
//             view.cardPassenger_originRute.text = pointsOrigin!!.geolocate.toString()



        return view

    }

    override fun getInfoWindow(p0: Marker?): View? {

        return null
    }


}