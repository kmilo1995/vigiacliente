package com.destinoseguro.vigiaespecialcliente.travels.model

import android.annotation.SuppressLint
import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import android.os.Parcelable
import com.destinoseguro.vigiaespecialcliente.requests.objects.passenger
import kotlinx.android.parcel.Parcelize

/**
 * Created by root on 30/11/17.
 */
@SuppressLint("ParcelCreator")
@Parcelize
@Entity (tableName = "table_travel")
class Travel(

        @ColumnInfo(name = "company_name")
        var companyName:String?,
        @ColumnInfo(name = "code")
        var code:String?,
        @ColumnInfo(name = "state")
        var state:String?,
        @ColumnInfo(name = "origin")
        var origin:String?,
        @ColumnInfo(name = "destination")
        var destination:String?,
        @ColumnInfo(name = "applicant_name")
        var applicantName:String?,
        @ColumnInfo(name = "start_time")
        var startTime:String?,
        @ColumnInfo(name = "end_time")
        var endTime:String?,
        @ColumnInfo(name = "rute")
        var rute:String?,
        @ColumnInfo(name = "distance")
        var distance:String?,
        @ColumnInfo(name = "type_services")
        var typeServices:String?,
        @ColumnInfo(name = "type_vehicle")
        var typeVehicle:String?,
        @ColumnInfo(name = "type_operation")
        var typeOperation:String?,
        @ColumnInfo(name = "observation")
        var observation:String?,
        @ColumnInfo(name = "number_passengers")
        var numberPassengers:String?,
        @ColumnInfo(name = "go_and_come_back")
        var goAndComeBack:Boolean?,
        @ColumnInfo(name = "synchronization")
        var synchronization:Boolean?





):Parcelable {

    @PrimaryKey(autoGenerate = true)
    var id :Int? = null


    constructor(

    ):this(null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,false,false)

    override fun toString(): String {
        return "Travel(companyName=$companyName, code=$code, state=$state, origin=$origin, destination=$destination, applicantName=$applicantName, startTime=$startTime, endTime=$endTime, rute=$rute, distance=$distance, typeServices=$typeServices, typeVehicle=$typeVehicle, typeOperation=$typeOperation, observation=$observation, numberPassengers=$numberPassengers, goAndComeBack=$goAndComeBack, synchronization=$synchronization, id=$id)"
    }


}







