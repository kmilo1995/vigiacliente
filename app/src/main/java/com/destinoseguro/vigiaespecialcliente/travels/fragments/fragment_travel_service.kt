package com.destinoseguro.vigiaespecialcliente.travels.fragments



import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.support.v4.app.Fragment
import android.os.Bundle
import android.util.Base64
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.destinoseguro.vigiaespecialcliente.R
import com.destinoseguro.vigiaespecialcliente.travels.objects.detailService
import kotlinx.android.synthetic.main.fragment_travel_services.view.*

/**
 * Created by root on 30/11/17.
 */
class fragment_travel_service : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {


        val bundle = arguments
       var  service = bundle!!.getParcelable<detailService>("service")
       var  img = bundle!!.getString("img")

        val view:View = inflater.inflate(R.layout.fragment_travel_services,container,false)



//        view.travelServiceLabelOriginDestination.text = service.origin +" - "+service.destination
        view.travelServiceLabelOrigin.text = service.origin
        view.travelServiceLabelDestination.text = service.destination
        view.travelServiceLabelDatatime.text = service.dataTime
        view.travelServiceLabelTypeService.text = service.typeServices
        view.travelServiceLabelTypeVehicle.text = service.typeVehicle

        var imgRuteBitmap = convertStringToBitmap(img)
        if( imgRuteBitmap != null){

            view.travelServiceImgMapRute.setImageBitmap(imgRuteBitmap)
        }


        if (service.distance != null){
            view.servicesDistance.text = service.distance
        }


        return view
    }

    fun convertStringToBitmap(imgStringBase64: String?): Bitmap? {

        var decodedImage = BitmapFactory.decodeResource(getResources(), R.drawable.mapa);
        if ( !imgStringBase64.equals("null") && !imgStringBase64.equals("") && imgStringBase64 != null){

            var imageBytes = Base64.decode(imgStringBase64, Base64.DEFAULT)
            decodedImage = BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.size)
        }



        return decodedImage
    }
}