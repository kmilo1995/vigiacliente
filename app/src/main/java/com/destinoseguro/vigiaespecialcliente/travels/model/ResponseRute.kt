package com.destinoseguro.vigiaespecialcliente.travels.model

/**
 * Created by root on 3/01/18.
 */


class ResponseRute {

    var isSuccess: Boolean = false
    var data: Data? = null

    /**
     * No args constructor for use in serialization
     *
     */
    constructor() {}

    /**
     *
     * @param data
     * @param success
     */
    constructor(success: Boolean, data: Data) : super() {
        this.isSuccess = success
        this.data = data
    }

}


class Data {

    var origen: List<Origen>? = null
    var destino: List<Destino>? = null

    /**
     * No args constructor for use in serialization
     *
     */
    constructor() {}

    /**
     *
     * @param destino
     * @param origen
     */
    constructor(origen: List<Origen>, destino: List<Destino>) : super() {
        this.origen = origen
        this.destino = destino
    }

}


class Destino {

    var document: String? = null
    var location: String? = null
    var geolocate: String? = null
    var type: String? = null
    var orden: Int = 0

    /**
     * No args constructor for use in serialization
     *
     */
    constructor() {}

    /**
     *
     * @param orden
     * @param geolocate
     * @param document
     * @param location
     * @param type
     */
    constructor(document: String, location: String, geolocate: String, type: String, orden: Int) : super() {
        this.document = document
        this.location = location
        this.geolocate = geolocate
        this.type = type
        this.orden = orden
    }

}

class Origen {

    var document: String? = null
    var location: String? = null
    var geolocate: String? = null
    var passenger: String? = null
    var cellphone: String? = null
    var tpe: String? = null
    var orden: Int = 0
    var type: String? = null

    /**
     * No args constructor for use in serialization
     *
     */
    constructor() {}

    /**
     *
     * @param orden
     * @param tpe
     * @param passenger
     * @param geolocate
     * @param document
     * @param location
     * @param cellphone
     * @param type
     */
    constructor(document: String, location: String, geolocate: String, passenger: String, cellphone: String, tpe: String, orden: Int, type: String) : super() {
        this.document = document
        this.location = location
        this.geolocate = geolocate
        this.passenger = passenger
        this.cellphone = cellphone
        this.tpe = tpe
        this.orden = orden
        this.type = type
    }

    constructor(location: String?):super() {
        this.location = location
        this.document = ""
        this.geolocate = ""
        this.passenger = ""
        this.cellphone = ""
        this.tpe = ""
        this.orden = 1
        this.type = ""
    }


}