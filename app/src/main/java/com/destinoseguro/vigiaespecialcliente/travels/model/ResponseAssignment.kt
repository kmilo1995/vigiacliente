package com.destinoseguro.vigiaespecialcliente.travels.model

/**
 * Created by root on 20/12/17.
 */
class ResponseAssignment {

    var success: String? = null
    var data: List<AssignmentServer>? = null

    /**
     * No args constructor for use in serialization
     *
     */
    constructor() {}

    /**
     *
     * @param data
     * @param success
     */
    constructor(success: String, data: List<AssignmentServer>) : super() {
        this.success = success
        this.data = data
    }

}


class AssignmentServer{

     var id: String? = null
     var conductor: String? = null
     var celular: String? = null
     var placa: String? = null
     var nombre: String? = null
     var modelo: String? = null
     var fotovehiculo1: String? = null
     var fotovehiculo2: String? = null
     var fotoconductor: String? = null

    constructor() {}

    /**
     *
     * @param nombre
     * @param id
     * @param conductor
     * @param placa
     * @param celular
     */
    constructor(id: String, conductor: String, celular: String, placa: String, nombre: String,modelo: String,fotovehiculo1: String,fotovehiculo2: String,fotoconductor:String ): super(){

        this.id = id
        this.conductor = conductor
        this.celular = celular
        this.placa = placa
        this.nombre = nombre
        this.modelo = modelo
        this.fotovehiculo1 = fotovehiculo1
        this.fotovehiculo2 = fotovehiculo2
        this.fotoconductor = fotoconductor
    }

    override fun toString(): String {
        return "AssignmentServer(id=$id, conductor=$conductor, celular=$celular, placa=$placa, nombre=$nombre, modelo=$modelo, fotovehiculo1=$fotovehiculo1, fotovehiculo2=$fotovehiculo2, fotoconductor=$fotoconductor)"
    }


}