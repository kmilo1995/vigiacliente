package com.destinoseguro.vigiaespecialcliente.travels.objects

import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.content.res.Resources
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.provider.MediaStore
import android.support.v4.view.PagerAdapter
import android.util.Base64
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.destinoseguro.vigiaespecialcliente.R
import com.destinoseguro.vigiaespecialcliente.travels.model.AssignmentServer
import kotlinx.android.synthetic.main.assigned_row.view.*
import android.content.pm.PackageManager
import android.net.Uri
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.widget.Toast
import android.support.v4.content.ContextCompat.startActivity
import com.destinoseguro.vigiaespecialcliente.travels.activitys.photoThumbnail


/**
 * Created by root on 11/01/18.
 */
class listAsignedAdapter(): PagerAdapter() {

      var listAsignedDriver:List<AssignmentServer>? = null



    constructor(listAsignedDriver:List<AssignmentServer>?) : this() {

           this.listAsignedDriver = listAsignedDriver

    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view == `object`
    }

    override fun getCount(): Int {
       return listAsignedDriver!!.size
    }

    override fun destroyItem(container: View, position: Int, `object`: Any) {
        super.destroyItem(container, position, `object`)
    }


    override fun instantiateItem(container: ViewGroup, position: Int): Any {


        val view = LayoutInflater.from(container.context).inflate(R.layout.assigned_row, container, false)

        var phoneDriver = listAsignedDriver?.get(position)!!.celular
        view.assignedName.text = listAsignedDriver?.get(position)!!.conductor
        view.assignedPhone.text = phoneDriver
        view.assignedMarket.text = listAsignedDriver?.get(position)!!.nombre
        view.assignedModel.text = listAsignedDriver?.get(position)!!.modelo
        view.assignedPlate.text = listAsignedDriver?.get(position)!!.placa

        view.assignedPhone.setOnClickListener( {
            uploadImage(phoneDriver.toString(),container.context)
        })

        var photoDriverBitmap = convertStringToBitmap(listAsignedDriver?.get(position)!!.fotoconductor.toString())
        var photoVehicleOneBitmap = convertStringToBitmap(listAsignedDriver?.get(position)!!.fotoconductor.toString())
        var photoVehicleTwoBitmap = convertStringToBitmap(listAsignedDriver?.get(position)!!.fotoconductor.toString())

        view.assignedPhotoVehicleOne.setOnClickListener( {

//            Toast.makeText(container.context,"show photo",Toast.LENGTH_LONG).show()
            val Intent = Intent(container.context,photoThumbnail::class.java)
            container.context.startActivity(Intent)
        })

        view.assignedPhotoVehicleTwo.setOnClickListener( {

            //            Toast.makeText(container.context,"show photo",Toast.LENGTH_LONG).show()
            val Intent = Intent(container.context,photoThumbnail::class.java)
            container.context.startActivity(Intent)
        })

        if (photoDriverBitmap  != null ){

            view.assignedPhotoDriver.setImageBitmap(photoDriverBitmap)
        }
        if (photoVehicleOneBitmap  != null ){

            view.assignedPhotoVehicleOne.setImageBitmap(photoVehicleOneBitmap)


        }
        if (photoVehicleTwoBitmap  != null ){

            view.assignedPhotoVehicleTwo.setImageBitmap(photoVehicleTwoBitmap)
        }


        container.addView(view)

        return view

    }


    private fun uploadImage(number: String, context: Context) {

        val opciones = arrayOf<CharSequence>( "LLamar", "Whatsapp")
        val alertOpciones = AlertDialog.Builder(context)
        alertOpciones.setTitle("Contactar")
        alertOpciones.setItems(opciones) { dialogInterface, i ->
            if (opciones[i] == "LLamar") {


                if (opciones[i] == "LLamar") {

                  OnCallDriver(context,number)

                } else {
                    dialogInterface.dismiss()
                }
            }
            if (opciones[i] == "Whatsapp") {


                if (opciones[i] == "Whatsapp") {

                   sendMessageWhatsapp(context,"")

//                    startActivityForResult(Intent.createChooser(intent, "Seleccione la Aplicación"), COD_SELECCIONA)

                } else {
                    dialogInterface.dismiss()
                }
            }
        }
        alertOpciones.show()

    }

     fun sendMessageWhatsapp(context: Context,number:String){

         val sendIntent = Intent()
         sendIntent.action = Intent.ACTION_SEND
         sendIntent.putExtra(Intent.EXTRA_TEXT, "")
         sendIntent.type = "text/plain"
         context.startActivity(sendIntent)
    }


    fun convertStringToBitmap(imgStringBase64: String): Bitmap? {

        var decodedImage = BitmapFactory.decodeResource(Resources.getSystem(), R.drawable.perfil)
        if (!imgStringBase64.isEmpty()){

            var imageBytes = Base64.decode(imgStringBase64, Base64.DEFAULT)
            Log.e("imageBytes",imageBytes.toString())
            decodedImage = BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.size)
        }



        return decodedImage
    }

    fun OnCallDriver(context: Context,number:String){


        var callInten = Intent(Intent.ACTION_CALL)
        callInten.setData(Uri.parse("tel:$number"))


        if (ContextCompat.checkSelfPermission(context,
                android.Manifest.permission.CALL_PHONE)
                != PackageManager.PERMISSION_GRANTED){

        }

        try {

            context.startActivity(callInten)
        }catch (e:SecurityException){
            Toast.makeText(context,R.string.messagePermisionPhone,Toast.LENGTH_LONG).show()
        }
    }
}