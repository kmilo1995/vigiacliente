package com.destinoseguro.vigiaespecialcliente.google_maps_api;

import com.google.maps.GeoApiContext;

/**
 * Created by root on 23/01/18.
 */

public class GeoApiContextInstance {

    private GeoApiContext context = null;

    public GeoApiContext instance(){

        if (context != null){

            return context;
        }else {

            return new GeoApiContext.Builder()
                    .apiKey("AIzaSyBaNiJG6xttK2-0xSDc1p5JX8gJLo9cPnY")
                    .build();


        }


    }

}
