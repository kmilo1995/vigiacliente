package com.destinoseguro.vigiaespecialcliente.firebase;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


/**
 * Created by root on 15/09/16.
 */

public  class Firebase
{
    private FirebaseDatabase database  = null;
    private DatabaseReference myRef   = null;
    private GoogleSignInAccount googleSignInAccount = null;
    private AppCompatActivity activity = null;
    //para poder realizar  el crud sin problemas hay que autenticarce en Firebase
    //variable para la autenticacion en Firebase
    private FirebaseAuth mAuth;
    private  FirebaseAuth.AuthStateListener mAuthListener;

    private static Firebase INSTANCE = null;

    private  boolean authen;
    @SuppressWarnings("unchecked")
    private Gson gson;


    //////////////////////////////////////
    //constructor de la clase Firebase
    private Firebase()
    {
        authen = true;
        gson = new Gson();
        database = FirebaseDatabase.getInstance();
        mAuth = FirebaseAuth.getInstance();
        if(database != null)
        {
            try
            {
                database.setPersistenceEnabled(true);
            }
             catch (Exception e)
            {
              Log.d("Firebase",e.getLocalizedMessage());
            }
        }
        //token Firebase
        //String token = FirebaseInstanceId.getInstance().getToken();
        //Log.d("TOKEN :"," ESTE ES EL TOKEN : "+token);
        //responde a los cambios de sesion del usuario
        mAuthListener = new FirebaseAuth.AuthStateListener()
        {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null)
                {
                    // User is signed in
                    Log.d("FIREBASE-IN", "UNO:" + user.getUid());
                } else
                {
                    // User is signed out
                    Log.d("FIREBASE-OUT", " DOS  "+ user.getUid());
                }
                // ...
            }
        };
    }

    public static Firebase getInstance(){

        if(INSTANCE == null){

            INSTANCE = new Firebase();


        }

        return INSTANCE;

    }

    ///optiene el objeto de google para realizar la autenticacion
    public void setGoogleAccount(GoogleSignInAccount gc, AppCompatActivity act)
    {
        googleSignInAccount = gc;
        activity  = act;
    }

    public void setGoogleSignInAccountCtx(GoogleSignInAccount gc, Context context)
    {
        googleSignInAccount = gc;
        activity  = (AppCompatActivity) context;
    }
    ///valida el usuario autenticado por google
    /*
     *  este metodo valida si el suario esta autenticado por google
     *  en caso cntrario crea un token anonimo
     */////////////////////////////////////////////////
    ///////////////////////////////////////////////////
    private boolean firebaseLogin(GoogleSignInAccount accte)
    {
        Object anonymus =  accte;
        //si la autenticacion es anonima
        if(anonymus == null)
        {

            mAuth.signInAnonymously().addOnCompleteListener(new OnCompleteListener<AuthResult>()
            {
                @Override
                public void onComplete(@NonNull Task<AuthResult> task)
                {
                    Log.d("FIREBASE", "TOKEN ANONYMO FIREBASE : " + task.isSuccessful());
                    if (!task.isSuccessful())
                    {
                        Log.w("FIREBASE", " NO ES POSIBLE LA AUTENTICACION ANONIMA : " + task.getException());
                        authen = false;
                    }
                }
            });
        }
        else  //si la autentiacion es por google
        {
            //Log.d("Firebase","Firebase autenticacion id"+accte.getId());
            AuthCredential credential = GoogleAuthProvider.getCredential(accte.getIdToken(), null);
            mAuth.signInWithCredential(credential).addOnCompleteListener(activity, new OnCompleteListener<AuthResult>() {
                @Override
                public void onComplete(@NonNull Task<AuthResult> task) {
                    Log.d("FIREBASE", "TOKEN FIREBASE : " + task.isSuccessful());
                    if (!task.isSuccessful()) {
                        Log.w("FIREBASE", " NO ES POSIBLE LA AUTENTICACION DEL USUARIO POR GOOGLE : " + task.getException());
                        authen = false;
                    }

                }
            });
        }
        return authen;
    }
    //////////////////////////////////////////////////////////////
    ///crea un nuevo nodo en la base de datos de Firebase////////
    /////////////////////////////////////////////////////////////
    public boolean setInsertNewNodo(Object nodo, String Reference)
    {
        boolean insert = true;
        //si la autentiacion es correcta  realiza el insert
        if(firebaseLogin(googleSignInAccount))
        {

            myRef =  database.getReference();
            myRef.keepSynced(true);
            myRef.child(Reference).setValue(nodo);

        }
        else
        {
            insert = false;
        }
        return insert;
    }

    public boolean setInsertNewFiel(String field, String Reference)
    {
        boolean insert = true;
        //si la autentiacion es correcta  realiza el insert
        if(firebaseLogin(googleSignInAccount))
        {

            myRef =  database.getReference();
            myRef.keepSynced(true);
            myRef.child(Reference).setValue(field);

        }
        else
        {
            insert = false;
        }
        return insert;
    }

    ////permite consultar la base de datos de Firebase
    public void getSelectNodos(final String documento, final String campo)
    {

        final HashMap<String,ArrayList> data = new HashMap<>();
        final ArrayList<String> resp2 = new ArrayList<String>();
        if(firebaseLogin(googleSignInAccount))
        {
            myRef =  database.getReference(documento);
            myRef.addValueEventListener(new ValueEventListener()
            {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot)
                {
                    Object value = new HashMap<>();
                    value = dataSnapshot.child(campo).getValue();
                    if(value == null)
                    {
                        for (DataSnapshot dt : dataSnapshot.getChildren())
                        {
                            for (DataSnapshot sbDS : dt.getChildren())
                            {
                                if (sbDS.getKey().equals(campo))
                                {
                                    value = sbDS.getValue();
                                    resp2.add(gson.toJson(value));
                                    data.put(campo, resp2);
                                }
                            }
                            Log.d("POR", "FIN");
                        }
                    }else
                    {
                        resp2.add(gson.toJson(value));
                        data.put(campo,resp2);
                    }

                }

                @Override
                public void onCancelled(DatabaseError databaseError)
                {
                    Log.d("Error","Error en el metodo select "+databaseError.toException().toString());
                }

            });
        }
        else
        {
            Log.d("joder","la consulta esta cameyuda");
        }
    }
    ////////////////////////////
    public void getSelectTotalNodos(final String documento) {

        final HashMap<String,String> data = new HashMap<>();
        final ArrayList<String> resp2 = new ArrayList<String>();
        if(firebaseLogin(googleSignInAccount))
        {
            myRef =  database.getReference(documento);
            myRef.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot)
                {
                    for (DataSnapshot dt : dataSnapshot.getChildren())
                    {
                        for(DataSnapshot sbDS : dt.getChildren())
                        {
                           ArrayList<String> tem = new ArrayList<String>();

                        }
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }
    }
    ////////////////////////////
    public DatabaseReference getSelectAlterna(final String documento)
    {
        DatabaseReference temp = null;
        if(firebaseLogin(googleSignInAccount))
        {
            temp = database.getReference(documento);
            temp.keepSynced(true);

        }
        return  temp;
    }

    ////////////////////////////

    ///////////////
    //@Override
    public void onStart()
    {
        //super.onStart();
        database.setPersistenceEnabled(true);
        mAuth.addAuthStateListener(mAuthListener);
    }
    //
    public void onStop()
    {
        // super.onStop();
        if (mAuthListener != null) {
            mAuth.removeAuthStateListener(mAuthListener);
        }
    }
    ////////////////////
    public String getToken()
    {
        String token = FirebaseInstanceId.getInstance().getToken();
        return token;
    }
    /////////////
    /*
     *  Method of actulization of fields
     */
    public boolean  firebaseUpdate(Map<String, Object> nodo, String key)
    {
        boolean update = true;
        if(firebaseLogin(googleSignInAccount))
        {

            myRef = database.getReference();
            myRef.keepSynced(true);
            myRef.child(key).updateChildren(nodo);
        }else
        {
            update = false;
        }

        return update ;
    }


}