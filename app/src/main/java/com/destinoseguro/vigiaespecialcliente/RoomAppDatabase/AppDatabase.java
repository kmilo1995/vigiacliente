package com.destinoseguro.vigiaespecialcliente.RoomAppDatabase;

/**
 * Created by root on 4/12/17.
 */

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.migration.Migration;
import android.content.Context;
import android.support.annotation.NonNull;

import com.destinoseguro.vigiaespecialcliente.register.model.Role;
import com.destinoseguro.vigiaespecialcliente.register.model.RoleDao;
import com.destinoseguro.vigiaespecialcliente.register.model.User;
import com.destinoseguro.vigiaespecialcliente.register.model.UserDao;
import com.destinoseguro.vigiaespecialcliente.requests.model.Passenger;
import com.destinoseguro.vigiaespecialcliente.requests.model.PassengerDao;
import com.destinoseguro.vigiaespecialcliente.travels.model.Travel;
import com.destinoseguro.vigiaespecialcliente.travels.model.TravelDao;

@Database (entities = {Travel.class, Passenger.class, User.class, Role.class}, version = 1)
public abstract class AppDatabase extends RoomDatabase {


    private static AppDatabase INSTANCE;

    public abstract TravelDao TravelDao();
    public abstract PassengerDao PassengerDao();
    public abstract UserDao UserDao();
    public abstract RoleDao RoleDao();
//    public abstract Passenger PassengerDao();


//    static final Migration MIGRATION_1_2 = new Migration(1,2) {
//        @Override
//        public void migrate(@NonNull SupportSQLiteDatabase database) {
//
//        }
//    };

    public static AppDatabase getAppDatabase(Context context) {
        if (INSTANCE == null) {
            INSTANCE =
                    Room.databaseBuilder(context.getApplicationContext(), AppDatabase.class, "internal-database")
                            // allow queries on the main thread.
                            // Don't do this on a real app! See PersistenceBasicSample for an example.
                            .allowMainThreadQueries()
                           // .addMigrations(MIGRATION_1_2)
                            .build();
        }
        return INSTANCE;
    }

    public static void destroyInstance() {
        INSTANCE = null;
    }


}

