package com.destinoseguro.vigiaespecialcliente.register.model;

import com.destinoseguro.vigiaespecialcliente.restApi.ConstantsRestApi;

import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by root on 24/11/17.
 */

public interface EndPointsRegister {

    // http://192.168.100.56/appvigia/app/cliente/setRegister​
    @FormUrlEncoded
    @POST(ConstantsRestApi.SET_REGISTER)
    Call<ResponseRegister> setRegister(
            @Field("imei") String imei,
            @Field("documento") String documento,
            @Field("nombre") String nombre,
            @Field("apellido") String apellido,
            @Field("celular") String celular,
            @Field("latitud") String latitud,
            @Field("longitud") String longitud,
            @Field("lugar") String lugar,
            @Field("tokenDispositivo") String tokenDispositivo
    );



//    http://192.168.100.56/appvigia/app/cliente/getValidateImei
      @FormUrlEncoded
      @POST(ConstantsRestApi.VALIDATE_iMEI)
      Call<ResponseRegister> getValidateImei(@Field("imei") String imei);


    @POST(ConstantsRestApi.END)
    Call<List<pojo>> getnames( );





}
