package com.destinoseguro.vigiaespecialcliente.register.model

import android.arch.persistence.room.*

/**
 * Created by root on 13/12/17.
 */
@Dao
interface RoleDao {

    @Query("SELECT * FROM table_role")
    fun listPermissions():List<Role>


    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(role:Role)

    @Update
    fun update(role:Role):Int


    @Query("DELETE FROM table_role")
    fun nukaTable()

}