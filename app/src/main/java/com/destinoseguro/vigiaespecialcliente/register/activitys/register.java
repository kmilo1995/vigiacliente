package com.destinoseguro.vigiaespecialcliente.register.activitys;

import android.content.Context;
import android.content.Intent;
import android.os.Parcelable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.destinoseguro.vigiaespecialcliente.R;
import com.destinoseguro.vigiaespecialcliente.RoomAppDatabase.AppDatabase;
import com.destinoseguro.vigiaespecialcliente.dashboard.activitys.dashboard;
import com.destinoseguro.vigiaespecialcliente.firebase.Firebase;
import com.destinoseguro.vigiaespecialcliente.register.model.EndPointsRegister;
import com.destinoseguro.vigiaespecialcliente.register.model.RequestFirebase;
import com.destinoseguro.vigiaespecialcliente.register.model.ResponseRegister;
import com.destinoseguro.vigiaespecialcliente.register.model.Role;
import com.destinoseguro.vigiaespecialcliente.register.model.User;
import com.destinoseguro.vigiaespecialcliente.restApi.RestApiAdapter;
import com.google.firebase.FirebaseApp;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.Checked;
import com.mobsandgeeks.saripaar.annotation.Email;
import com.mobsandgeeks.saripaar.annotation.Length;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class register extends AppCompatActivity implements Validator.ValidationListener {


  private final  RestApiAdapter restApiAdapter = new RestApiAdapter();



  private final int REQUEST_ACCESS_FINE_LOCATION = 5;

  @Length(sequence = 1, min = 3, message = "Documento debe contener minimo 6 digitos")
  private TextView document = null;
  @Length(sequence = 2, min = 6, message = "Ingresa un nombre")
  private TextView name = null;
  @Length(sequence = 3, min = 3, message = "Ingresa un apellido")
  private TextView lastName = null;
  @Length(sequence = 4, min = 10, max = 15, message = "Numero debe contener minimo 7 digitos")
  private TextView phone = null;
  @Email(message = "Ingrese in correo valido")
  private TextView mail = null;
  @Checked(message = "Para Continuar debes aceptar terminos y condiciones")
  private CheckBox conditions = null;

   private Validator validator;
   public static String IMEI = null;
//
   private Firebase firebase = Firebase.getInstance();
   private RequestFirebase requestFirebase;

   private Context context;
   private Retrofit retrofit = restApiAdapter.conectionApiRest();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        IMEI = getIntent().getExtras().getString("imei");
        context = this;
        requestFirebase = new RequestFirebase(this);

        setContentView(R.layout.activity_register);


        Button registerBtnRegister =  findViewById(R.id.registerBtnRegister);
        document =  findViewById(R.id.registertxt_document);
        name =  findViewById(R.id.registertxt_name);
        lastName =  findViewById(R.id.registertxt_lastname);
        phone =  findViewById(R.id.registertxt_phone);
        mail =  findViewById(R.id.registertxt_mail);
        conditions =  findViewById(R.id.registerCheck_conditions);
        FirebaseApp.initializeApp(this);
        validator = new Validator(this);
        validator.setValidationListener(this);



        registerBtnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


//                validar los campos
                validator.validate();


            }
        });


    }


    @Override
    public void onValidationSucceeded() {

        User newUser = new User(
                document.getText().toString(),
                mail.getText().toString(),
                lastName.getText().toString(),
                "4.56665",
                "-7.4654654",
                name.getText().toString(),
                phone.getText().toString(),
                "Bogota",
                true,
                "special",
                "",
                firebase.getToken()
        );

        Log.e("register","onValidationSucceeded "+newUser.toString());
        registerNewUser(newUser);

    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {

        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);


            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(this, message, Toast.LENGTH_LONG).show();
            }
        }
    }





    public  void registerNewUser(final User user){

        if(IMEI != null){
//            saveNewUserServer(user,this,db,retrofit);
            Log.e("register","registerNewUser "+IMEI);
//              genera un nuevo nodo en firebase
            saveNewUserServer(user , firebase , retrofit);

        }
    }



    public void userFirebase(String IMEI ){


        firebase.getSelectAlterna("Usuarios/Customers/" + IMEI.toString()).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

//                Log.e("user", "dataSnapshot " + dataSnapshot.toString());
                Log.e("register onDataChange ","userFirebase "+dataSnapshot.toString());

                if(Boolean.parseBoolean(dataSnapshot.child("activation").getValue().toString())){

                    getRolesFirebase(dataSnapshot.child("roleName").getValue().toString());
                }else{
                    Intent intent = new Intent(context,Preactivation.class);
                    startActivity(intent);
                    finish();

                }

//                createOrUpdateUser(dataSnapshot,user)
//                saveNewUserServer(loadUser(dataSnapshot) , context, db, retrofit)
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e("onCancelled ", "userFirebase " + databaseError.getMessage());
            }
        });



    }


    private void getRolesFirebase(String roleName) {

        if (roleName != null) {
            Log.e("RoleName",  roleName);

            firebase.getSelectAlterna("Aplicaciones/Customer/Roles/" + roleName).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {


                    Log.e("register onDataChange","getRolesFirebase "+ dataSnapshot.toString());

                    AppDatabase db = AppDatabase.getAppDatabase(context);
                    db.RoleDao().nukaTable();

                    for (DataSnapshot role : dataSnapshot.getChildren()) {

//                        roles.add(role.getValue().toString());

                        Role rol = new Role(Integer.parseInt(role.getKey().toString()),role.getValue().toString());

                        int response = db.RoleDao().update(rol);
                        if (response == 0){

                            db.RoleDao().insert(rol);
                        }
                    }


                    Intent intent = new Intent(context,dashboard.class);

                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    finish();
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    Log.e("onCancelled ", "getRolesFirebase " + databaseError.getMessage());
                }
            });

        }

    }





    public  void saveNewUserServer(final User user,final Firebase firebase ,Retrofit retrofit) {


        // obtiene los datos de firebase y  guarda los datos en el servidor
        Call<ResponseRegister> response = retrofit
                .create( EndPointsRegister.class).setRegister(
                        IMEI,
                        user.getDocument().toString(),
                        user.getName(),
                        user.getLastName(),
                        user.getPhone(),
                        user.getLatitude(),
                        user.getLongitude(),
                        user.getPlace(),
                        user.getToken()
                );

        // resive la respuesta del servidor
        response.enqueue(new Callback<ResponseRegister>() {
            @Override
            public void onResponse(Call<ResponseRegister> call, Response<ResponseRegister> response) {

                Log.e("respuesta registro", String.valueOf(response.toString()));


                try {

                    Log.e("register onResponse ","saveNewUserServer "+response.body().getData()+" "+response.body().getSuccess());
                    //                               valida respuesta del servidor codigo de solicitud y success de respuesta
                    if(response.code() == 200 && response.body().getSuccess()){

                        firebase.setInsertNewNodo(user, "Usuarios/Customers/"+IMEI.toString().replaceAll("[.\\s]", ""));
//                      consulta los datos ingresados
                        userFirebase(IMEI);
//
//

                    }

                }catch (Exception e){
                    Log.e("register exception ", "setRegister "+e.getMessage());
                    Toast.makeText(context,response.toString(),Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void onFailure(Call<ResponseRegister> call, Throwable t) {
                Log.e("onFailure setRegister",""+t.getMessage());
            }
        });




    }




}

