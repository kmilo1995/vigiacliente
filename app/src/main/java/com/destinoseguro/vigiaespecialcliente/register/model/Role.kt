package com.destinoseguro.vigiaespecialcliente.register.model

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

/**
 * Created by root on 13/12/17.
 */
@Entity(tableName = "table_role")
class Role(
        @PrimaryKey
        var id:Int = 0,
        @ColumnInfo(name = "role_name")
        var name:String? = null,
        @ColumnInfo(name = "access")
        var access:Boolean? = false,
        @ColumnInfo(name = "id_resource")
        var idResourse:Int? = null

) {




    constructor(id:Int,name: String?):this(id,name,false,null)

    override fun toString(): String {
        return "Role(name=$name, access=$access, idResourse=$idResourse, id=$id)"
    }


}