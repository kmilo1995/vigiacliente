package com.destinoseguro.vigiaespecialcliente.register.model

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

/**
 * Created by root on 12/12/17.
 */
@Entity(tableName = "table_user")
class User(
        @ColumnInfo(name = "document")
        var document: String?,
        @ColumnInfo(name = "email")
        var email: String?,
        @ColumnInfo(name = "last_name")
        var lastName: String?,
        @ColumnInfo(name = "latitude")
        var latitude: String?,
        @ColumnInfo(name = "longitude")
        var longitude: String?,
        @ColumnInfo(name = "name")
        var name: String?,
        @ColumnInfo(name = "phone")
        var phone: String?,
        @ColumnInfo(name = "place")
        var place: String?,
        @ColumnInfo(name = "activation")
        var activation: Boolean? ,
        @ColumnInfo(name = "role_name")
        var roleName:String?,
        @ColumnInfo(name = "photo")
        var photo:String?,
        @ColumnInfo(name = "token")
        var token: String?
) {
   @PrimaryKey(autoGenerate = true)
    var id:Int? = null

    constructor():this(null,null,null,null,null,null,null,null,null,null,null,null)

    override fun toString(): String {
        return "User(document=$document, email=$email, lastName=$lastName, latitude=$latitude, longitude=$longitude, name=$name, phone=$phone, place=$place, activation=$activation, roleName=$roleName, photo=$photo, token=$token)"
    }


}


