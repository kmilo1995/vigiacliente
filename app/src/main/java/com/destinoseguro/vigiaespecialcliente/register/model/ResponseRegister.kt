package com.destinoseguro.vigiaespecialcliente.register.model

/**
 * Created by root on 24/11/17.
 */
class ResponseRegister {

    var success: Boolean? = false
    var data: String? = null

    /**
     * No args constructor for use in serialization
     *
     */
    constructor() {}

    /**
     *
     * @param data
     * @param success
     */
    constructor(success: Boolean, data: String) : super() {
        this.success = success
        this.data = data
    }

}