package com.destinoseguro.vigiaespecialcliente.register.model

import android.content.Context
import android.content.Intent
import android.content.Intent.getIntent
import android.provider.Settings
import android.support.design.widget.NavigationView
import android.support.v4.content.ContextCompat.startActivity
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.widget.Toast
import com.destinoseguro.vigiaespecialcliente.Launch_Activity
import com.destinoseguro.vigiaespecialcliente.RoomAppDatabase.AppDatabase
import com.destinoseguro.vigiaespecialcliente.dashboard.activitys.dashboard
import com.destinoseguro.vigiaespecialcliente.firebase.Firebase
import com.destinoseguro.vigiaespecialcliente.register.activitys.register
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import retrofit2.Retrofit

/**
 * Created by root on 13/12/17.
 */
class RequestFirebase(private val context: AppCompatActivity) {

   val IMEI:String = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID)

    private val firebase = Firebase.getInstance()

    val db = AppDatabase.getAppDatabase(context)

//    fun getRolesFirebase(navigationView: NavigationView) {
    fun getRolesFirebase(navigationView: NavigationView) {

        if (db.UserDao().user().roleName != null) {
            Log.e("RoleName",  db.UserDao().user().toString())
            firebase.getSelectAlterna("Aplicaciones/Customer/Roles/" + db.UserDao().user().roleName!!)!!.addValueEventListener(object : ValueEventListener {

                override fun onDataChange(dataSnapshot: DataSnapshot) {


                    Log.e("rolname ", db.UserDao().user().roleName.toString())
                    for (data in dataSnapshot.children) {

                        Log.e("recuestfirebase ", "" + data.toString())

                        var role= Role(data.key.toInt(),data.value.toString())

                        var response = db.RoleDao().update(role)
                        if (response == 0){

                            db.RoleDao().insert(role)
                        }

                    }

//                    dashboard.hideItems(navigationView,db,context)

                }

                override fun onCancelled(databaseError: DatabaseError) {
                    Log.e("onCancelled ", "getRolesFirebase " + databaseError.message)
                }
            })
        }

    }



    fun userFirebase(IMEI:String,retrofit:Retrofit,user:User){

        firebase.getSelectAlterna("Usuarios/Customers/" + IMEI.toString())!!.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {





                Log.e("user", "dataSnapshot " + dataSnapshot.toString())

                createOrUpdateUser(dataSnapshot,user)
//                register.saveNewUserServer(db.UserDao().user(), context, db, retrofit)


            }

            override fun onCancelled(databaseError: DatabaseError) {
                Log.e("onCancelled ", "userFirebase " + databaseError.message)
            }
        })

    }


    fun createOrUpdateUser(dataSnapshot:DataSnapshot,user:User?){

        Log.e("uupdate algo cliente ", "" + dataSnapshot )
        var newUser = loadUser(dataSnapshot)



        var response = db.UserDao().update(newUser)
        if(response == 0){

            if (user != null) {
                db.UserDao().insert(user)
            }
        }

        Log.e("res update", "db" +response)
        Log.e("user", "db ${db.UserDao().user()}")
    }





     fun validateActivationAccount(IMEI:String) {

            firebase.getSelectAlterna("Usuarios/Customers/" + IMEI)!!.addValueEventListener(object : ValueEventListener {
                override fun onDataChange(dataSnapshot: DataSnapshot) {
                    try {

                        Log.e("evento ", "" + dataSnapshot + "///" + (dataSnapshot.child("activation").value!!.toString() == "true"))
                        var user =  db.UserDao().user()
                        var roleName = dataSnapshot.child("roleName").value!!.toString()
                        createOrUpdateUser(dataSnapshot,user)
                        user.roleName = roleName
//                        showActivityValidateAcount(dataSnapshot.child("activation").value.toString().toBoolean(),context)

                    } catch (e: NullPointerException) {
                        var user = loadUser(dataSnapshot)

                        if (user.token != null){

                            db.UserDao().insert(user)

                            validateActivationAccount(IMEI)

                        }


                        Log.e("exception firebase", "validateActivationAccount " + e.message)
                        Toast.makeText(context, "Error de cuenta con firebase", Toast.LENGTH_LONG).show()


                    }

                }

                override fun onCancelled(databaseError: DatabaseError) {
                    Log.e("onCancelled ", "firebase validation " + databaseError.message)
                }
            })





    }


    fun loadUser(dataSnapshot:DataSnapshot):User{

        var user = User(
                dataSnapshot.child("document").value.toString(),
                dataSnapshot.child("email").value as String,
                dataSnapshot.child("lastName").toString(),
                dataSnapshot.child("latitude").toString(),
                dataSnapshot.child("logitude").toString(),
                dataSnapshot.child("name").toString(),
                dataSnapshot.child("phone").toString(),
                dataSnapshot.child("place").toString(),
                dataSnapshot.child("activation").value as Boolean,
                dataSnapshot.child("roleName").toString(),
                dataSnapshot.child("photo").toString(),
                dataSnapshot.child("token").toString()
        )

        return user
    }







}