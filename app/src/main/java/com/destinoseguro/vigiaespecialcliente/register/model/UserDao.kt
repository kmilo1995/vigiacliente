package com.destinoseguro.vigiaespecialcliente.register.model

import android.arch.persistence.room.*

/**
 * Created by root on 12/12/17.
 */
@Dao
interface UserDao {


    @Query("SELECT * FROM table_user LIMIT 1")
    fun user():User

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(user:User)

    @Update
    fun update(user: User):Int


    @Delete
    fun delete(user: User)


    @Query("DELETE FROM table_user")
    fun nukaTable()

}