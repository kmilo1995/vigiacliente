package com.destinoseguro.vigiaespecialcliente.requests.model

import android.annotation.SuppressLint
import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import android.graphics.Bitmap



/**
 * Created by root on 4/12/17.
 */

var direccionorigenPasajero: String? = null
var direcciondestinoPasajero: String? = null
var fechainicioPasajero: String? = null
var nombrePasajero: String? = null
var apellidoPasajero: String? = null
var celularPasajero: String? = null
var documentoPasajero: String? = null

@SuppressLint("ParcelCreator")
@Parcelize
@Entity (tableName = "table_passenger")
class Passenger(
                @ColumnInfo(name = "name")
                var name: String?,
                @ColumnInfo(name = "last_name")
                var lastName: String?,
                @ColumnInfo(name = "document")
                var document: String?,
                @ColumnInfo(name = "phone_number")
                var phoneNumber: String?,
                @ColumnInfo(name = "origin")
                var origin: String?,
                @ColumnInfo(name = "destination")
                var destination: String?,
                @ColumnInfo(name = "observation")
                var observation: String?,
                @ColumnInfo(name = "start_date")
                var startDate: String?,
                @ColumnInfo(name = "user_id")
                var travelId: Int?): Parcelable {

    @PrimaryKey(autoGenerate = true)
    var id:Int? = null


    constructor( ):this(null,null,null,null,null,null,null,null,null)

    override fun toString(): String {
        return "Passenger(name=$name, lastName=$lastName, document=$document, phoneNumber=$phoneNumber, origin=$origin, destination=$destination, observation=$observation, startDate=$startDate, travelId=$travelId, id=$id)"
    }


}






