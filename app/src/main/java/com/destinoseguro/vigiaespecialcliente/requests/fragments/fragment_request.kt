package com.destinoseguro.vigiaespecialcliente.requests.fragments

import android.annotation.SuppressLint
import android.annotation.TargetApi
import android.app.Activity.RESULT_OK
import android.app.DatePickerDialog
import android.app.Fragment
import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.graphics.*
import android.location.Address
import android.location.Geocoder
import android.os.Bundle
import android.os.Build
import android.provider.Settings
import android.support.annotation.RequiresApi
import android.support.v4.content.ContextCompat
import android.text.InputType
import android.util.Base64
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.location.places.Place
import com.google.android.gms.location.places.ui.PlacePicker
import com.destinoseguro.vigiaespecialcliente.R
import com.destinoseguro.vigiaespecialcliente.RoomAppDatabase.AppDatabase
import com.destinoseguro.vigiaespecialcliente.dashboard.activitys.dashboard
import com.destinoseguro.vigiaespecialcliente.requests.activitys.AddPassengersActivity
import com.destinoseguro.vigiaespecialcliente.requests.activitys.ShowListPassengers
import com.destinoseguro.vigiaespecialcliente.requests.model.*
import com.destinoseguro.vigiaespecialcliente.requests.objects.setTime
import com.destinoseguro.vigiaespecialcliente.requests.objects.spinnerArrayAdapterList
import com.destinoseguro.vigiaespecialcliente.restApi.RestApiAdapter
import com.destinoseguro.vigiaespecialcliente.travels.model.Destino
import com.destinoseguro.vigiaespecialcliente.travels.model.Origen
import com.destinoseguro.vigiaespecialcliente.travels.model.Travel
import com.destinoseguro.vigiaespecialcliente.travels.objects.DirectionFinder
import com.destinoseguro.vigiaespecialcliente.travels.objects.DirectionFinderListener
import com.destinoseguro.vigiaespecialcliente.travels.objects.Route
import com.google.android.gms.maps.*
import com.google.android.gms.maps.model.*

import com.weiwangcn.betterspinner.library.material.MaterialBetterSpinner
import kotlinx.android.synthetic.main.fragment_request.*
import kotlinx.android.synthetic.main.fragment_request.view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import java.io.ByteArrayOutputStream
import java.io.IOException
import java.io.UnsupportedEncodingException
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


/**
 * Created by root on 16/11/17.
 */
class fragment_request: Fragment(), GoogleApiClient.OnConnectionFailedListener , View.OnClickListener
       // ,OnMapReadyCallback
        , OnMapReadyCallback, DirectionFinderListener
{



    private val PLACE_PICKER_REQUEST:Int = 1
    private val DATA_PASSENGER_REQUEST:Int = 2

    private val restApiAdapter:RestApiAdapter = RestApiAdapter()
    private val retrofit:Retrofit = restApiAdapter.conectionApiRest()

    private var inputLocation:View? = null

    private val calendar: Calendar = Calendar.getInstance()

    private var estados = "Solicitado"

    var listOrigins: List<Origen>? = null
    var listDestinations: List<Destino>? = null

    var imgRuta : String? = null
    var mileage = ""

     var selected:Int = 0
     var selectedService:Int = 0
     var selectedOperation:Int = 0
     var selectedVehicle:Int = 0

    var jsonPassengers:String? = null

    var idTravel:Int? = null

    var SPINNERLISTSERVICES = ArrayList<itemSpinner>()
    var SPINNERLISTOPERATIONS = ArrayList<itemSpinner>()
    var SPINNERLISTVEHICLES = ArrayList<itemSpinner>()

    companion object {

         val listPassengers = ArrayList<Passenger>()

    }


    private var  dateSetListener = object : DatePickerDialog.OnDateSetListener {
         override fun onDateSet(view: DatePicker, year: Int, monthOfYear: Int,
                                dayOfMonth: Int) {
             calendar.set(Calendar.YEAR, year)
             calendar.set(Calendar.MONTH, monthOfYear)
             calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)
             updateDateInView()
         }
     }



    var vista:View? = null
    @SuppressLint("NewApi")
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {


        val view:View? = inflater.inflate(R.layout.fragment_request,container,false)

        if (dashboard.isConection(activity)){

            view!!.linearLayoutMessageNotConectionRequest.visibility = View.VISIBLE
        }else{
            view!!.linearLayoutMessageNotConectionRequest.visibility = View.GONE
        }
//        var view:View = inflater!!.inflate( R.layout.fragment_request,container,false)
        vista =view
        activity.title = "Solicitudes"




        SPINNERLISTSERVICES.add(itemSpinner("",""))

        setSpinner(SPINNERLISTSERVICES,vista!!.requestSpinner_typeServices)

        SPINNERLISTOPERATIONS.add(itemSpinner("",""))
        setSpinner(SPINNERLISTOPERATIONS,vista!!.requestSpinner_typeOperations)

        SPINNERLISTVEHICLES.add(itemSpinner("",""))
        setSpinner(SPINNERLISTVEHICLES,vista!!.requestSpinner_typeVehicles)

        loadAdapterVehicless()
        loadAdapterServices()
        loadAdapterOperations()

        initMap()


    //        escuchadores
        vista!!.requestTxt_Origin.setOnClickListener(this)
        vista!!.requestTxt_Destination.setOnClickListener(this)
        vista!!.requestTxt_Date.setOnClickListener(this)
        setTime(vista!!.requestTxt_time,activity)


    //        no permite inicializar el teclado cuando el foco se encuentra en los elementos
        vista!!.requestTxt_Origin.setInputType(InputType.TYPE_NULL)
        vista!!.requestTxt_Destination.setInputType(InputType.TYPE_NULL)
        vista!!.requestTxt_Date.setInputType(InputType.TYPE_NULL)
        vista!!.requestTxt_time.setInputType(InputType.TYPE_NULL)



        return vista
    }



    fun loadAdapterOperations(){

        val response = retrofit.create(EndPointsRequests::class.java).getTypeOperations()

//        var SPINNERLISTOPERATIONS = HashMap<String,String>()

        response.enqueue(object :Callback<ResponseSpinner>{

            override fun onResponse(call: Call<ResponseSpinner>?, response: Response<ResponseSpinner>?) {

                if (response != null) {

                    for (item: itemSpinner in response.body()!!.data!!) {

//                        SPINNERLISTOPERATIONS.add(item.nombre!!)

                        SPINNERLISTOPERATIONS.add(itemSpinner(item.id.toString(), item.nombre.toString()))
//                        SPINNERLISTOPERATIONS.put(item.id,item.nombre)

                        //
                    }

                    val arrayAdapterOperations = spinnerArrayAdapterList(activity!!, SPINNERLISTOPERATIONS)

                    requestSpinner_typeOperations.setAdapter(arrayAdapterOperations)
                    requestSpinner_typeOperations.setOnItemClickListener { parent, view, position, id ->
                        requestSpinner_typeOperations.setText(SPINNERLISTOPERATIONS.get(position).nombre.toString())
                        selectedOperation = position



                    }
                }

            }
            override fun onFailure(call: Call<ResponseSpinner>?, t: Throwable?) {

                Log.e("error spinner operation", t!!.message)
            }


        })

    }




    fun setSpinner(arrayList:ArrayList<itemSpinner>,spinner: MaterialBetterSpinner){

        arrayList.clear()
        val arrayAdapterOperations = spinnerArrayAdapterList(activity!!,arrayList)

        spinner.setAdapter(arrayAdapterOperations)
        spinner.setOnItemClickListener { parent, view, position, id ->
            spinner.setText(arrayList.get(position).nombre.toString())
            this.selected = position

        }
    }


    fun loadAdapterServices(){

        val response = retrofit.create(EndPointsRequests::class.java).getTypeServices()



        response.enqueue(object :Callback<ResponseSpinner>{

            override fun onResponse(call: Call<ResponseSpinner>?, response: Response<ResponseSpinner>?) {

                if (response != null) {


                    for (item: itemSpinner in response.body()!!.data!!) {


                        SPINNERLISTSERVICES.add(itemSpinner(item.id.toString(), item.nombre.toString()))

                        //
                    }


                    val arrayAdapterServices = spinnerArrayAdapterList(activity!!, SPINNERLISTSERVICES)

                    requestSpinner_typeServices.setAdapter(arrayAdapterServices)
                    requestSpinner_typeServices.setOnItemClickListener { parent, view, position, id ->
                        requestSpinner_typeServices.setText(SPINNERLISTSERVICES.get(position).nombre.toString())
                        selectedService = position


                    }
                }

            }
            override fun onFailure(call: Call<ResponseSpinner>?, t: Throwable?) {

                Log.e("error spinner Services", t!!.message)
            }


        })

    }


    fun loadAdapterVehicless(){

        val response = retrofit.create(EndPointsRequests::class.java).getTypeVehicles()




        response.enqueue(object :Callback<ResponseSpinner>{

            override fun onResponse(call: Call<ResponseSpinner>?, response: Response<ResponseSpinner>?) {

                if (response != null) {


                    for (item: itemSpinner in response.body()!!.data!!){


                        SPINNERLISTVEHICLES.add(itemSpinner( item.id.toString(), item.nombre.toString()))
//
                    }

                    val arrayAdapterVehicles = spinnerArrayAdapterList(activity,SPINNERLISTVEHICLES)

                    requestSpinner_typeVehicles.setAdapter(arrayAdapterVehicles)
                    requestSpinner_typeVehicles.setOnItemClickListener { parent, view, position, id ->
                        requestSpinner_typeVehicles.setText(SPINNERLISTVEHICLES.get(position).nombre.toString())
                        selectedVehicle = position



                    }


                }

            }
            override fun onFailure(call: Call<ResponseSpinner>?, t: Throwable?) {

                Log.e("error spinner vehicles", t!!.message)
            }


        })

    }





    private fun updateDateInView() {
        val myFormat = "yyyy-MM-dd" // mention the format you need
        val sdf = SimpleDateFormat(myFormat, Locale.US)
        requestTxt_Date!!.setText(sdf.format(calendar.getTime()))
    }







    @RequiresApi(Build.VERSION_CODES.N)
    override fun onClick(p0: View?) {


        when (p0!!.id) {
            R.id.boxInputs_Origin,
            R.id.textInputLayout_Origin,
            R.id.requestTxt_Origin,
            R.id.requestTxt_Destination ->  {

              inputLocation = p0
                //llamada al selector de sitios
                  val builder:PlacePicker.IntentBuilder  = PlacePicker.IntentBuilder()
                  startActivityForResult(builder.build(activity),PLACE_PICKER_REQUEST)

            }
            R.id.requestTxt_Date -> {

                DatePickerDialog(activity,
                        dateSetListener,
                        // set DatePickerDialog to point to today's date when it loads up
                        calendar.get(Calendar.YEAR),
                        calendar.get(Calendar.MONTH),
                        calendar.get(Calendar.DAY_OF_MONTH)).show()

            }


        }



    }



    fun createPassengers(){

        val intent = Intent(activity, AddPassengersActivity::class.java)


        intent.putExtra("ORIGIN_REQUEST",vista!!.requestTxt_Origin.text.toString())
        intent.putExtra("DESTINATION_REQUEST",vista!!.requestTxt_Destination.text.toString())
        startActivityForResult(intent,DATA_PASSENGER_REQUEST)

    }



    fun showListPassengers(){

        var intent = Intent(activity,ShowListPassengers::class.java)

        val bundle = Bundle()
        bundle.putParcelableArrayList("listPassengers", listPassengers)
        intent.putExtras(bundle)
        startActivity(intent)
    }




    fun saveRequest(){



        if (!dashboard.isConection(activity)){

            view!!.linearLayoutMessageNotConectionRequest.visibility = View.GONE
            var result =  validateFieldsRequest(listOf(
                    vista!!.requestTxt_Origin,
                    vista!!.requestTxt_Destination,
                    vista!!.requestTxt_Date,
                    view!!.requestSpinner_typeOperations,
                    view!!.requestSpinner_typeServices,
                    view!!.requestSpinner_typeVehicles,
                    view!!.requestTxt_time
            ))
                 var n = listPassengers.size

               if ( result ) {




                   var newRequest = Travel(

                           "",
                           "",
                           estados,
                           requestTxt_Origin.text.toString(),
                           requestTxt_Destination.text.toString(),
                           "",
                           requestTxt_Date.text.toString() + " " + requestTxt_time.text,
                           "",
                           imgRuta,
                           mileage,
                           SPINNERLISTSERVICES.get(selectedService).id,
                           SPINNERLISTVEHICLES.get(selectedVehicle).id,
                           SPINNERLISTOPERATIONS.get(selectedOperation).id,
                           requestTxt_observation.text.toString(),
                           jsonPassengers,
                           false,
                           false

                   )


                   takeSnapshot(newRequest)
               }
        }else{
            view!!.linearLayoutMessageNotConectionRequest.visibility = View.VISIBLE
        }


    }

    private fun setNewRequestInServer(jsonPassenger: String?, newRequest: Travel) {

         val restApiAdapter = RestApiAdapter()
         val retrofit = restApiAdapter.conectionApiRest()
        var IMEI = Settings.Secure.getString(activity!!.getContentResolver(), Settings.Secure.ANDROID_ID)



        var response: Call<ResponseSetRequest> = retrofit.create(EndPointsRequests::class.java).setRequest(
                newRequest.origin,
                newRequest.destination,
                newRequest.startTime,
                newRequest.observation,
                "1",
//                newRequest.goAndComeBack.toString(),
                newRequest.typeServices,
//                newRequest.typeServices,
                newRequest.typeVehicle,
//                newRequest.typeVehicle,
                jsonPassenger,
                newRequest.typeOperation,
//                newRequest.typeOperation,
                IMEI,
                mileage,
                imgRuta
        )

        Log.e("solicitud",newRequest.origin+" | "+newRequest.destination+" | "+newRequest.startTime+" | "+newRequest.observation+" | "+newRequest.goAndComeBack+" | "+newRequest.typeServices+" | "+newRequest.typeVehicle+" | "+jsonPassenger+" | "+newRequest.typeOperation+" | "+IMEI)
        response.enqueue(object :Callback<ResponseSetRequest>{

            override fun onResponse(call: Call<ResponseSetRequest>?, response: Response<ResponseSetRequest>?) {

                Log.e("services create success", response!!.body()!!.id.toString())

                val idNewTravel = response!!.body()!!.id.toString()

                if(response!!.code() != 500 && !idNewTravel.isNullOrEmpty()){

                    val db:AppDatabase = AppDatabase.getAppDatabase(activity)
                    newRequest.id = idNewTravel.toInt()
                    newRequest.code = idNewTravel

                    db.TravelDao().insert(newRequest)

                    Toast.makeText(activity,"Datos guardados",Toast.LENGTH_LONG).show()
                }else{

                    Toast.makeText(activity,response.toString(),Toast.LENGTH_LONG).show()
                }



            }

            override fun onFailure(call: Call<ResponseSetRequest>?, t: Throwable?) {
                Log.e("fragmen_request ","setNewRequestInServer onFailure"+t!!.message)
            }

        })


    }


    private fun validateFieldsRequest(list: List<EditText>):Boolean {


        var success = true
        for (view:EditText in list){

            if ( view.text.isEmpty()){
                view.setError("Dato obligatorio")
                success = false
            }
        }

        return success
    }




    override fun onConnectionFailed(p0: ConnectionResult) {
        TODO("not implemented") //To change body of created functions use File | Setting | File Templates.
    }




    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {



        if (requestCode == PLACE_PICKER_REQUEST){
            if (resultCode == RESULT_OK){

                val place:Place = PlacePicker.getPlace(activity,data)
                val arrayPlace = place.address.split(",")






                when(inputLocation!!.id){
                    R.id.requestTxt_Origin ->
                    {

                        var origin = Origen("${place.latLng.latitude},${place.latLng.longitude}")
                        listOrigins = ArrayList()
                        (listOrigins as ArrayList<Origen>).add(origin)

                        requestTxt_Origin.setText(place.address)
                        Log.e("origin mapa", "${place.latLng.latitude},${place.latLng.longitude}")
                        requestTxt_Destination.requestFocusFromTouch()



                    }
                    R.id.requestTxt_Destination ->{

                        var destination = Destino()
                        listDestinations = ArrayList()
                        destination.location = "${place.latLng.latitude},${place.latLng.longitude}"
                        (listDestinations as ArrayList<Destino>).add(destination)
                        requestTxt_Destination.setText(place.address)
                        Log.e("destination mapa", "${place.latLng.latitude},${place.latLng.longitude}")








                        //                        listOrigins.add(Origen("4.592313,-74.142199"))
//                        listDestinations.add(Destino("123456789","4.675942,-74.056026","Barrancabermeja - Yondó, Barrancabermeja, Santander","2",2))
//                        listOrigins.add(origin)
//                        listDestinations.add(Destino("123456789","4.664627500000017,-74.05431640624998","Barrancabermeja - Yondó, Barrancabermeja, Santander","2",2))

                        sendRequest(listOrigins,listDestinations)
                    }


                }



            }

        }else if (requestCode == DATA_PASSENGER_REQUEST){
            if (resultCode == RESULT_OK){

                val bundle = data!!.extras
//                val newPassenger = bundle!!.getParcelable<Passenger>("passenger")
//               var newPassenger=  data?.extras?.getParcelable<PassengerServer2>("passenger")
               var json = bundle!!.getString("JSON_LIST_PASSENGERS")
               var numberPassengers = bundle!!.getString("NUMBER_PASSENGERS")

                if (json != null){

                    jsonPassengers = json
                    vista!!.counterPassenger.text = numberPassengers


                }

               Log.e("lista", listPassengers.toString())



            }

        }




    }

    private var mMap: GoogleMap? = null
    private var Markers: java.util.ArrayList<Marker> = ArrayList()
    private var polylinePaths:List<Polyline>? = ArrayList()
    private var progressDialog: ProgressDialog? = null

    override fun onDirectionFinderStart() {

        progressDialog = ProgressDialog.show(activity, "Please wait.",
                "Finding direction..!", true)

        if (Markers != null) {
            for (marker in Markers!!) {
                marker.remove()
            }
        }


        if (polylinePaths != null) {
            for (polyline in polylinePaths!!) {
                polyline.remove()
            }
        }
    }

    override fun onDirectionFinderSuccess(routes: MutableList<Route>?) {

        progressDialog!!.dismiss()
        polylinePaths = java.util.ArrayList()
//        destinationMarkers = ArrayList<Marker>()

        if (routes != null) {
            Log.e("rutas", routes.size.toString())
        }

        //printMarketsRute()



        if (routes != null) {
            for (route in routes) {

                var icon = bitmapDescriptorFromVector(activity!!, R.drawable.ic_person_start)

                mMap!!.addMarker(
                        MarkerOptions()
                                .icon(icon)
                                .title("titulo")
                                .position(route.startLocation))




                 icon = bitmapDescriptorFromVector(activity!!, R.drawable.ic_person_end)

                mMap!!.addMarker(
                        MarkerOptions()
                                .icon(icon)
                                .title("titulo")
                                .position(route.endLocation))


                var zoom = 1.0
                if(route.distance.value < 1000f){
                    zoom = 15.0
                }else if (route.distance.value <= 2000f){
                    zoom = 14.0
                }else if (route.distance.value <= 3000f){
                    zoom = 13.0
                }else if (route.distance.value <= 4000f){
                    zoom = 12.0
                }else if (route.distance.value <= 5000f){
                    zoom = 11.0
                }else if (route.distance.value <= 60000f){
                    zoom = 11.0
                }else if (route.distance.value <= 7000f){
                    zoom = 9.0
                }else if (route.distance.value <= 8000f){
                    zoom = 8.0
                }else if (route.distance.value <= 9000f){
                    zoom = 7.0
                }else if (route.distance.value <= 10000f){
                    zoom = 6.0
                }else if (route.distance.value <= 11000f){
                    zoom = 5.0
                }else if (route.distance.value <= 12000f){
                    zoom = 5.0
                }else if (route.distance.value > 12000f){
                    zoom = 5.0
                }
                // obtiene la distancia de la ruta y la convierte de metros a kilometros
                mileage = ""+(route.distance.value / 1000)

                Log.e("rutas distancia", route.distance.value.toString()+" "+zoom)

                mMap?.moveCamera(CameraUpdateFactory.newLatLngZoom(route.startLocation, zoom.toFloat()))
//                    mMap!!.moveCamera(CameraUpdateFactory.newLatLngZoom(route.endLocation, (mMap?.cameraPosition!!.zoom-13811f) ))

//                mMap?.moveCamera(CameraUpdateFactory.newLatLngZoom(route.startLocation, 15f))
//                mMap!!.moveCamera(CameraUpdateFactory.newLatLngZoom(route.endLocation, (mMap?.cameraPosition!!.zoom-0.40f) ))



                val polylineOptions = PolylineOptions().geodesic(true).color(Color.BLUE).width(11f)

                for (i in 0 until route.points.size)
                    polylineOptions.add(route.points.get(i))

                (polylinePaths as java.util.ArrayList<Polyline>).add(mMap!!.addPolyline(polylineOptions))
            }
        }
    }

    override fun onMapReady(map: GoogleMap?) {
        mMap = map
    }

    private fun  bitmapDescriptorFromVector(context: Context, vectorResId:Int):BitmapDescriptor{

        var vectorDrawable = ContextCompat.getDrawable(context, vectorResId);
        vectorDrawable!!.setBounds(0, 0, vectorDrawable.getIntrinsicWidth(), vectorDrawable.getIntrinsicHeight())
        val bitmap = Bitmap.createBitmap(vectorDrawable!!.getIntrinsicWidth(), vectorDrawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888)
        val canvas = Canvas(bitmap)
        vectorDrawable!!.draw(canvas)
        return BitmapDescriptorFactory.fromBitmap(bitmap);

    }



    private fun takeSnapshot(newRequest: Travel) {
        if (mMap == null) {
            return
        }

        val snapshotHolder = vista!!.imgMapRute as ImageView

        val callback = GoogleMap.SnapshotReadyCallback { snapshot ->
            // Callback is called from the main thread, so we can modify the ImageView safely.
            snapshotHolder.setImageBitmap(snapshot)

            val resizedBitmap = Bitmap.createScaledBitmap(snapshot, 200, 200, true)
//            var bitmap = redimensionarImagenMaximo(snapshot,800f,600f)
             imgRuta = converToBase64(resizedBitmap)

            setNewRequestInServer(jsonPassengers,newRequest)


            requestTxt_Origin.setText("")
            requestTxt_Destination.setText("")
            requestTxt_Date.setText("")
            requestTxt_time.setText("")
            requestTxt_observation.setText("")
            requestSpinner_typeOperations.setText("")
            requestSpinner_typeServices.setText("")
            requestSpinner_typeVehicles.setText("")
            counterPassenger.text = "0"

            AddPassengersActivity.listPassengers.clear()

            Log.e("base imagen mapa", "///"+converToBase64(snapshot)
            )
        }

        if (true) {
            mMap!!.setOnMapLoadedCallback(GoogleMap.OnMapLoadedCallback { mMap!!.snapshot(callback) })
        } else {
            mMap!!.snapshot(callback)
        }
    }

    fun redimensionarImagenMaximo(mBitmap: Bitmap, newWidth: Float, newHeigth: Float): Bitmap {
        //Redimensionamos
        val width = mBitmap.width
        val height = mBitmap.height
        val scaleWidth = newWidth.toFloat() / width
        val scaleHeight = newHeigth.toFloat() / height
        // create a matrix for the manipulation
        val matrix = Matrix()
        // resize the bit map
        matrix.postScale(scaleWidth, scaleHeight)
        // recreate the new Bitmap
        return Bitmap.createBitmap(mBitmap, 0, 0, width, height, matrix, false)
    }


    fun converToBase64(bitmap:Bitmap):String{

        var baos =  ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos)
        var imageBytes = baos.toByteArray()
        var imageString = Base64.encodeToString(imageBytes, Base64.DEFAULT)

        return imageString
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    fun initMap(){
        val coder = Geocoder(activity)
        try {
            val adresses = coder.getFromLocationName("Cl. 90 #8-1, Bogotá", 50) as java.util.ArrayList<Address>
            for (add in adresses) {
                // if (statement) {//Controls to ensure it is right address such as country etc.
                val longitude = add.longitude
                val latitude = add.latitude
                //                }
                Log.e("location", "" + longitude + " - " + latitude)
            }
        } catch (e: IOException) {
            e.printStackTrace()
            Log.e("error", e.message)
        }


        val mapFragment = childFragmentManager.findFragmentById((R.id.map2)) as MapFragment
        mapFragment.getMapAsync(this)

    }



    private fun sendRequest(origin: List<Origen>?, destination: List<Destino>?) {

        var latestDestination = destination!!.size - 1
        var firstOrigin = 0
        var wayPoints = getWayPoints(origin,destination)



        Log.e("destination size", destination?.size.toString())

        try {
            if (destination != null && origin != null) {
                DirectionFinder(
                        this,
                        origin.get(firstOrigin).location,
//                        origin!!.get(origin.size-1).location,
                        destination.get(latestDestination).location,
                        wayPoints,
                        2
                ).execute()


            }
        } catch (e: UnsupportedEncodingException) {
            e.printStackTrace()
        }

    }


    private fun getWayPoints(origin: List<Origen>?, destination: List<Destino>?): String? {

        var wayPonts = ""

        if (origin != null) {
            for (pointsOrigin:Origen in origin.iterator()){

                if (pointsOrigin.orden != 1 ){


                    wayPonts += pointsOrigin.location+"|"

                }



            }
        }

        if (destination != null) {
            for (pointsDestination:Destino in destination.iterator()){

                if ( pointsDestination.orden != destination.size){


                    if (pointsDestination.orden != destination.size-1){

                        wayPonts += pointsDestination.location+"|"
                    }else{
                        wayPonts += pointsDestination.location
                    }
                }



            }
        }

        return wayPonts
    }








}



