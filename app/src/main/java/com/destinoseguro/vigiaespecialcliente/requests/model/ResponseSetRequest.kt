package com.destinoseguro.vigiaespecialcliente.requests.model

/**
 * Created by root on 21/12/17.
 */
class ResponseSetRequest {

    var success: Boolean? = false
    var id: Int? = 0
    var data: String? = null

    /**
     * No args constructor for use in serialization
     *
     */
    constructor() {}

    /**
     *
     * @param data
     * @param success
     */
    constructor(success: Boolean,id:Int, data: String) : super() {
        this.success = success
        this.id = id
        this.data = data
    }
}