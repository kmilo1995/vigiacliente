package com.destinoseguro.vigiaespecialcliente.requests.model

import android.arch.persistence.room.*

/**
 * Created by root on 4/12/17.
 */
@Dao
interface PassengerDao {

    @Query("SELECT * FROM table_passenger")
    fun getAll(): List<Passenger>


    @Query("SELECT * FROM table_passenger WHERE user_id = :arg0")
    fun loadAllByIds(id: String): List<Passenger>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(vararg passenger: Passenger)


    @Delete
    fun delete(passenger: Passenger)


    @Query("SELECT * FROM table_passenger WHERE id = (SELECT MAX(ID) FROM table_passenger); ")
    fun getLastId():Int


    @Query("DELETE FROM table_passenger")
    fun nukeTable()

}