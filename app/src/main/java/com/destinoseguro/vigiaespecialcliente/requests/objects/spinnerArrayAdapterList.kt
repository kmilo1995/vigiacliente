package com.destinoseguro.vigiaespecialcliente.requests.objects

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import com.destinoseguro.vigiaespecialcliente.R
import com.destinoseguro.vigiaespecialcliente.requests.model.itemSpinner


/**
 * Created by julianlopez on 27/12/17.
 */


class spinnerArrayAdapterList(context: Context, lists: ArrayList<itemSpinner>) : ArrayAdapter<itemSpinner>(context, 0, lists) {

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        var convertView = convertView
        // Get the data item for this position
        val item = getItem(position)
        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.item_spinner, parent, false)
        }
        // Lookup view for data population

        val tvName = convertView!!.findViewById<TextView>(R.id.item_text_spinner)
        // Populate the data into the template view using the data object
        tvName.setText(item.nombre.toString())

        // Return the completed view to render on screen
        return convertView
    }
}