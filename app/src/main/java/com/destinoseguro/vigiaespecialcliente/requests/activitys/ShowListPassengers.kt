package com.destinoseguro.vigiaespecialcliente.requests.activitys

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.View
import com.destinoseguro.vigiaespecialcliente.R
import com.destinoseguro.vigiaespecialcliente.requests.fragments.fragment_request
import com.destinoseguro.vigiaespecialcliente.requests.model.Passenger
import com.destinoseguro.vigiaespecialcliente.requests.objects.listPassengerAdapter
import kotlinx.android.synthetic.main.activity_show_list_passengers.*

class ShowListPassengers : AppCompatActivity() {



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_show_list_passengers)

        val bundle = intent.extras
        val passengerArrayList = bundle!!.getParcelableArrayList<Passenger>("listPassengers")



        val layoutManager:RecyclerView.LayoutManager = LinearLayoutManager(this)

        showListPassengerRecycleView.layoutManager = layoutManager
        showListPassengerRecycleView.setHasFixedSize(true)

//        val adapterPassenger = listPassengerAdapter(this,passengerArrayList )
        val adapterPassenger = listPassengerAdapter(this,fragment_request.Companion.listPassengers )



        showListPassengerRecycleView.adapter = adapterPassenger



    }




}
