package com.destinoseguro.vigiaespecialcliente.requests.model

import android.annotation.SuppressLint
import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

/**
 * Created by root on 21/12/17.
 */
@SuppressLint("ParcelCreator")
@Parcelize
class PassengerServer2(): Parcelable {

    var nombrePasajero: String? = null
    var apellidoPasajero: String? = null
    var documentoPasajero: String? = null
    var celularPasajero: String? = null
    var direccionorigenPasajero: String? = null
    var direcciondestinoPasajero: String? = null
    var observacionPasajero: String? = null
    var fechainicioPasajero: String? = null

    /**
     * No args constructor for use in serialization
     *
     */

    /**
     *
     * @param documentoPasajero
     * @param direcciondestinoPasajero
     * @param celularPasajero
     * @param fechainicioPasajero
     * @param direccionorigenPasajero
     * @param observacionPasajero
     * @param nombrePasajero
     * @param apellidoPasajero
     */
    constructor(
            nombrePasajero: String,
            apellidoPasajero: String,
            documentoPasajero: String,
            celularPasajero: String,
            direccionorigenPasajero: String,
            direcciondestinoPasajero: String,
            observacionPasajero: String,
            fechainicioPasajero: String
    ):this(){

        this.direccionorigenPasajero = direccionorigenPasajero
        this.direcciondestinoPasajero = direcciondestinoPasajero
        this.fechainicioPasajero = fechainicioPasajero
        this.nombrePasajero = nombrePasajero
        this.apellidoPasajero = apellidoPasajero
        this.celularPasajero = celularPasajero
        this.documentoPasajero = documentoPasajero
        this.observacionPasajero = observacionPasajero
    }

    override fun toString(): String {
        return "PassengerServer2(nombrePasajero=$nombrePasajero, apellidoPasajero=$apellidoPasajero, documentoPasajero=$documentoPasajero, celularPasajero=$celularPasajero, direccionorigenPasajero=$direccionorigenPasajero, direcciondestinoPasajero=$direcciondestinoPasajero, observacionPasajero=$observacionPasajero, fechainicioPasajero=$fechainicioPasajero)"
    }


}