package com.destinoseguro.vigiaespecialcliente.requests.activitys

import android.app.Activity
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.MotionEvent
import android.view.View
import android.widget.Toast
import com.destinoseguro.vigiaespecialcliente.R
import com.destinoseguro.vigiaespecialcliente.requests.model.Passenger
import com.destinoseguro.vigiaespecialcliente.requests.objects.MainTest
import com.destinoseguro.vigiaespecialcliente.requests.objects.addPassengerAdapter
import kotlinx.android.synthetic.main.add_passengers.*
import android.widget.AdapterView.OnItemClickListener



class AddPassengersActivity : AppCompatActivity() {


    var originRequest:String?= null
    var destinationRequest:String?= null
    var activity:Activity? = null



    companion object {

        val listPassengers = HashMap<String,Passenger>()
    }



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.add_passengers)

       activity = this

        renderListNewsPassengers(null)

        toolbarAddPassenger.title = "Agregar pasajeros"
        originRequest = intent.getStringExtra("ORIGIN_REQUEST")
        destinationRequest = intent.getStringExtra("DESTINATION_REQUEST")

        if (!originRequest.equals("")){
            txtOriginRequest.setText(originRequest)
        }

        if (!destinationRequest.equals("")){
            txtDestinationRequest.setText(destinationRequest)
        }


        setSupportActionBar(toolbarAddPassenger)

        fab_add_Passenger.setOnClickListener(View.OnClickListener {


             openFormPassenger()


        })

        btnSaveListPassengers.setOnClickListener(View.OnClickListener {

          saveListPassengerInRequest()


        })








    }

    private fun openFormPassenger() {

        var intentFormAddPassenger = Intent(this,formAddPassenger::class.java)
        intentFormAddPassenger.putExtra("ORIGIN_REQUEST",originRequest)
        intentFormAddPassenger.putExtra("DESTINATION_REQUEST",destinationRequest)

        startActivityForResult(intentFormAddPassenger,6)
    }

    private fun saveListPassengerInRequest() {

        var json =  createJsonPassengers(listPassengers.values.toList())

        intent.putExtra("JSON_LIST_PASSENGERS",json)
        intent.putExtra("NUMBER_PASSENGERS", listPassengers.size.toString())
        Log.e("number passenger", listPassengers.size.toString())
        setResult(Activity.RESULT_OK,intent)
        finish()
    }


    private fun createJsonPassengers(listPassengers: List<Passenger>):String {


        var MainTest = MainTest()

//        Log.e("lista pasajeros", listPassengers[0].toString())
        var json = MainTest.convert(listPassengers.toList())

        json = json.replace("origin","direccionorigenPasajero")
        json = json.replace("destination","direcciondestinoPasajero")
        json = json.replace("startDate","fechainicioPasajero")
        json = json.replace("name","nombrePasajero")
        json = json.replace("lastName","apellidoPasajero")
        json = json.replace("phoneNumber","celularPasajero")
        json = json.replace("document","documentoPasajero")
        json = json.replace("observation","observacionPasajero")

        Log.e("json pasajeros", json)

        return json

    }


    override fun onCreateOptionsMenu(menu: Menu?): Boolean {

        menuInflater.inflate(R.menu.menu_add_passenger, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        when(item.itemId){
            R.id.nav_save_new_passenger->{
                saveListPassengerInRequest()
            }
            R.id.action_form_new_passenger->{
                openFormPassenger()
            }
        }

        return true
    }








    var EDDIT_NEW_PASSENGER: Int = 7

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {

        if (requestCode == 6) {
            if (resultCode == RESULT_OK) {

                renderListNewsPassengers(data!!.extras)


            }
        } else if (requestCode == EDDIT_NEW_PASSENGER) {
            if (resultCode == RESULT_OK) {
                renderListNewsPassengers(data!!.extras)

            }
        }


    }



    private fun renderListNewsPassengers(bundle: Bundle?) {

        val newPassenger = bundle?.getParcelable<Passenger>("passenger")


        if (newPassenger != null) {

            Log.e("nuevo pasajero", newPassenger.toString())

//
            if(listPassengers.contains(newPassenger.document)){
                Toast.makeText(this,R.string.message_update_passenger,Toast.LENGTH_LONG).show()
            }else{
                Toast.makeText(this,R.string.message_new_passenger,Toast.LENGTH_LONG).show()
            }
            listPassengers.put(newPassenger.document.toString(),newPassenger)



        }

        if (AddPassengersActivity.listPassengers.size > 0) {
            btnSaveListPassengers.visibility = View.VISIBLE
        }

        var adapterAddPassenger = addPassengerAdapter(activity, listPassengers.values.toList(), EDDIT_NEW_PASSENGER)

        var layoutManagerAddPassenger: RecyclerView.LayoutManager = LinearLayoutManager(activity)

        recyclerAddPassengers.layoutManager = layoutManagerAddPassenger
        recyclerAddPassengers.adapter = adapterAddPassenger
    }


}
