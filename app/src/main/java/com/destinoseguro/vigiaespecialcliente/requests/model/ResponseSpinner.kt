package com.destinoseguro.vigiaespecialcliente.requests.model

/**
 * Created by root on 28/11/17.
 */
class ResponseSpinner {

    var isSuccess: Boolean = false
    var data: ArrayList<itemSpinner>? = null

    /**
     * No args constructor for use in serialization
     *
     */
    constructor() {}

    /**
     *
     * @param data
     * @param success
     */
    constructor(success: Boolean, data: ArrayList<itemSpinner>) : super() {
        this.isSuccess = success
        this.data = data
    }



}

class itemSpinner {

    var id: String? = null
    var nombre: String? = null

    /**
     * No args constructor for use in serialization
     *
     */
    constructor() {}

    /**
     *
     * @param nombre
     * @param id
     */
    constructor(id: String, nombre: String) : super() {
        this.id = id
        this.nombre = nombre
    }

}