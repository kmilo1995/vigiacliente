package com.destinoseguro.vigiaespecialcliente.requests.objects

import android.annotation.SuppressLint
import android.arch.persistence.room.Entity
import android.arch.persistence.room.ForeignKey
import android.os.Parcelable
import com.destinoseguro.vigiaespecialcliente.travels.model.Travel
import com.google.android.gms.maps.model.LatLngBounds
import kotlinx.android.parcel.Parcelize
import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.PrimaryKey



/**
 * Created by root on 22/11/17.
 */
@SuppressLint("ParcelCreator")
@Parcelize

class passenger(

        var name: String,
        var lastName:String?,
        var document:String?,
        var phoneNumber:String?,
        var origin:String?,
        var destination:String?




) : Parcelable {//,lastName:String,document:String,phoneNumber:String,origin: LatLngBounds? ,destination: LatLngBounds?) {


constructor(name: String):this(name,null,null,null,null,null){

    this.name = name
}



}


