package com.destinoseguro.vigiaespecialcliente.requests.model;

import com.destinoseguro.vigiaespecialcliente.restApi.ConstantsRestApi;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by root on 28/11/17.
 */

public interface EndPointsRequests {



    @POST(ConstantsRestApi.GET_TYPE_SERVICES)
    Call<ResponseSpinner> getTypeServices();

    @POST(ConstantsRestApi.GET_TYPE_VEHICLES)
    Call<ResponseSpinner> getTypeVehicles();

//cambiar por end point de operaciones
    @POST(ConstantsRestApi.GET_TYPE_OPERATIONS)
    Call<ResponseSpinner> getTypeOperations();

    @FormUrlEncoded
    @POST(ConstantsRestApi.SET_REQUEST)
    Call<ResponseSetRequest> setRequest(
            @Field("direccionorigen") String origin,
            @Field("direcciondestino")String destination,
            @Field("fechainicio")String startTime,
            @Field("observacion")String observation,
            @Field("vciclico")String goAndBack,
            @Field("idtiposervicio")String idTypeServices,
            @Field("idtipovehiculo")String idTypeVehicle,
            @Field("pasajero")String jsonPassengers,
            @Field("idtipooperacion")String idTypeOperation,
            @Field("imei")String imei,
            @Field("kilometraje")String mileage,
            @Field("imgruta")String imageRute

    );





}
