package com.destinoseguro.vigiaespecialcliente.requests.objects

import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.TextView
import android.widget.Toast
import com.destinoseguro.vigiaespecialcliente.R
import com.destinoseguro.vigiaespecialcliente.requests.model.Passenger
import com.destinoseguro.vigiaespecialcliente.requests.model.PassengerServer2


/**
 * Created by root on 22/11/17.
 */
class listPassengerAdapter(private val mContext: AppCompatActivity, private val passengerArrayList: ArrayList<Passenger>?) : RecyclerView.Adapter<listPassengerAdapter.CustomViewHolder>() {

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): CustomViewHolder {
        val view = LayoutInflater.from(viewGroup.context).inflate(R.layout.card_view_passenger_row, null)
        return CustomViewHolder(view)
    }

    override fun onBindViewHolder(customViewHolder: CustomViewHolder, position: Int) {
        val passengerItem = passengerArrayList!![position]

        //Setting text view title
        customViewHolder.textView.text = passengerItem.name
        customViewHolder.buttonDelete.setOnClickListener(View.OnClickListener {
            Toast.makeText(mContext,""+passengerItem.name,Toast.LENGTH_LONG).show()

            this.notifyItemRemoved(position)
            this.passengerArrayList.remove(passengerItem)

        })



    }

    override fun getItemCount(): Int {
        return passengerArrayList?.size ?: 0
    }

    inner class CustomViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        var textView: TextView
        var buttonDelete: ImageButton


        init {

            this.textView = view.findViewById<View>(R.id.cardPassengerName) as TextView
            this.buttonDelete = view.findViewById<View>(R.id.cardPassengerButtonDelete) as ImageButton


        }
    }
}