package com.destinoseguro.vigiaespecialcliente.requests.activitys;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.destinoseguro.vigiaespecialcliente.R;
import com.destinoseguro.vigiaespecialcliente.requests.model.Passenger;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.Length;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;

import java.util.List;

/**
 * Created by root on 17/01/18.
 */

public class formAddPassenger extends AppCompatActivity implements Validator.ValidationListener{


    private Validator validator;
    private int PLACE_PICKER_REQUEST = 1;


    //fiels
    @Length(sequence = 1, min = 3, message = "Seleccione un origen")
    private EditText Origin;
    @Length(sequence = 2, min = 3, message = "Seleccione un destino")
    private EditText Destination;
    @Length(sequence = 3, min = 6, message = "Digite un numero de documento")
    private EditText Document;
    @Length(sequence = 4, min = 3, message = "Digite un nombre")
    private EditText Name;
    @Length(sequence = 5, min = 3, message = "Digite un apellido")
    private EditText LastName;
    @Length(sequence = 6, min = 7, message = "Digite un numero de telefono")
    private EditText Phone;
    private Button AddPassengersBtn;
    private CheckBox checkOriginAndDestinationParent;
    private LinearLayout boxInputsPassengerOrigin;
    private LinearLayout boxInputsPassengerDestination;

    private Activity context;

    private View inputLocation;

    private String originRequest = null;
    private String destinationRequest = null;
    private Passenger newPassenger = null;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_add_passanger);
        validator = new Validator(this);
        validator.setValidationListener(this);
        setTitle("Nuevo Pasajero");



        Bundle bundle = getIntent().getExtras();
         newPassenger  = bundle.getParcelable("passenger");



        originRequest = getIntent().getStringExtra("ORIGIN_REQUEST");
        destinationRequest = getIntent().getStringExtra("DESTINATION_REQUEST");



        AddPassengersBtn = findViewById(R.id.addPassengersBtn);
        Origin = findViewById(R.id.addPassengersTxt_Origin);
        Destination = findViewById(R.id.addPassengersTxt_Destination);
        Document = findViewById(R.id.addPassengersTxt_document);
        Name = findViewById(R.id.addPassengersTxt_name);
        LastName = findViewById(R.id.addPassengersTxt_lastName);
        Phone = findViewById(R.id.addPassengersTxt_phoneNumber);
        checkOriginAndDestinationParent = findViewById(R.id.checkOriginAndDestinationParent);
        boxInputsPassengerOrigin = findViewById(R.id.boxInputsPassenger_Origin);
        boxInputsPassengerDestination = findViewById(R.id.boxInputsPassenger_Destination);

        if(newPassenger != null ){

            originRequest = newPassenger.getOrigin();
            destinationRequest = newPassenger.getDestination();
            Name.setText(newPassenger.getName());
            LastName.setText(newPassenger.getLastName());
            Phone.setText(newPassenger.getPhoneNumber());
            Document.setText(newPassenger.getDocument());
            checkOriginAndDestinationParent.setVisibility(View.GONE);
            boxInputsPassengerOrigin.setVisibility(View.VISIBLE);
            boxInputsPassengerDestination.setVisibility(View.VISIBLE);


        }else{
            checkOriginAndDestinationParent.setVisibility(View.VISIBLE);
        }


        if (!originRequest.isEmpty() && !destinationRequest.isEmpty() ){
            Origin.setText(originRequest);
            Destination.setText(destinationRequest);



        }else {
            checkOriginAndDestinationParent.setVisibility(View.GONE);
            boxInputsPassengerOrigin.setVisibility(View.VISIBLE);
            boxInputsPassengerDestination.setVisibility(View.VISIBLE);
        }




        checkOriginAndDestinationParent.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if(checkOriginAndDestinationParent.isChecked()){
                    boxInputsPassengerOrigin.setVisibility(View.GONE);
                    boxInputsPassengerDestination.setVisibility(View.GONE);
                    Origin.setText(originRequest);
                    Destination.setText(destinationRequest);
                }else {
                    boxInputsPassengerOrigin.setVisibility(View.VISIBLE);
                    boxInputsPassengerDestination.setVisibility(View.VISIBLE);
                    Origin.setText(originRequest);
                    Destination.setText(destinationRequest);
                }
            }
        });


       context = this;

       Origin.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {

                //llamada al selector de sitios
               inputLocation = v;
               PlacePicker.IntentBuilder builder  =new PlacePicker.IntentBuilder();
               try {
                   startActivityForResult(builder.build(context),PLACE_PICKER_REQUEST);
               } catch (GooglePlayServicesRepairableException e) {
                   e.printStackTrace();
               } catch (GooglePlayServicesNotAvailableException e) {
                   e.printStackTrace();
               }
           }
       });


       Destination.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {

               inputLocation = v;
               PlacePicker.IntentBuilder builder  =new PlacePicker.IntentBuilder();
               try {
                   startActivityForResult(builder.build(context),PLACE_PICKER_REQUEST);
               } catch (GooglePlayServicesRepairableException e) {
                   e.printStackTrace();
               } catch (GooglePlayServicesNotAvailableException e) {
                   e.printStackTrace();
               }
           }
       });

        AddPassengersBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validator.validate();
            }
        });
    }

    @Override
    public void onValidationSucceeded() {




        Intent intent =new Intent();

                 newPassenger =new Passenger(
                        Name.getText().toString(),
                        LastName.getText().toString(),
                        Document.getText().toString(),
                        Phone.getText().toString(),
                        Origin.getText().toString(),
                        Destination.getText().toString(),
                        "observacion ",
                        "22/12/2017 17:22:00",
                        null
                );


                intent.putExtra("passenger",newPassenger);
                setResult(Activity.RESULT_OK,intent);
                finish();

    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {

        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);


            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(this, message, Toast.LENGTH_LONG).show();
            }
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {


        if (requestCode == PLACE_PICKER_REQUEST){
            if (resultCode == RESULT_OK){

                Place place = PlacePicker.getPlace(this,data);
                String[] arrayPlace = place.getAddress().toString().split(",");

                switch (inputLocation.getId()){
                    case R.id.addPassengersTxt_Origin:{
                        Origin.setText(place.getAddress());
                        Destination.requestFocusFromTouch();
                        break;
                    }
                    case R.id.addPassengersTxt_Destination:{
                        Destination.setText(place.getAddress());
                        break;
                    }
                }





            }
//
        }


    }

    public void validateFiels(View view){

        validator.validate();
    }


}
