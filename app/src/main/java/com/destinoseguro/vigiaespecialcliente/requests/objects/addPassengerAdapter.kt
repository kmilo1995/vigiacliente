package com.destinoseguro.vigiaespecialcliente.requests.objects

import android.app.Activity
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.destinoseguro.vigiaespecialcliente.R
import com.destinoseguro.vigiaespecialcliente.requests.activitys.AddPassengersActivity
import com.destinoseguro.vigiaespecialcliente.requests.activitys.formAddPassenger
import com.destinoseguro.vigiaespecialcliente.requests.model.Passenger
import kotlinx.android.synthetic.main.card_view_passenger_row.view.*

/**
 * Created by root on 25/01/18.
 */
class addPassengerAdapter(private val context: Activity?, private val listPassenges: List<Passenger>, private val EDDIT_NEW_PASSENGER: Int): RecyclerView.Adapter<addPassengerAdapter.CustomerViewHolder>() {



    //Creating an ArrayList of values
    var listPassengesArray = ArrayList<Passenger>(listPassenges)

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): CustomerViewHolder {

        var view:View = LayoutInflater.from(parent!!.context).inflate(R.layout.card_view_passenger_row,null)


        return CustomerViewHolder(view)
    }

    override fun getItemCount(): Int {
       return listPassengesArray.size
    }

    override fun onBindViewHolder(holder: CustomerViewHolder?, position: Int) {

        var itemAddPassenger = listPassengesArray.get(position)

        if (holder != null) {
            holder.passengerName.text = itemAddPassenger.name+" "+itemAddPassenger.lastName
            holder.passengerDocument.text = itemAddPassenger.document
            holder.passengerPhone.text = itemAddPassenger.phoneNumber
            holder.passengerOrigin.text = itemAddPassenger.origin
            holder.passengerDestination.text = itemAddPassenger.destination


            holder.cardPassengerButtonEddit.setOnClickListener(View.OnClickListener {

                var intentFormAddPassenger = Intent(context, formAddPassenger::class.java)
                intentFormAddPassenger.putExtra("passenger", itemAddPassenger)


                if (context != null) {
                    context.startActivityForResult(intentFormAddPassenger,EDDIT_NEW_PASSENGER)
                }
            })

            holder.cardPassengerButtonDelete.setOnClickListener(View.OnClickListener {


                AddPassengersActivity.listPassengers.remove(listPassengesArray.get(position).document)
                listPassengesArray.removeAt(position)
                notifyItemRemoved(position)
                notifyItemRangeChanged(position, listPassenges.size)
                holder.itemView.setVisibility(View.GONE)

            })


        }

    }


    inner class CustomerViewHolder(view: View):RecyclerView.ViewHolder(view) {


        var passengerName = view.cardPassengerName
        var passengerDocument = view.cardPassenger_document
        var passengerPhone = view.cardPassenger_phone
        var passengerOrigin = view.cardPassenger_origin
        var passengerDestination = view.cardPassenger_destination
        var cardPassengerButtonEddit = view.cardPassengerButtonEddit
        var cardPassengerButtonDelete = view.cardPassengerButtonDelete
    }


}