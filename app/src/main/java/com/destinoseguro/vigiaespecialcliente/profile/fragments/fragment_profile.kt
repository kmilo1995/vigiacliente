package com.destinoseguro.vigiaespecialcliente.profile.fragments

import android.Manifest
import android.app.AlertDialog
import  android.app.Fragment
import android.content.ContentValues.TAG
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.destinoseguro.vigiaespecialcliente.R
import kotlinx.android.synthetic.main.fragment_profile.view.*
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.support.v4.content.ContextCompat
import android.support.v13.app.FragmentCompat
import android.widget.Toast
import android.content.DialogInterface
import android.provider.Settings
import android.provider.MediaStore
import android.app.Activity.RESULT_OK
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Canvas
import android.graphics.drawable.Drawable
import android.preference.PreferenceManager
import android.util.Base64
import com.destinoseguro.vigiaespecialcliente.Launch_Activity
import com.destinoseguro.vigiaespecialcliente.dashboard.activitys.dashboard
import com.destinoseguro.vigiaespecialcliente.firebase.Firebase
import com.destinoseguro.vigiaespecialcliente.profile.activitys.Setting
import com.destinoseguro.vigiaespecialcliente.profile.activitys.aboutVigia
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import de.hdodenhof.circleimageview.CircleImageView
import kotlinx.android.synthetic.main.fragment_profile.*
import kotlinx.android.synthetic.main.nav_header_dashboard.*
import java.io.ByteArrayOutputStream


/**
 * Created by root on 16/11/17.
 */
 class fragment_profile: Fragment() {

//    private val mDatabase: DatabaseReference? = null
    private val firebase = Firebase.getInstance()

    private val REQUEST_READ_EXTERNAL_STORAGE = 7

   private val COD_SELECCIONA = 10
  private  val COD_FOTO = 20

    private val CARPETA_RAIZ = "imagesVigia/"
    private val RUTA_IMAGEN = CARPETA_RAIZ + "images"
    var  path:String? = null

    var IMEI:String? = null

    companion object {

        var viewFragment:View? = null



        fun enableInput(viewFragment:View){

        }

        fun disableInput(viewFragment:View){

//
        }
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        IMEI = Settings.Secure.getString(activity.getContentResolver(), Settings.Secure.ANDROID_ID)

       viewFragment =inflater!!.inflate(R.layout.fragment_profile, container, false)
//        mDatabase = FirebaseDatabase.getInstance().getReference();
        val view = viewFragment

        view!!.profile_image.setOnClickListener(View.OnClickListener {

            profileChangePhoto()

        })

        view.cardViewAboutVigia.setOnClickListener(View.OnClickListener {



            var intent = Intent(activity,aboutVigia::class.java)
            startActivity(intent)

        })


        view.cardViewSettings.setOnClickListener(View.OnClickListener {
            var intent = Intent(activity, Setting::class.java)
            startActivity(intent)

        })

        view.closedSession.setOnClickListener(View.OnClickListener {
            val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(activity)
             sharedPreferences.edit().putBoolean("SESSION_USER", false).commit()

            var intent = Intent(activity, Launch_Activity::class.java)
            startActivity(intent)
            activity.finish()

        })


        firebase.getSelectAlterna("Usuarios/Customers/" + IMEI).addValueEventListener(object : ValueEventListener{

            override fun onDataChange(dataProfile: DataSnapshot?) {

                try {

                    view.profileTextView_document.text = dataProfile!!.child("document").value.toString()
                    var base64Img = dataProfile!!.child("photo").value.toString()
                    var bitmapImage =  convertStringToBitmap(base64Img)
                    view.profile_image.setImageBitmap(bitmapImage)
                    dashboard.logoProfile.setImageBitmap(bitmapImage)
                    val nombre = dataProfile!!.child("name").value.toString()
                    val apellido = dataProfile!!.child("lastName").value.toString()
                    view.profileTextView_names.text = Character.toUpperCase(nombre.get(0)) + nombre.substring(1, nombre.length) + " " +Character.toUpperCase(apellido.get(0)) + apellido.substring(1, apellido.length)
                    view.profileTextView_phoneNumber.text = dataProfile!!.child("phone").value.toString()
                    view.profileTextView_mail.text = dataProfile!!.child("email").value.toString()

                }catch (e:Exception){
                    Log.e("fragment profile ","Exception"+e.message)
                }


            }

            override fun onCancelled(p0: DatabaseError?) {

            }
        })





       return viewFragment

    }




    private fun profileChangePhoto() {


        // Here, thisActivity is the current activity
        if (ContextCompat.checkSelfPermission(activity,
                Manifest.permission.READ_CONTACTS)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (FragmentCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.READ_CONTACTS)) {

                // Show an expanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
//                Toast.makeText(context,"Desves")
                FragmentCompat.requestPermissions(this,
                        arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE),
                        REQUEST_READ_EXTERNAL_STORAGE)

            } else {

                // No explanation needed, we can request the permission.

                FragmentCompat.requestPermissions(this,
                        arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE),
                        REQUEST_READ_EXTERNAL_STORAGE)

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        }

////


    }

//

    override fun onRequestPermissionsResult(requestCode: Int,
                                            permissions: Array<String>, grantResults: IntArray) {

        Log.e("result","ok")
        when (requestCode) {
            REQUEST_READ_EXTERNAL_STORAGE -> {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    uploadImage()
                    Log.v(TAG,"success")
                } else {

                    solicitarPermisosManual();
                    Log.e(tag,"permission denied")
                    Toast.makeText(activity,"Para poder cargar una imagen de perfil debe ceder permisos de almacenamiento",Toast.LENGTH_LONG).show()
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return
            }
        }// other 'case' lines to check for other
        // permissions this app might request
    }



    private fun solicitarPermisosManual() {

        val opciones = arrayOf<CharSequence>("si", "no")
        val alertOpciones = AlertDialog.Builder(activity)
        alertOpciones.setTitle("¿Desea configurar los permisos de forma manual?")
        alertOpciones.setItems(opciones, DialogInterface.OnClickListener { dialogInterface, i ->
            if (opciones[i] == "si") {
                val intent = Intent()
                intent.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
                val uri = Uri.fromParts("package", activity.packageName, null)
                intent.data = uri
                startActivity(intent)
            } else {
                Toast.makeText(activity, "Los permisos no fueron aceptados", Toast.LENGTH_SHORT).show()
                dialogInterface.dismiss()
            }
        })
        alertOpciones.show()
    }


    private fun uploadImage() {

        val opciones = arrayOf<CharSequence>( "Cargar Imagen", "Cancelar")
        val alertOpciones = AlertDialog.Builder(activity)
        alertOpciones.setTitle("Seleccione una Opción")
        alertOpciones.setItems(opciones) { dialogInterface, i ->
            if (opciones[i] == "Cargar Imagen") {


                if (opciones[i] == "Cargar Imagen") {
                    val intent = Intent(Intent.ACTION_GET_CONTENT, MediaStore.Images.Media.INTERNAL_CONTENT_URI)
                    intent.type = "image/"

                    startActivityForResult(Intent.createChooser(intent, "Seleccione la Aplicación"), COD_SELECCIONA)

                } else {
                    dialogInterface.dismiss()
                }
            }
        }
        alertOpciones.show()

    }



    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode == RESULT_OK) {

            when (requestCode) {
                COD_SELECCIONA -> {
                    var miPath = data!!.data
                    profile_image.setImageURI(miPath)


                    putPhotoInFirebase(profile_image)
                }
            }
        }
    }


    fun convertStringToBitmap(imgStringBase64: String): Bitmap {

        var decodedImage = BitmapFactory.decodeResource(getResources(), R.drawable.perfil);
        if (!imgStringBase64.isEmpty()){

            var imageBytes = Base64.decode(imgStringBase64, Base64.DEFAULT)
             decodedImage = BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.size)
        }



        return decodedImage
    }

    fun convertToBitmap(drawable: Drawable, widthPixels: Int, heightPixels: Int): Bitmap {
        val mutableBitmap = Bitmap.createBitmap(widthPixels, heightPixels, Bitmap.Config.ARGB_8888)
        val canvas = Canvas(mutableBitmap)
        drawable.setBounds(0, 0, widthPixels, heightPixels)
        drawable.draw(canvas)

        return mutableBitmap
    }





    fun converToBase64(bitmap:Bitmap):String{

        var baos =  ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos)
        var imageBytes = baos.toByteArray()
        var imageString = Base64.encodeToString(imageBytes, Base64.DEFAULT)

        return imageString
    }

     fun putPhotoInFirebase(img: CircleImageView) {

         val bitmap = convertToBitmap(img.drawable,img.width,img.height)
//
         var imgBase64 = converToBase64(bitmap)

         Log.e("imagen base 64 ",imgBase64)

         firebase.setInsertNewFiel(imgBase64, "Usuarios/Customers/" + IMEI.toString()+"/photo".replace("[.\\s]".toRegex(), ""))


    }


}

