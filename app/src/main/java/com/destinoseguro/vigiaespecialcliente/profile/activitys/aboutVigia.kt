package com.destinoseguro.vigiaespecialcliente.profile.activitys

import android.app.FragmentManager
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.destinoseguro.vigiaespecialcliente.R
import com.destinoseguro.vigiaespecialcliente.R.id.toolbar
import kotlinx.android.synthetic.main.activity_about_vigia.*
import android.content.Intent
import android.net.Uri
import com.destinoseguro.vigiaespecialcliente.profile.fragments.termsAndConditions


class aboutVigia : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_about_vigia)
        setSupportActionBar(toolbarAbout)
        setTitle("Acerca de Vigia")


        aboutCardShowWebVigia.setOnClickListener(View.OnClickListener {

            val uri = Uri.parse("http://transportesvigia.com/")
            val intent = Intent(Intent.ACTION_VIEW, uri)
            startActivity(intent)
        })


        aboutCardTermsAndConditions.setOnClickListener(View.OnClickListener {

            var fragmentManager = fragmentManager

            var fragmentTermsAndConditions = termsAndConditions()

            fragmentTermsAndConditions.show(fragmentManager,"Tems and Conditions")

        })






    }
}

